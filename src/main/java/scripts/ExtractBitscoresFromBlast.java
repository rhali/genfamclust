package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class ExtractBitscoresFromBlast {

	public static void main(String[] args) {
		String fileName = "MultiSpecSANCComplete.bl";
		String path = "/Users/rhali/Desktop/Projects/GenFamClust2/Datasets/Multi-Species/BlastResults/";
		String str;
		int count = 0;
		try {
			BufferedReader bufferedreader = new BufferedReader(new FileReader(path + fileName));
			while ((str = bufferedreader.readLine()) != null) {
				count++;
				String[] strArr = str.split("\t");
				SequenceFileWriter.writeAndAppendLine(path, "MultiSpecSANC.bl", strArr[0].replaceAll("\\s", "") + " " + strArr[1].replaceAll("\\s", "") + " " + strArr[11].replaceAll("\\s", ""));
				if (count%100000==1)
					System.out.println("Count = " + count);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
	}
}
