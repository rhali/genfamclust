package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class RemoveSelfHits {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int data = 7;
		String path = "/Users/rhali/Desktop/Simulations/TransSimulation_" + data + "/DB/";
		String fileName = "Data" + data + ".nnc";
		BufferedReader bufferedreader;
		String str;

		try {
			bufferedreader = new BufferedReader(new FileReader(path + fileName));
			while ((str = bufferedreader.readLine()) != null) { 
					String[] strArr = str.split("\t");
					if(!strArr[0].equals(strArr[1]))
						SequenceFileWriter.writeAndAppendLine(path, "Data" + data + "Updated.nnc", str);
			}
			bufferedreader.close();
			System.out.println("Completed Writing Synteny file");

		} catch (Exception e) {
			System.out.println("Failed: " + e.getMessage());
			System.exit(-1);
		}

	}

}
