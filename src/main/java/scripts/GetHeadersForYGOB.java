package scripts;

import java.util.ArrayList;

import miscellaneous.Sequence;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;

public class GetHeadersForYGOB {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = "./Datasets/YGOB/";
		String fileName = "AA.fsa";
		SimpleFileIO reader = new SimpleFileIO();
		
		try {
			reader.readSequenceFile(path, fileName);
			ArrayList<Sequence> sequence = reader.getSequences();
			for(int i = 0; i < sequence.size(); i++) {
				String header = sequence.get(i).getIdentifier();
				String[] strArr = header.split(" ");
				Sequence seq = new Sequence(strArr[0], sequence.get(i).getSequence());
				SequenceFileWriter.writeAndAppendSequenceInFasta("./Results/Yeast_Input_Files/", "ProteinSequence.fasta", seq);
			}
			
		} catch (Exception e) {
			System.out.println("Headers For YGOB failed: " + e.getMessage());
			System.exit(-1);
		}

	}

}
