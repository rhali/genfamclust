package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class SeperatingQueryQueryDataset {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = "/Users/rhali/Desktop/Projects/GenFamClust2/Datasets/Multi-Species/BlastResults/";
		String fileName = "MultiSpecSANCComplete.bl";
		BufferedReader bufferedreader;
		String str;

		try {
			bufferedreader = new BufferedReader(new FileReader(path + fileName));
			while ((str = bufferedreader.readLine()) != null) { 
				String[] strArr = str.split("\t");
				if(strArr[0].startsWith("7.")) {
					if(strArr[1].startsWith("7.") || strArr[1].startsWith("10."))
						SequenceFileWriter.writeAndAppendLine(path, "Human.txt", str);
				} else if(strArr[0].startsWith("10.")) {
					if(strArr[1].startsWith("7.") || strArr[1].startsWith("10."))
						SequenceFileWriter.writeAndAppendLine(path, "Mouse.txt", str);
				}
			}
			bufferedreader.close();
			System.out.println("Completed Reading allvsallBlast");

		} catch (Exception e) {
			System.out.println("Failed: " + e.getMessage());
			System.exit(-1);
		}

	}

}
