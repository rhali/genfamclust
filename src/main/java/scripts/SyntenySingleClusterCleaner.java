package scripts;

import java.util.ArrayList;

import miscellaneous.SimpleFileIO;
import miscellaneous.Species;

public class SyntenySingleClusterCleaner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String syntenyFile = "Synteny.hsf";
		String blastFileName = "YeastAllvsAllBlast.bl";
		String path = "/Users/rhali/Desktop/Results3/";
		int count = 0;
		try {
			SimpleFileIO reader = new SimpleFileIO();
			reader.readSyntenyFile(path, syntenyFile);
			ArrayList<Species> speciesList = reader.getSpeciesList();
			reader.readBlastFile(path, blastFileName, speciesList);
			for(int i = 0; i < speciesList.size(); i++) {
				for(int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
					for(int k = 0; k < speciesList.get(i).getChromosomes().get(j).getGeneList().size(); k++) {
						if(speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGeneBlastResult().size() == 0) {
							System.out.println("Blast hit not found for " + count + "\t" + i + "." + j + "." + k);
						} 
						count++;
					}
				}
				System.out.println("Species " + i);
			}
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

}
