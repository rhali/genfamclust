package scripts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import miscellaneous.Species;

public class ConvertIndexedNamesToDataNames {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			String fileName = "Synteny.hsf", path = "/Users/rhali/Desktop/Projects/GenFamClust2/Datasets/Multi-Species/BlastResults/", str;
			SimpleFileIO reader = new SimpleFileIO();
			reader.readSyntenyFile(path, fileName);
			System.out.println("File read");
			ArrayList<Species> speciesList = reader.getSpeciesList();
			
			BufferedReader bufferedreader;
			bufferedreader = new BufferedReader(new FileReader(path + "MultiSpecSANC.bl"));
			while ((str = bufferedreader.readLine()) != null) {
				String[] strArr = str.split(" ");
				String[] strArr1 = strArr[0].split("\\.");
				String[] strArr2 = strArr[1].split("\\.");
				int spec1 = Integer.parseInt(strArr1[0]);
				int chr1 = Integer.parseInt(strArr1[1]);
				int gene1 = Integer.parseInt(strArr1[2]);
				int spec2 = Integer.parseInt(strArr2[0]);
				int chr2 = Integer.parseInt(strArr2[1]);
				int gene2 = Integer.parseInt(strArr2[2]);
				SequenceFileWriter.writeAndAppendLine(path, "MultiSpecSANC1.bl", speciesList.get(spec1).getChromosomes().get(chr1).getGeneList().get(gene1).getGeneID() + "\t" + speciesList.get(spec2).getChromosomes().get(chr2).getGeneList().get(gene2).getGeneID() + "\t" + strArr[2]);
			}
		} catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
}
