package scripts;

import java.util.ArrayList;

import miscellaneous.Sequence;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import miscellaneous.Species;

public class ExtractOriginalSequences {
	
	public static void main(String[] args) {
		SimpleFileIO reader = new SimpleFileIO();
		try {
			reader.readSequenceFile("/Users/rhali/", "GenFam.fasta");
			reader.readSyntenyFile("/Users/rhali/", "TestSynteny.hsf");
			reader.readSyntenyFile("/Users/rhali/", "TempRefSynteny.hsf");
			
			System.out.println("Reading data done");
			ArrayList<Species> specieslist = reader.getSpeciesList();
			ArrayList<Sequence> sequences = reader.getSequences();
			
			int start = 0;
			
			for(int i = 0; i < specieslist.size(); i++) {
				for (int j = 0; j < specieslist.get(i).getChromosomes().size(); j++) {
					for(int k = 0; k < specieslist.get(i).getChromosomes().get(j).getNumberOfGenes(); k++) {
						String seq = sequences.get(start).getSequence();
						start++;
						String header = specieslist.get(i).getChromosomes().get(j).getGeneList().get(k).getGeneID();
						Sequence sequence = new Sequence(header, seq);
						if(i<2)
							SequenceFileWriter.writeAndAppendSequenceInFasta("/Users/rhali/", "TestSequences.fasta", sequence);
						else
							SequenceFileWriter.writeAndAppendSequenceInFasta("/Users/rhali/", "RefSequences.fasta", sequence);
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
