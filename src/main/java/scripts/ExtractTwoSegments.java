package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class ExtractTwoSegments {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = "/Users/rhali/Documents/Eclipse/GenFamClust/Datasets/MultipleSpecies/SyntenyCorrScores/";
		String fileName = "RegionSyntenyScoresTotal.snc";
		String output = "RegionSyntenyScores1.snc";
//		String chr1 = "1.";
		String chr2 = "0.17";
		BufferedReader bufferedreader;
		String str;
		
		try {
			bufferedreader = new BufferedReader(new FileReader(path + fileName));
			
			while ((str = bufferedreader.readLine()) != null) {
				String[] strArr = str.split("\t");
//				if(strArr[0].startsWith(chr1) && strArr[1].startsWith(chr2)) 
				if(strArr[1].startsWith(chr2)) 
					SequenceFileWriter.writeAndAppendLine(path, output, str);
			}
		} catch (Exception e) {
			System.out.println("Error: ExtractSegment Failed " + e.getMessage());
			System.exit(-1);
		}
	}
}
