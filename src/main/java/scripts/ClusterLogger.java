package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class ClusterLogger {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String fileName = "genFam.log", path = "/Users/rhali/Documents/Eclipse/GenFamClust/Datasets//SubAverageClusters/", str;
		BufferedReader bufferedreader;
		try {
			bufferedreader = new BufferedReader(new FileReader(path + fileName));
			while ((str = bufferedreader.readLine()) != null) {
				String[] strArr = str.split("= ");
				if(!str.startsWith("\t"))
					SequenceFileWriter.writeAndAppendLine(path, "Clust.log", strArr[2]);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}

	}

}
