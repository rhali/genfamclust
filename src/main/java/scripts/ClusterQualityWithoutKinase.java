package scripts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class ClusterQualityWithoutKinase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] familySize = {10, 44, 6, 44, 78, 12, 56, 9, 22, 45, 8, 44, 38, 30, 31, 55, 12, 76, 38};
		for(int nc = 3; nc>= 3; nc--) {
			String method = "";
			String path = "";

			try {
				int l;
				l=0;
				for(; l <= 0; l++) {
/*					for(int m = 0; m < 3; m++) {
						if(m == 0) 
*/							method = "a";
/*						else if(m == 1) 
							method = "c";
						else 
							method = "s";
*/
						SequenceFileWriter.writeAndAppendLine("/Users/rhali/", "Quality2.txt", "");
						SequenceFileWriter.writeAndAppendLine("/Users/rhali/", "Quality2.txt", "Quality column for x" + nc + "y" + l + method);

						path = "/Volumes/MyBook/HashimData/HumanMouseAverage/x" + nc + "y" + l + method + "/";

						//			path = "/Users/rhali/Documents/Eclipse/GenFamClust/Datasets/MultipleSpecies/NCOnlyClusteringResults/Threshold0.025/HumanMouseAverageLinkage/";
						int totalClusterSize = 0;
						int currentCluster = 0;
						BufferedReader bufferedreader;
						String clusterFileName = "Cluster.bcl";
						String clusterFileName1 = "Cluster1.bcl";
						String str;

						bufferedreader = new BufferedReader(new FileReader(path + clusterFileName));
						while ((str = bufferedreader.readLine()) != null) { 
							String[] strArr = str.split("\t");
							boolean check = true;
							for(int i = 0; i < strArr.length; i++) {
								if(check == true) {
									if(Integer.parseInt(strArr[i]) < 6) {
										SequenceFileWriter.writeAndAppendString(path, clusterFileName1, strArr[i]);
										check = false;
									} else if (Integer.parseInt(strArr[i]) > 6) {
										int temp = Integer.parseInt(strArr[i]) - 1;
										SequenceFileWriter.writeAndAppendString(path, clusterFileName1, Integer.toString(temp));
										check = false;
									}
								} else {
									if(Integer.parseInt(strArr[i]) < 6) {
										SequenceFileWriter.writeAndAppendString(path, clusterFileName1, "\t" + strArr[i]);
										check = false;
									} else if (Integer.parseInt(strArr[i]) > 6) {
										int temp = Integer.parseInt(strArr[i]) - 1;
										SequenceFileWriter.writeAndAppendString(path, clusterFileName1, "\t" + Integer.toString(temp));
										check = false;
									}
								}
							}
							if(check == false) {
								SequenceFileWriter.writeAndAppendLine(path, clusterFileName1, "");
								totalClusterSize++;
							}
						}
						bufferedreader.close();

						double[][] fMatrix = new double[familySize.length][totalClusterSize];
						int[][] clusterFamilyCounter = new int[familySize.length][totalClusterSize];

						bufferedreader = new BufferedReader(new FileReader(path + clusterFileName1));
						while ((str = bufferedreader.readLine()) != null) { 
							for(int i = 0; i < familySize.length; i++) 
								clusterFamilyCounter[i][currentCluster] = 0;
							String[] strArr = str.split("\t");
							for(int i = 0; i < strArr.length; i++) 
								clusterFamilyCounter[Integer.parseInt(strArr[i])][currentCluster]++;
							for (int i = 0; i < familySize.length; i++) {
								double precision = (double)clusterFamilyCounter[i][currentCluster]/strArr.length;
								double recall = (double)clusterFamilyCounter[i][currentCluster]/(double)familySize[i];
								double fijValue = 0;
								if(precision != 0 || recall != 0)
									fijValue = ((double)2 * precision * recall)/(double)(precision + recall);
								fMatrix[i][currentCluster] = fijValue;
							}
							currentCluster++;
						}
						double[] fiArray = new double[familySize.length];
						double fScore = (double)0;

						for(int i = 0; i < familySize.length; i++) {
							double score = (double)0;
							for(int j = 0; j < totalClusterSize; j++) {
								fScore = fScore + (double)((double)clusterFamilyCounter[i][j] * fMatrix[i][j]/(double)658);
								score = score + (double)((double)clusterFamilyCounter[i][j] * fMatrix[i][j]/familySize[i]);
							}
							fiArray[i] = score;
						}

						SequenceFileWriter.writeAndAppendLine("/Users/rhali/", "Quality2.txt", String.valueOf(fScore));

						File temp = new File(path + clusterFileName1);
						temp.delete();
					} 
//				}
			} catch (Exception e) {
				System.out.println("ClusterQualityEvaluator Failed : " + e.getMessage()); 
				System.exit(-1);
			}
		}		
	}
}