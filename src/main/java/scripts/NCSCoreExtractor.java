package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class NCSCoreExtractor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String fileName = "SyntenyCorr_ms1.snc", path = "/Users/rhali/Documents/Eclipse/GenFamClust/Datasets/MultipleSpecies/SyntenyCorrScores/", str;
		BufferedReader bufferedreader;
		try {
			bufferedreader = new BufferedReader(new FileReader(path + fileName));
			while ((str = bufferedreader.readLine()) != null) {
				String[] strArr = str.split("\t");
				SequenceFileWriter.writeAndAppendLine(path, "NCScore.hef", strArr[0] + "\t" + strArr[1] + "\t" + strArr[2]);
			}
		} catch (Exception e) {
			System.out.println("File not found." + e.getMessage());
		}
	}
}
