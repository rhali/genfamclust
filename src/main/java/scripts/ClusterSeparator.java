package scripts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import miscellaneous.SequenceFileWriter;

public class ClusterSeparator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for(int nc = 3; nc>= 3; nc--) {
			String method = "";
			String path = "";

			try {
				int l;
				l=0;
				for(; l <= 0; l++) {
					System.out.println("Completed x" + nc + "y" + l);
//					for(int m = 0; m < 3; m++) {
//						if(m == 0) 
							method = "a";
/*						else if(m == 1) 
							method = "c";
						else 
							method = "s";
*/
						path = "/Volumes/MyBook/HashimData/HumanMouseAverage/x" + nc + "y" + l + method + "/";

		//				path = "/Users/rhali/Desktop/x5y8a/";
						String mapFileName = "TestToIndexMapping.map", str;
						String graphFileName = "genFam.gexf";
						String logFileName = "Cluster.bcl";
						int count = -1;
						BufferedReader bufferedreader;
						ArrayList<String> lines = new ArrayList<String>();
						ArrayList<ArrayList<String>> families = new ArrayList<ArrayList<String>>();
						String currentFamily = "";
						bufferedreader = new BufferedReader(new FileReader(path + mapFileName));
						while ((str = bufferedreader.readLine()) != null) {
							String[] strArr = str.split("\t");
							if(currentFamily.equals(strArr[2])) {
								families.get(count).add(strArr[1]);
							} else {
								ArrayList<String> family = new ArrayList<String>();
								family.add(strArr[1]);
								families.add(family);
								currentFamily = strArr[2];
								count++;
							}
						}

						bufferedreader.close();

						/*			for(int i = 0; i < families.size(); i++) 
							for(int j = 0; j < families.get(i).size(); j++)
								System.out.println(i + "." + j + "\t" + families.get(i).get(j));*/

						bufferedreader = new BufferedReader(new FileReader(path + graphFileName));
						while ((str = bufferedreader.readLine()) != null) {
							if(str.startsWith("<?xml") || str.startsWith("<gexf") || str.startsWith("\t<meta") || str.startsWith("\t\t<creator") || str.startsWith("\t\t<creator") || str.startsWith("\t</meta") || str.startsWith("</gexf")) {
								//								SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", str);
								continue;
							} else if(str.startsWith("\t\t</nodes")) {
								while (!(str = bufferedreader.readLine()).startsWith("\t\t</edges")) {
									continue;
								}
							} else if(!str.startsWith("\t</graph")) {
								lines.add(str);
							} else {
								lines.add(str);
								boolean check = false;

								outerloop:
									for(int i = 0; i < lines.size(); i++) {
										if(lines.get(i).startsWith("\t\t\t<node id=\"")) {
											for(int j = 0; j < families.size(); j++) {
												for(int k = 0; k < families.get(j).size(); k++) {
													if(lines.get(i).startsWith("\t\t\t<node id=\"" + families.get(j).get(k) + "\"")) {
														System.out.println("Cluster member " + families.get(j).get(k) + " found");
														check = true;
														break outerloop;
													}
												}
											}
										}
									}

								if(check == true) {
									for(int i = 0; i < lines.size(); i++) {
										boolean checker = false;
										if(lines.get(i).startsWith("\t\t\t<node id=\"")) {
											outerloop:
												for(int j = 0; j < families.size(); j++) {
													for(int k = 0; k < families.get(j).size(); k++) {
														if(lines.get(i).startsWith("\t\t\t<node id=\"" + families.get(j).get(k) + "\"")) {
															SequenceFileWriter.writeAndAppendString(path, logFileName, j + "\t");
															//															SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", lines.get(i));
															i++;
															//															SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", lines.get(i));
															i++;
															//															SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", "\t\t\t\t<viz:size value=\"50\"></viz:size>");
															i++;
															//															SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", lines.get(i));
															i++;
															//															SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", "\t\t\t\t<viz:color r=\"" + red + "\" g=\"" + green + "\" b=\"" + blue + "\"></viz:color>");
															checker = true;
															break outerloop;
														}
													}
												}
										if(checker == false) {
											//											SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", lines.get(i));
											i++;
											//											SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", lines.get(i));
											i++;
											//											SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", "\t\t\t\t<viz:size value=\"10\"></viz:size>");
											i++;
											//											SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", lines.get(i));
											i++;
											//											SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", "\t\t\t\t<viz:color r=\"" + 255 + "\" g=\"" + 255 + "\" b=\"" + 255 + "\"></viz:color>");
										}
										} else {
											//											SequenceFileWriter.writeAndAppendLine(path, "TestClusters.gexf", lines.get(i));
										}
									}
									SequenceFileWriter.writeAndAppendLine(path, logFileName, "");
								}
								while(lines.size()!= 0) 
									lines.remove(0);
							} 
						}
						bufferedreader.close();
					}
//				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			}
		}
	}
}
