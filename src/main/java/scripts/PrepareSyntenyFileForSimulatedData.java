package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class PrepareSyntenyFileForSimulatedData {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int data = 9;
		String path = "/Users/rhali/Desktop/Simulations/TransSimulation_" + data + "/DB/";
		String fileName = "Data" + data + "Sequences.fa";
		BufferedReader bufferedreader;
		String str;

		try {
			bufferedreader = new BufferedReader(new FileReader(path + fileName));
			while ((str = bufferedreader.readLine()) != null) { 
				if(str.startsWith(">")) {
					String[] strArr = str.split(",");
					String[] strArr1 = strArr[0].split("_");
					String[] strArr2 = strArr[2].split(" ");
					SequenceFileWriter.writeAndAppendLine(path, "Data" + data + "Synteny.hsf", strArr1[1] + "\t1\t" + strArr1[1] + "_" + strArr1[0].substring(1) + "\t" + strArr2[2]);
				}
			}
			bufferedreader.close();
			System.out.println("Completed Writing Synteny file");

		} catch (Exception e) {
			System.out.println("Failed: " + e.getMessage());
			System.exit(-1);
		}

	}

}
