package scripts;

import java.io.File;

import miscellaneous.SequenceFileWriter;

public class CreateDirectory {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (int nc = 9;nc > 3; nc--) {
			String method = "";
			try {
				for(int i = 1; i <= 10; i++) {
					//				for(int j = 0; j < 3; j++) {
					String fileName;
					//					if(j == 0) 
					method = "a";
					/*					else if(j == 1) 
						method = "c";
					else 
						method = "s";
					 */					
					String path = "/Volumes/MyBook/HashimData/HumanMouseAverage/x" + nc + "y" + i + method + "/";
					File theDir = new File(path);

					// if the directory does not exist, create it
					if (!theDir.exists())
					{
						System.out.println("creating directory: " + "x" + nc + "y" + i + method + "/");
						boolean result = theDir.mkdir();  
						if(result){    
							System.out.println("DIR created");  
						}

					}
					fileName = "test.sh";

					SequenceFileWriter.writeAndAppendLine(path, fileName, "#!/bin/bash\n");

					SequenceFileWriter.writeAndAppendLine(path, fileName, "#SBATCH -A b2012160");
					SequenceFileWriter.writeAndAppendLine(path, fileName, "#SBATCH -p core");
					SequenceFileWriter.writeAndAppendLine(path, fileName, "#SBATCH -n 1");
					SequenceFileWriter.writeAndAppendLine(path, fileName, "#SBATCH -t 50:00:00");
					SequenceFileWriter.writeAndAppendLine(path, fileName, "#SBATCH -J x" + nc + "y" + i + method);

					SequenceFileWriter.writeAndAppendLine(path, fileName, "cd /bubo/home/h4/raja/private/x" + nc + "y" + i + method);
					double temp = (double)i/(double)10;
					double temp2 = (double)nc/(double)10;

					if(temp == (double)1 && temp2 != (double)1)
						SequenceFileWriter.writeAndAppendLine(path, fileName, "java -Xmx3000m -Xms2800m -Xss100m -jar genfamclust-0.0.1-SNAPSHOT.jar SparseClusterer TestSynteny.hsf a" + temp2 + "b" + 1 + ".eva -m " + method + " -t 0.75");
					else if(temp != (double)1 && temp2 != (double)1)
						SequenceFileWriter.writeAndAppendLine(path, fileName, "java -Xmx3000m -Xms2800m -Xss100m -jar genfamclust-0.0.1-SNAPSHOT.jar SparseClusterer TestSynteny.hsf a" + temp2 + "b" + temp + ".eva -m " + method + " -t 0.75");
					else if(temp == (double)1 && temp2 == (double)1)
						SequenceFileWriter.writeAndAppendLine(path, fileName, "java -Xmx3000m -Xms2800m -Xss100m -jar genfamclust-0.0.1-SNAPSHOT.jar SparseClusterer TestSynteny.hsf a" + 1 + "b" + 1 + ".eva -m " + method + " -t 0.75");
					else if(temp != (double)1 && temp2 == (double)1)
						SequenceFileWriter.writeAndAppendLine(path, fileName, "java -Xmx3000m -Xms2800m -Xss100m -jar genfamclust-0.0.1-SNAPSHOT.jar SparseClusterer TestSynteny.hsf a" + 1 + "b" + temp + ".eva -m " + method + " -t 0.75");

				}
				//			}
			} catch (Exception e) {
				System.out.println("Write Failed: " + e.getMessage());
				System.exit(-1);
			}
		}
	}
}


