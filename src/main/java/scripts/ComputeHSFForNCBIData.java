package scripts;

import java.util.ArrayList;

import miscellaneous.Sequence;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;

public class ComputeHSFForNCBIData {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		String[] species = {"Acetobacter_pasteurianus", "Acidiphilium_cryptum", "Acidiphilium_multivorum", "Agrobacterium_tumefaciens", "Anaplasma_centrale", "Anaplasma_marginale", "Anaplasma_phagocytophilum", "Azorhizobium", "Azospirillum_brasilense", "Azospirillum_lipoferum", "Bartonella_bacilliformis", "Bartonella_henselae", "Bartonella_quintana", "Beijerinckia", "Bradyrhizobium_japonicum", "Bradyrhizobium_sp_BTAi1", "Brucella_abortus", "Brucella_canis", "Brucella_melitensis", "Brucella_ovis", "Brucella_suis", "Caulobacter_crescentus_CB15", "Ehrlichia_canis", "Ehrlichia_chaffeensis", "Ehrlichia_ruminantium", "Gluconobacter_oxydans", "Hyphomicrobium_denitrificans", "Hyphomonas_neptunium", "Magnetospirillum_magneticum", "Mesorhizobium_australicum", "Mesorhizobium_ciceri", "Mesorhizobium_loti", "Mesorhizobium_opportunistum", "Methylobacterium_extorquens", "Methylocella", "Neorickettsia_risticii", "Neorickettsia_sennetsu", "Nitrobacter_hamburgensis_X14", "Nitrobacter_winogradskyi", "Ochrobactrum", "Orientia_tsutsugamushi", "Paracoccus_denitrificans", "Rhizobium_etli", "Rhizobium_leguminosarum", "Rhizobium_tropici", "Rhodobacter_capsulatus", "Rhodobacter_sphaeroides", "Rhodomicrobium", "Rhodopseudomonas_palustris_CGA009", "Rhodospirillum_centenum", "Rhodospirillum_photometricum", "Rhodospirillum_rubrum", "Rickettsia_akari", "Rickettsia_conorii", "Rickettsia_felis", "Rickettsia_prowazekii", "Rickettsia_rickettsii", "Rickettsia_typhi", "Roseobacter_denitrificans", "Roseobacter_litoralis", "Sinorhizobium_fredii", "Sinorhizobium_meliloti", "Sphingobium_japonicum", "Sphingomonas_wittichii", "Sphingopyxis_alaskensis", "Wolbachia", "Xanthobacter", "Zymomonas_mobilis"};
//		Integer[] chr = {7, 9, 9, 5, 1, 1, 1, 1, 7, 7, 1, 1, 1, 3, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 6, 1, 1, 1, 1, 2, 3, 1, 1, 1, 1, 1, 4, 1, 2, 1, 3, 7, 7, 4, 2, 7, 1, 2, 1, 1, 2, 1, 1, 3, 1, 1, 1, 5, 4, 3, 3, 5, 3, 2, 1, 2, 7};

		String[] species = {"AgrobacteriumTumefaciens", "BartonellaHenselae", "BartonellaQuintana", "BradyrhizobiumJaponicum", "BrucellaMelitensis", "BrucellaSuis", "CaulobacterCrescentus", "MesorhizobiumLoti", "RhodopseudomonasPalustris", "RickettsiaConorii", "RickettsiaProwazekii", "RickettsiaRickettsii", "SinorhizobiumMeliloti", "Wolbachia1"};
		Integer[] chr = {5, 1, 1, 1, 2, 2, 1, 3, 2, 1, 1, 1, 3, 1};

		try {
			for(int j = 0; j < species.length; j++) {
				for(int k = 0; k < chr[j]; k++) {
					SimpleFileIO reader = new SimpleFileIO();
					int l = k + 1;
					reader.readSequenceFile("./Datasets/Alphaproteobacteria/" + species[j] + "/", "Chr" + l + "prot.txt");
					ArrayList<Sequence> sequences = reader.getSequences();
					for(int i = 0; i < sequences.size(); i++) {
						String header = sequences.get(i).getIdentifier();
						String seq = sequences.get(i).getSequence();
						String[] strArr = header.split(" ");
						String identifier = strArr[0];
						Sequence sequence = new Sequence(identifier, seq);
						String startpos = strArr[strArr.length-1];
						startpos = startpos.substring(10);

						if(startpos.startsWith("complement")) 
							startpos = startpos.substring(11);
						
						if(startpos.startsWith("join"))
							startpos = startpos.substring(5);
						
						if(startpos.startsWith("<"))
							startpos = startpos.substring(1);
						
						String[] startArr = startpos.split("\\.\\.");
						startpos = startArr[0];
						SequenceFileWriter.writeAndAppendLine("./Results/APB/", "QuerySynteny.hsf", species[j] + "\t" + l + "\t" + identifier + "\t" + startpos);
						SequenceFileWriter.writeAndAppendSequenceInFasta("./Results/APB/", "QuerySequences.fasta", sequence);
					}
					reader.emptySequences();
					reader = null;
				}
				int l = j + 1;
				System.out.println("Species Done = " + l);
			}
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			System.exit(-1);
		}

	}

}
