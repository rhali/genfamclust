package scripts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import miscellaneous.Sequence;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;

public class PrepareSequenceFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int data = 9;
		String path = "/Users/rhali/Desktop/Simulations/TransSimulation_" + data + "/DB/";
		String fileName = "Data" + data + "Synteny.hsf";
		BufferedReader bufferedreader;
		String str;
		ArrayList<String> species = new ArrayList<String>();
		ArrayList<String> gene = new ArrayList<String>();
		ArrayList<String> locus = new ArrayList<String>();
		try {
			bufferedreader = new BufferedReader(new FileReader(path + fileName));
			while ((str = bufferedreader.readLine()) != null) { 
					String[] strArr = str.split("\t");
					species.add(strArr[0]);
					gene.add(strArr[2]);
					locus.add(strArr[3]);
			}
			bufferedreader.close();
			SimpleFileIO reader = new SimpleFileIO();
			reader.readSequenceFile(path, "Data" + data + "Sequences.fa");
			ArrayList<Sequence> sequences = reader.getSequences();
			System.out.println("Sequences size is " + sequences.size());
			ArrayList<Sequence> sequences1 = new ArrayList<Sequence>();
			
			for(int i = 0; i < sequences.size(); i++) {
				String header = sequences.get(i).getIdentifier();
				String[] strArr = header.split(",");
				String[] strArr1 = strArr[0].split("_");
				sequences1.add(new Sequence(strArr1[1] + "_" + strArr1[0], sequences.get(i).getSequence()));
			}
			System.out.println("Sequences1 size is " + sequences1.size());
			System.out.println(gene.get(0));
			System.out.println(sequences1.get(0).getIdentifier());
			
			for(int i = 0; i < gene.size(); i++) {
				for(int j = 0; j < sequences1.size(); j++) {
					String header = sequences1.get(j).getIdentifier();
					if(header.equals(gene.get(i))) {
						SequenceFileWriter.writeAndAppendSequenceInFasta(path, "Data" + data + "Sequences.fasta", sequences1.get(j));
						break;
					}
				}
			}
			System.out.println("Completed Writing Synteny file");

		} catch (Exception e) {
			System.out.println("Failed: " + e.getMessage());
			System.exit(-1);
		}
	}

}
