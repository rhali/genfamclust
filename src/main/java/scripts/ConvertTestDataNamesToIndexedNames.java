package scripts;

import java.io.BufferedReader;
import java.io.FileReader;

import miscellaneous.SequenceFileWriter;

public class ConvertTestDataNamesToIndexedNames {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			String path = "/Users/rhali/Desktop/Projects/GenFamClust2/Datasets/Multi-Species/BlastResults/", str;
			BufferedReader bufferedreader;
			BufferedReader bufferedreader1;
			bufferedreader = new BufferedReader(new FileReader(path + "MultiSpecSANCComplete.bl"));
			bufferedreader1 = new BufferedReader(new FileReader(path + "MultiSpecSANC.bl"));
			while ((str = bufferedreader.readLine()) != null) {
				String str1 = bufferedreader1.readLine();
				String[] strArr = str.split("\t");
				String[] strArr1 = str1.split(" ");
				SequenceFileWriter.writeAndAppendLine(path, "MultiSpecSANCComplete1.bl", strArr1[0] + "\t" + strArr1[1] + "\t" + strArr[2] + "\t" + strArr[3] + "\t" + strArr[4] + "\t" +strArr[5] + "\t" +strArr[6] + "\t" +strArr[7] + "\t" +strArr[8] + "\t" +strArr[9] + "\t" +strArr[10] + "\t" +strArr[11]);
			}
		} catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}


	}

}
