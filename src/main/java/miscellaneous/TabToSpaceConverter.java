package miscellaneous;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * 
 * @author Raja Hashim Ali
 *
 */
public class TabToSpaceConverter {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	String oldFileName;
	String newFileName;
	String path;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public TabToSpaceConverter(String path, String oldFileName, String newFileName) {
		this.oldFileName = oldFileName;
		this.newFileName = newFileName;
		this.path = path;
	}
	
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public String[] makeCommand() {
		String[] command = {"sed", "$'s/\t\t*/ /g'", path + oldFileName};
		return command;
	}
	
	public String[] makeCommand1() {
		String[] command = {"gsed", "-i", "s/\t\t*/ /g", path + oldFileName};
		return command;
	}

	public boolean runCommand(String[] command) {
		int 				exitVal;
		String 				line;
		Runtime 			runtime;
		Process 			process;
		InputStream 		stderr;
		BufferedReader 		bufferedreader;
		InputStreamReader 	inputstreamreader;
				
		try {
			System.out.println("Converting tabbed file into spaced file ... Please wait");
			runtime = Runtime.getRuntime();
			process = runtime.exec(command, null, null);
			exitVal = process.waitFor();
			if(exitVal == 0) {
				System.out.println("sed found.");
				File file1 = new File(path + oldFileName);
				File file2 = new File(path + newFileName);
				if(file2.exists()) {
					System.out.println("File " + newFileName + " already exists. Can not rename.");
					return false;
				}
				else {
					if(file1.renameTo(file2) == true)
						System.out.println("File " + oldFileName + " renamed to " + newFileName);
					else
						System.out.println("File " + oldFileName + " could not be renamed.");
					return false;
				}
			} else {
				stderr = process.getErrorStream();
				inputstreamreader = new InputStreamReader(stderr);
				bufferedreader = new BufferedReader(inputstreamreader);
	            while ( (line = bufferedreader.readLine()) != null)
	                System.out.println(line);
				bufferedreader.close();
				
				System.out.println("sed not found. Trying to find and run gsed now.");
				command = makeCommand1();
				
				runtime = Runtime.getRuntime();
				process = runtime.exec(command, null, null);
				exitVal = process.waitFor();
				if(exitVal == 0) {
					System.out.println("gsed found.");
					File file1 = new File(path + oldFileName);
					File file2 = new File(path + newFileName);
					if(file2.exists()) {
						System.out.println("File " + newFileName + " already exists. Can not rename.");
						return false;
					}
					else {
						if(file1.renameTo(file2) == true)
							System.out.println("File " + oldFileName + " renamed to " + newFileName);
						else
							System.out.println("File " + oldFileName + " could not be renamed.");
						return false;
					}
				} 
			}
		} catch(Exception e) {
			System.out.println("gsed also not found. Running the slower java implementation now. Please wait.");
			try {
				int lines = countLines(new File(path + oldFileName));
				
				bufferedreader = new BufferedReader(new FileReader(path + oldFileName));
				StringBuilder output = new StringBuilder("");
				int count = 0;
				int totalcount = 0;
				while ( (line = bufferedreader.readLine()) != null) {
					count++;
					String[] strArr = line.split("\t");
					output.append(strArr[0] + " " + strArr[1] + " " + strArr[2] + "\n");
					if(lines/10<count) {
						System.out.println("Percentage lines converted = " + ((float)(totalcount)/lines*100));
						SequenceFileWriter.writeAndAppendString(path, newFileName, output.toString());
						output = new StringBuilder("");
						totalcount=totalcount+count;
						count = 0;
					}
				}
				SequenceFileWriter.writeAndAppendString(path, newFileName, output.toString());
				bufferedreader.close();
				return true;
			}  catch(Exception e1) {
				System.out.println("Error in converting tabbed file to spaced file for NC: " + e.getMessage());
				System.exit(-1);
				return false;
			}
		}
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df.format(cal.getTime()));
		System.out.println("Conversion Completed");		
		return true;
	}
	
	public static int countLines(File aFile) throws Exception {
	    LineNumberReader reader = null;
	    try {
	        reader = new LineNumberReader(new FileReader(aFile));
	        while ((reader.readLine()) != null);
	        return reader.getLineNumber();
	    } catch (Exception ex) {
	        return -1;
	    } finally { 
	        if(reader != null) 
	            reader.close();
	    }
	}
	
	public static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

}
