package miscellaneous;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * 
 * @author Raja Hashim Ali
 *
 */
abstract public class SequenceFileWriter {
	/*'******************************************************************************
	 * 							CLASS PUBLIC FUNCTIONS								*
	 ********************************************************************************/
	public static void writeSequenceInFasta(String path, String fileName, Sequence sequence) throws Exception {
		BufferedWriter out;
		FileWriter fstream;
		path = path.concat(fileName);
		fstream = new FileWriter(path);
		out = new BufferedWriter(fstream);
		try {
			out.write(">");
			out.write(sequence.getIdentifier());
			out.write("\n");
			out.write(sequence.getSequence());
			out.write("\n");
		} finally {
			try {
				out.flush();
				out.close();
			} catch (Exception e) {
				System.out.println("writeSequenceInFasta Failed: "+ e.getMessage());
				System.exit(-1);
			}
		} 
	}

	public static void writeAndAppendSequenceInFasta(String path, String fileName, Sequence sequence) throws Exception{
		path 				= path.concat(fileName);
		FileWriter fstream 	= new FileWriter(path, true);
		BufferedWriter out 	= new BufferedWriter(fstream);

		try {
			out.write(">");
			out.write(sequence.getIdentifier());
			out.write("\n");
			out.write(sequence.getSequence());
			out.write("\n");
		} finally {
			try {
				out.flush();
				out.close();
			} catch (Exception e) {
				System.out.println("writeSequenceInFasta Failed: "+ e.getMessage());
				System.exit(-1);
			}
		} 
	}

	public static void writeAndAppendString(String path, String fileName, String string) throws Exception {
		path 				= path.concat(fileName);
		FileWriter fstream 	= new FileWriter(path, true);
		BufferedWriter out 	= new BufferedWriter(fstream);

		try {
			out.write(string);
		} finally {
			try {
				out.flush();
				out.close();
			} catch (Exception e) {
				System.out.println("writeSequenceInFasta Failed: "+ e.getMessage());
				System.exit(-1);
			}
		} 
	}

	public static void writeAndAppendLine(String path, String fileName, String string) throws Exception {
		path 				= path.concat(fileName);
		FileWriter fstream 	= new FileWriter(path, true);
		BufferedWriter out 	= new BufferedWriter(fstream);

		try{
			out.write(string + "\n");
		} finally {
			try {
				out.flush();
				out.close();
			} catch (Exception e) {
				System.out.println("writeSequenceInFasta Failed: "+ e.getMessage());
				System.exit(-1);
			}
		} 
	}

	/*'******************************************************************************
	 * 							END OF CLASS										*
	 ********************************************************************************/
}
