package miscellaneous;

import java.util.ArrayList;

/**
 * @author Raja Hashim Ali
 */
public interface ClusterLinkageInterface {
	public void method(String path, String refSyntenyFile, String querySyntenyFile, String evaluationFile, String output, Float threshold, boolean graphics) throws Exception;
	public void performLinkage(Float threshold, Integer run);
	public void updateCluster(Integer xIndex, Integer yIndex);
	public int addEdgesforCluster(String path, String output, ArrayList<String> members, float threshold, int edgeCount) throws Exception;
	public Triple<Integer, Integer, Float> getMinimum();
	public void printCluster(String path, Integer xIndex, Integer yIndex);
}
