package miscellaneous;

import java.util.ArrayList;

public class Cluster {
	private int index;
	ArrayList<String> members;
	ArrayList<Couple<Integer, Float>> evalList;
	
	public Cluster(int index) {
		members = new ArrayList<String>();
		evalList = new ArrayList<Couple<Integer, Float>>();
		this.index = index;
	}
	
	public void addEvalScore(Couple<Integer, Float> evalHit) {
		evalList.add(evalHit);
	}
	
	public ArrayList<Couple<Integer, Float>> getEvalScore() {
		return evalList;
	}
	
	public void addMember(String member) {
		members.add(member);
	}
	
	public ArrayList<String> getMembers() {
		return members;
	}
	
	public int getIndex() {
		return index;
	}
	
	public Couple<Integer, Float> removeEvalListMember(int index) {
		return evalList.remove(index);
	}
	
	public void removeClusterWithTrace() {
		while(members.size() > 0) 
			members.remove(0);
		while(evalList.size() > 0) 
			evalList.remove(0);
	}
	
	public int getNumberOfMembers() {
		return members.size();
	}
}
