package miscellaneous;

/**
 * A class for storing and manipulating protein sequences in fasta format. A sequence 
 * in fasta format has an Identifier/name and its amino acid sequence. THe class
 * provides attributes and methods for all data and operations related to a protein
 * sequence.
 * @author Raja Hashim Ali
 *
 */
public class Sequence {
	/*'******************************************************************************
	 * 							CLASS VARIABLES										*
	 ********************************************************************************/
	/** The amino acid sequence of this protein */
	private String aminoacidSequence;
	
	/** The identifier of this protein. Also called as header */
	private String identifier;
	
	/** The length of the amino acid sequence of this protein */
	private int length;
	
	/*'******************************************************************************
	 * 							CLASS CONSTRUCTORS									*
	 ********************************************************************************/
	public Sequence(String identifier, String aminoacidSequence) {
		this.aminoacidSequence 	= aminoacidSequence;
		this.identifier 		= identifier;
		this.length 			= aminoacidSequence.length();
	}
	
	/*'******************************************************************************
	 * 							CLASS PUBLIC FUNCTIONS								*
	 ********************************************************************************/
	public String getSequence(){
		if(aminoacidSequence != null)
			return aminoacidSequence;
		else{
			System.out.println("No sequence found Sequence call failed.");
			return null;
		}
	}
	
	public String getIdentifier(){
		if(identifier != null)
			return identifier;
		else{
			System.out.println("No header found. Identifier call failed.");
			return null;
		}
	}
	
	public int getLength() { return length; }
	
	public void displaySequence() { System.out.println(">" + identifier + "\n" + aminoacidSequence); }
	
	public Sequence getReversedSequence() {
		StringBuffer buffer = new StringBuffer(aminoacidSequence);  
		buffer = buffer.reverse();    
		String reverseString = buffer.toString();  
		Sequence newSeq = new Sequence("Rev" + identifier, reverseString);
		return newSeq;
	}
	
	/*'******************************************************************************
	 * 							END OF CLASS										*
	 ********************************************************************************/
}
