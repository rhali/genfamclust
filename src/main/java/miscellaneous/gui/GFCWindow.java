package miscellaneous.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.filechooser.FileFilter;

import miscellaneous.Sequence;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import modules.blastModule.BlastModule;
import modules.dataPostprocess.PostProcessData;
import modules.dataPrepModule.DataPreparator;
import modules.evaluator.Evaluator;
import modules.hcluster.Hcluster;
import modules.hifix.Hifix;
import modules.ncCompModule.NeighborhoodCorrelation;
import modules.silix.Silix;
import modules.sparseClusterer.SparseClusterer;
import modules.syntenyCorrelationScore.SyntenyCorrelationScore;
import modules.syntenyExtractorModule.SyntenyExtractor;

public class GFCWindow extends JFrame {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private GFCWindow window;
	private static final long 	serialVersionUID = 1L;
	
	private JPanel 				defaultpanel = null;
	protected JPanel 			westpanel = null;
	protected JPanel 			eastpanel = null;
	protected JPanel 			southpanel = null;
	private JPanel 				panel = null;
	
	private GFCDisplayPanel 	analysisPanel = null;
	private GFCDisplayPanel 	msaPanel = null;
	private GFCDisplayPanel 	NCPanel = null;
	private GFCDisplayPanel 	silixPanel = null;
	private GFCDisplayPanel 	mclPanel = null;
	private GFCDisplayPanel 	hclusterPanel = null;
	
	@SuppressWarnings("rawtypes")
	private JComboBox 			droplist = new JComboBox();
	@SuppressWarnings("rawtypes")
	private JComboBox 			modulepanel = new JComboBox();
	@SuppressWarnings("rawtypes")
	private JComboBox 			blasttype = new JComboBox();
	@SuppressWarnings("rawtypes")
	private JComboBox 			clustermethod = new JComboBox();
	
	private String 				outputName = "";
	private String 				blastevalue = "10";
	private String 				ncthreshold = "0.50";
	private String 				evaathreshold = "0.50";
	private String 				evabthreshold = "1.0";
	private String 				clusterthreshold = "0.50";
	private String 				silixpercentidentity = "0.35";
	private String 				silixminpercentoverlap = "0.80";
	private String 				mclinflation = "2.0";
	private String 				hclusterminedgeweight = "20";
	private String 				hclusterminedgedensity = "0.50";
	private String 				hclustermaxsize = "500";
	private String 				hclusterbreakingedgedensity = "0.10";	
	private String 				clusteringmethod = "a";
	private String 				moduleName = "dataprocessor";
	private String 				neighborhoodsize = "5";
	private String				progName = "GFC";
	
	private File 				path = null;
	private File 				blastresourcepath = null;
	private File 				ncresourcepath = null;
	private File 				silixresourcepath = null;
	private File 				mclresourcepath = null;
	private File 				hclusterresourcepath = null;
	private File 				qsynfile = null;
	private File 				qseqfile = null;
	
	private File 				rsynfile = null;
	private File 				rseqfile = null;
	private File 				avablastfile = null;
	private File 				ncfile = null;
	private File 				syntenyscorefile = null;
	private File 				syntenycorrelationfile = null;
	private File 				homologyevaluatorfile = null;
	private File 				genefamilyfile = null;
	
	private Lock 				displaypanelLock = null;
	private ArrayList<String>	displayTextEastPanel = null;
	private JScrollPane			eastScrollPane = null;
	
	private JButton 			runProgram = new JButton("Execute!");
	private JButton 			helpButton = new JButton("Help");
	private JButton 			querySyntenyButton = new JButton("Browse!");
	private JButton 			querySequenceButton = new JButton("Browse!");
	private JButton 			browseOutputDirButton = new JButton("Browse!");
	private JButton 			browseBlastResourceDirButton = new JButton("Browse!");
	private JButton 			browseNCResourceDirButton = new JButton("Browse!");
	private JButton 			browseSiLiXResourceDirButton = new JButton("Browse!");
	private JButton 			browseMCLResourceDirButton = new JButton("Browse!");
	private JButton 			browsehclusterResourceDirButton = new JButton("Browse!");
	private JButton 			refSyntenyButton = new JButton("Browse!");
	private JButton 			refSequenceButton = new JButton("Browse!");
	private JButton 			avaBlastButton = new JButton("Browse!");
	private JButton 			ncButton = new JButton("Browse!");
	private JButton 			syntenyextractorButton = new JButton("Browse!");
	private JButton 			syntenycorrelationButton = new JButton("Browse!");
	private JButton 			homologyevaluatorButton = new JButton("Browse!");
	private JButton 			genefamilyButton = new JButton("Browse!");
	
	private boolean 			proteinblast = true;
	
	private final JTextField 	blastthresholdlabel = new JTextField();
	private final JTextField 	ncthresholdlabel = new JTextField();
	private final JTextField 	evaathresholdlabel = new JTextField();
	private final JTextField 	evabthresholdlabel = new JTextField();
	private final JTextField 	clusterthresholdlabel = new JTextField();
	private final JTextField 	silixpercentidentitylabel = new JTextField();
	private final JTextField 	silixminpercentoverlaplabel = new JTextField();
	private final JTextField 	mclinflationlabel = new JTextField();
	private final JTextField 	hclusterminedgeweightlabel = new JTextField();
	private final JTextField 	hclusterminedgedensitylabel = new JTextField();
	private final JTextField 	hclustermaxsizelabel = new JTextField();
	private final JTextField 	hclusterbreakingedgedensitylabel = new JTextField();
	private final JTextField 	neighborhoodsizelabel = new JTextField();
	private final JTextField 	outputFileName = new JTextField();	//Text field for bayesian confidence level
	
	private Thread[] 			workerThreads;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public GFCWindow(String programName) {	
		workerThreads = new Thread[1];

		for(int i=0; i<workerThreads.length; i++)
			workerThreads[i] = new Thread();

		displaypanelLock = new ReentrantLock();
		defaultpanel = new JPanel();

		defaultpanel.setLayout(new BorderLayout());
		defaultpanel.setBackground(new Color(0xFFEEEEFF));

		westpanel 		= new JPanel();
		eastpanel 		= new JPanel();
		southpanel 		= new JPanel();

		if(programName.equals("GFC")) {
			progName = "GFC";
			getEastPanelGFC();
			westpanel.add(getWestPanelsGFC());
			initDefaultButtonsGFC();
		} else if(programName.equals("NC")) {
			getEastPanelNC();
			westpanel.add(getWestPanelsNC());
			initDefaultButtonsNC();
		}
		
		westpanel.setLayout(new FlowLayout());
		westpanel.setMinimumSize(new Dimension(620, 0));
		westpanel.setPreferredSize(new Dimension(620, 0));
		westpanel.setBackground(new Color(0xFFEEEEFF));

		eastpanel.setBackground(new Color(0xFFEEEEFF));
		eastpanel.setPreferredSize(new Dimension(620, 800));
		eastpanel.add(eastScrollPane);

		southpanel.setMinimumSize(new Dimension(0, 28));
		southpanel.setPreferredSize(new Dimension(0, 28));

		southpanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		southpanel.setBackground(new Color(0xFFEEEEFF));

		southpanel.add(Box.createHorizontalGlue());
		southpanel.add(runProgram);
		southpanel.add(helpButton);
		southpanel.add(Box.createRigidArea(new Dimension(10, 0)));
		
		defaultpanel.add(westpanel, BorderLayout.WEST);
		defaultpanel.add(eastpanel, BorderLayout.EAST);
		defaultpanel.add(southpanel, BorderLayout.SOUTH);		
	}
		
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void addMessageForReader(String msg) {displayTextEastPanel.add(msg);}
	public JPanel getMainPanel() { return defaultpanel; }
	
	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	/** CreateDisplayPanels: Creates and adds panels responsible showing, file information, statistics and burn in. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JPanel getWestPanelsGFC() {
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(600,480));
		panel.setMaximumSize(new Dimension(600,480));
		final String[] dm = {"Pre-processor", "All vs all Blast", "Neighborhood correlation", "Synteny extractor", "Synteny correlation", "Homology evaluator", "gene family inference", "post-processor"};
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(new Color(0xFFEEEEFF));

		String[] paramnames = {"Selected program:"};
		
		analysisPanel = new GFCDisplayPanel(paramnames, "Analysis Type");
		msaPanel = new GFCDisplayPanel("Options");

		outputFileName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(path == null) {
						displayTextEastPanel.add("[ERROR] : Can not set filename without setting output directory. Output directory is empty.");
						updateEastPanelText(3);
						return;
					}
					if(outputFileName.getText() == "") {
						displayTextEastPanel.add("[ERROR] : Empty file name entered. File name not changed.");
						updateEastPanelText(3);
						return;
					}
					File file = new File(path, File.separator + outputFileName.getText());
					if(file.exists()) {
						displayTextEastPanel.add("[WARNING] : File exists already and will be overwritten. File is - " + path + outputFileName.getText());
						updateEastPanelText(2);
						outputName = outputFileName.getText();
						return;
					}
					displayTextEastPanel.add("[INFO] - Output file path and name set to : " + file.getAbsolutePath());
					updateEastPanelText(1);
					outputName = outputFileName.getText();
					enableExecuteButton();
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Output file can not be created. Check name/permission/space. " + path + outputName);
					updateEastPanelText(3);
				}
			}
		});
		
		blastthresholdlabel.setText(blastevalue);
		blastthresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(blastthresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + blastthresholdlabel.getText());
						updateEastPanelText(3);
						blastthresholdlabel.setText(blastevalue);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + blastthresholdlabel.getText());
					updateEastPanelText(3);
					blastthresholdlabel.setText(blastevalue);
					return;
				}
				blastevalue = blastthresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - Threshold : " + blastevalue);
				updateEastPanelText(1);
			}
		});
		
		ncthresholdlabel.setText(ncthreshold);
		ncthresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(ncthresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + ncthresholdlabel.getText());
						updateEastPanelText(3);
						ncthresholdlabel.setText(ncthreshold);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (greater than 1.0) float, so not set. You provided :" + ncthresholdlabel.getText());
						updateEastPanelText(3);
						ncthresholdlabel.setText(ncthreshold);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + ncthresholdlabel.getText());
					updateEastPanelText(3);
					ncthresholdlabel.setText(ncthreshold);
					return;
				}
				ncthreshold = ncthresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - NC Threshold : " + ncthreshold);
				updateEastPanelText(1);
			}
		});
		
		neighborhoodsizelabel.setText(neighborhoodsize);
		neighborhoodsizelabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(neighborhoodsizelabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Neighborhood size is an invalid (negative) float, so not set. You provided :" + neighborhoodsizelabel.getText());
						updateEastPanelText(3);
						neighborhoodsizelabel.setText(neighborhoodsize);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Neighborhood sizeize is not a valid float, so not set. You provided :" + neighborhoodsizelabel.getText());
					updateEastPanelText(3);
					neighborhoodsizelabel.setText(neighborhoodsize);
					return;
				}
				neighborhoodsize = neighborhoodsizelabel.getText();
				displayTextEastPanel.add("[INFO] - Neighborhood size for synteny score computation is : " + neighborhoodsize);
				updateEastPanelText(1);
			}
		});
		
		evaathresholdlabel.setText(evaathreshold);
		evaathresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(evaathresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + evaathresholdlabel.getText());
						updateEastPanelText(3);
						evaathresholdlabel.setText(evaathreshold);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (greater than 1.0) float, so not set. You provided :" + evaathresholdlabel.getText());
						updateEastPanelText(3);
						evaathresholdlabel.setText(evaathreshold);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + evaathresholdlabel.getText());
					updateEastPanelText(3);
					evaathresholdlabel.setText(evaathreshold);
					return;
				}
				evaathreshold = evaathresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - Evaluation Similarity Threshold : " + evaathreshold);
				updateEastPanelText(1);
			}
		});
		
		evabthresholdlabel.setText(evabthreshold);
		evabthresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(evabthresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + evabthresholdlabel.getText());
						updateEastPanelText(3);
						evabthresholdlabel.setText(evabthreshold);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (greater than 1.0) float, so not set. You provided :" + evabthresholdlabel.getText());
						updateEastPanelText(3);
						evabthresholdlabel.setText(evabthreshold);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + evabthresholdlabel.getText());
					updateEastPanelText(3);
					evabthresholdlabel.setText(evabthreshold);
					return;
				}
				evabthreshold = evabthresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - Evaluation Synteny Threshold : " + evabthreshold);
				updateEastPanelText(1);
			}
		});

		clusterthresholdlabel.setText(clusterthreshold);
		clusterthresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(clusterthresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + clusterthresholdlabel.getText());
						updateEastPanelText(3);
						clusterthresholdlabel.setText(clusterthreshold);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (greater than 1.0) float, so not set. You provided :" + clusterthresholdlabel.getText());
						updateEastPanelText(3);
						clusterthresholdlabel.setText(clusterthreshold);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + clusterthresholdlabel.getText());
					updateEastPanelText(3);
					clusterthresholdlabel.setText(clusterthreshold);
					return;
				}
				clusterthreshold = clusterthresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - Clustering Threshold : " + clusterthreshold);
				updateEastPanelText(1);
			}
		});

        modulepanel.setBackground(Color.WHITE);
        modulepanel.setModel(new DefaultComboBoxModel(dm)); 
		ActionListener cbActionListener1 = new ActionListener() {//add actionlistner to listen for change
            public void actionPerformed(ActionEvent e) {

                Integer index = modulepanel.getSelectedIndex();//get the selected item

                switch (index) {//check for a match
                	case 0:
                		moduleName = "dataprocessor";
                		break;
                	case 1:
                		moduleName = "avablast";
                		break;
                	case 2:
                		moduleName = "nc";
                		break;
                	case 3:
                		moduleName = "sysc";
                        break;
                	case 4:
                		moduleName = "syc";
                        break;
                	case 5:
                		moduleName = "eva";
                        break;
                	case 6:
                		moduleName = "gfc";
                        break;
                	case 7:
                		moduleName = "postprocess";
                        break;
                }
                displayHelpForSelectedDistanceMethod();
            }
        };
        modulepanel.addActionListener(cbActionListener1);

		droplist.setBackground(Color.WHITE);
		String analysis[] = {"Complete GenFamClust Analysis", "Starting from a particular module", "Execute a particular module"};
		droplist.setModel(new DefaultComboBoxModel(analysis)); 
		ActionListener cbActionListener = new ActionListener() {//add actionlistner to listen for change
            public void actionPerformed(ActionEvent e) {

                Integer index = droplist.getSelectedIndex();//get the selected item

                switch (index) {//check for a match
                	case 0:
                		displayHelpForSelectedIndexGFC(0);
                		break;
                	case 1:
                		displayHelpForSelectedIndexGFC(1);
                		break;
                	case 2:
                		displayHelpForSelectedIndexGFC(2);
                		break;
                }
        		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
        		displayTextEastPanel.add(" NOTE : Whatever field is enabled, is compulsorily required except ref synteny and");
        		displayTextEastPanel.add(" sequences!");
        		displayTextEastPanel.add(" Required Arguments : (When ENABLED)");
        		displayTextEastPanel.add("                      1) Query synteny file (hsf format)");
        		displayTextEastPanel.add("                      2) Query sequence file (FASTA format)");
        		displayTextEastPanel.add("                      3) Output/input directory.");
        		displayTextEastPanel.add("                      4) Output filename.");
        		displayTextEastPanel.add("                      5) Blast resource path for finding makeblastdb and blastp/blastn. ");
        		displayTextEastPanel.add("                      6) Evalue limit for blast. ");
        		displayTextEastPanel.add("                      7) Blast type. ");
        		displayTextEastPanel.add("                      8) NC_Standalone resource path. ");
        		displayTextEastPanel.add("                      9) Threshold for Neighborhood Correlation (between 0 and 1). ");
        		displayTextEastPanel.add("                      10) Evaluate homology threshold a. ");
        		displayTextEastPanel.add("                      11) Evaluate homology threshold b. ");
        		displayTextEastPanel.add("                      12) Clustering method for combining families. ");	
        		displayTextEastPanel.add("                      13) Threshold for clustering. ");
        		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
        		displayTextEastPanel.add(" Optional Arguments : (When ENABLED)");
        		displayTextEastPanel.add("                      1) Reference synteny file (hsf format). (ALWAYS OPTIONAL).");
        		displayTextEastPanel.add("                      2) Reference sequence file (FASTA format). (ALWAYS OPTIONAL).");
        		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
        		updateEastPanelText(0);
            }
        };
        droplist.addActionListener(cbActionListener);

		blasttype.setBackground(Color.WHITE);
		String blastoptions[] = {"Protein", "Nucleotide"};
		blasttype.setModel(new DefaultComboBoxModel(blastoptions)); 
		ActionListener cbActionListener2 = new ActionListener() {//add actionlistner to listen for change
            public void actionPerformed(ActionEvent e) {

                Integer index = blasttype.getSelectedIndex();//get the selected item

                switch (index) {//check for a match
                	case 0:
                		proteinblast = true;
                		displayTextEastPanel.add("[INFO] - Blast for proteins (blastp) selected.");
                		updateEastPanelText(1);
                		break;
                	case 1:
                		proteinblast = false;
                		displayTextEastPanel.add("[INFO] - Blast for nucelotides (blastn) selected.");
                		updateEastPanelText(1);
                		break;
                }
            }
        };
        blasttype.addActionListener(cbActionListener2);
        
		clustermethod.setBackground(Color.WHITE);
		String clustermethodoptions[] = {"UPGMA", "SL", "CL"};
		clustermethod.setModel(new DefaultComboBoxModel(clustermethodoptions)); 
		ActionListener cbActionListener3 = new ActionListener() {//add actionlistner to listen for change
            public void actionPerformed(ActionEvent e) {

                Integer index = clustermethod.getSelectedIndex();//get the selected item

                switch (index) {//check for a match
                	case 0:
                		clusteringmethod = "a";
                		displayTextEastPanel.add("[INFO] - Average Linkage Clustering method selected.");
                		updateEastPanelText(1);
                		break;
                	case 1:
                		clusteringmethod = "s";
                		displayTextEastPanel.add("[INFO] - Single Linkage Clustering method selected.");
                		updateEastPanelText(1);
                		break;
                	case 2:
                		clusteringmethod = "c";
                		displayTextEastPanel.add("[INFO] - Complete Linkage Clustering method selected.");
                		updateEastPanelText(1);
                		break;
                }
            }
        };
        clustermethod.addActionListener(cbActionListener3);

		analysisPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		analysisPanel.add(droplist);
		analysisPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		analysisPanel.addComponent("Module Name:", modulepanel, new Dimension(600, 20));
		analysisPanel.add(Box.createRigidArea(new Dimension(0, 7)));

		msaPanel.addSpecialComponent("Choose query synteny file: ", new JLabel(), browseqSynButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Choose query sequence file: ", new JLabel(), browseqSeqButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Choose reference synteny file: ", new JLabel(), browserSynButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Choose reference sequence file: ", new JLabel(), browserSeqButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		msaPanel.addSpecialComponent("Choose output folder: ", new JLabel(), browseOutputDirButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent("Choose output file name:", outputFileName, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addSpecialComponent("Choose Blast resource folder: ", new JLabel(), browseBlastResourceDirButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent(" Blast E-value Threshold:", blastthresholdlabel, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent(" Type of Blast:", blasttype, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addSpecialComponent("Choose NC resource folder: ", new JLabel(), browseNCResourceDirButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent(" NC Threshold:", ncthresholdlabel, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addComponent(" Neighborhood size Threshold:", neighborhoodsizelabel, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addComponent(" Evaluation threshold - Similarity (a):", evaathresholdlabel, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent(" Evaluation threshold - Synteny (b):", evabthresholdlabel, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addComponent(" Clustering method:", clustermethod, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent(" Clustering distance threshold:", clusterthresholdlabel, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addSpecialComponent("All vs All blast file: ", new JLabel(), browseAvaBlastButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Neighborhood correlation score file: ", new JLabel(), browseNCButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Synteny score file: ", new JLabel(), browseSyntenyExtractorButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Synteny correlation score file: ", new JLabel(), browseSyntenyCorrelationButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Inferred homology file: ", new JLabel(), browseHomologyEvaluatorButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Inferred gene family file: ", new JLabel(), browseGeneFamilyButtonGFC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		
        analysisPanel.setToolTipText("Type of Analysis panel");
		msaPanel.setToolTipText("Other options panel");

		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(analysisPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(msaPanel);

		droplist.setSelectedIndex(0);
		return panel;
	}
	
	/** CreateDisplayPanels: Creates and adds panels responsible showing, file information, statistics and burn in. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JPanel getWestPanelsNC() {
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(600,480));
		panel.setMaximumSize(new Dimension(600,480));
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(new Color(0xFFEEEEFF));

		String[] paramnames = {"Selected program:"};
		
		analysisPanel = new GFCDisplayPanel(paramnames, "Gene family inference method");
		msaPanel = new GFCDisplayPanel("General options");
		NCPanel = new GFCDisplayPanel("Neighborhood correlation options");
		silixPanel = new GFCDisplayPanel("SiLiX/HiFiX options");
		mclPanel = new GFCDisplayPanel("Markov clustering options");
		hclusterPanel = new GFCDisplayPanel("hcluster_sg options");

		outputFileName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(path == null) {
						displayTextEastPanel.add("[ERROR] : Can not set filename without setting output directory. Output directory is empty.");
						updateEastPanelText(3);
						return;
					}
					if(outputFileName.getText() == "") {
						displayTextEastPanel.add("[ERROR] : Empty file name entered. File name not changed.");
						updateEastPanelText(3);
						return;
					}
					File file = new File(path, File.separator + outputFileName.getText());
					if(file.exists()) {
						displayTextEastPanel.add("[WARNING] : File exists already and will be overwritten. File is - " + path + outputFileName.getText());
						updateEastPanelText(2);
						outputName = outputFileName.getText();
						return;
					}
					displayTextEastPanel.add("[INFO] - Output file path and name set to : " + file.getAbsolutePath());
					updateEastPanelText(1);
					outputName = outputFileName.getText();
					enableExecuteButton();
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Output file can not be created. Check name/permission/space. " + path + outputName);
					updateEastPanelText(3);
				}
			}
		});
		
		blastthresholdlabel.setText(blastevalue);
		blastthresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(blastthresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + blastthresholdlabel.getText());
						updateEastPanelText(3);
						blastthresholdlabel.setText(blastevalue);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + blastthresholdlabel.getText());
					updateEastPanelText(3);
					blastthresholdlabel.setText(blastevalue);
					return;
				}
				blastevalue = blastthresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - Threshold : " + blastevalue);
				updateEastPanelText(1);
			}
		});
		
		ncthresholdlabel.setText(ncthreshold);
		ncthresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(ncthresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + ncthresholdlabel.getText());
						updateEastPanelText(3);
						ncthresholdlabel.setText(ncthreshold);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (greater than 1.0) float, so not set. You provided :" + ncthresholdlabel.getText());
						updateEastPanelText(3);
						ncthresholdlabel.setText(ncthreshold);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + ncthresholdlabel.getText());
					updateEastPanelText(3);
					ncthresholdlabel.setText(ncthreshold);
					return;
				}
				ncthreshold = ncthresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - NC Threshold : " + ncthreshold);
				updateEastPanelText(1);
			}
		});

		clusterthresholdlabel.setText(clusterthreshold);
		clusterthresholdlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(clusterthresholdlabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (negative) float, so not set. You provided :" + clusterthresholdlabel.getText());
						updateEastPanelText(3);
						clusterthresholdlabel.setText(clusterthreshold);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Threshold is an invalid (greater than 1.0) float, so not set. You provided :" + clusterthresholdlabel.getText());
						updateEastPanelText(3);
						clusterthresholdlabel.setText(clusterthreshold);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Threshold is not a valid float, so not set. You provided :" + clusterthresholdlabel.getText());
					updateEastPanelText(3);
					clusterthresholdlabel.setText(clusterthreshold);
					return;
				}
				clusterthreshold = clusterthresholdlabel.getText();
				displayTextEastPanel.add("[INFO] - Clustering Threshold : " + clusterthreshold);
				updateEastPanelText(1);
			}
		});
		
		silixpercentidentitylabel.setText(silixpercentidentity);
		silixpercentidentitylabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(silixpercentidentitylabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Percent identity value is an invalid (negative) float, so not set. You provided :" + silixpercentidentitylabel.getText());
						updateEastPanelText(3);
						silixpercentidentitylabel.setText(silixpercentidentity);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Percent identity value is an invalid (greater than 1.0) float, so not set. You provided :" + silixpercentidentitylabel.getText());
						updateEastPanelText(3);
						silixpercentidentitylabel.setText(silixpercentidentity);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Percent identity value is not a valid float, so not set. You provided :" + silixpercentidentitylabel.getText());
					updateEastPanelText(3);
					silixpercentidentitylabel.setText(silixpercentidentity);
					return;
				}
				silixpercentidentity = silixpercentidentitylabel.getText();
				displayTextEastPanel.add("[INFO] - Minimum percent identity value : " + silixpercentidentity);
				updateEastPanelText(1);
			}
		});
		
		silixminpercentoverlaplabel.setText(silixminpercentoverlap);
		silixminpercentoverlaplabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(silixminpercentoverlaplabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Minimum percent overlap value is an invalid (negative) float, so not set. You provided :" + silixminpercentoverlaplabel.getText());
						updateEastPanelText(3);
						silixminpercentoverlaplabel.setText(silixminpercentoverlap);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Minimum percent overlap value is an invalid (greater than 1.0) float, so not set. You provided :" + silixminpercentoverlaplabel.getText());
						updateEastPanelText(3);
						silixminpercentoverlaplabel.setText(silixminpercentoverlap);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Minimum percent overlap is not a valid float, so not set. You provided :" + silixminpercentoverlaplabel.getText());
					updateEastPanelText(3);
					silixminpercentoverlaplabel.setText(silixminpercentoverlap);
					return;
				}
				silixminpercentoverlap = silixminpercentoverlaplabel.getText();
				displayTextEastPanel.add("[INFO] - Minimum percent overlap value : " + silixminpercentoverlap);
				updateEastPanelText(1);
			}
		});
		
		mclinflationlabel.setText(silixpercentidentity);
		mclinflationlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(mclinflationlabel.getText());
					if(thresh < 1) {
						displayTextEastPanel.add("[ERROR] : Inflation parameter value is an invalid (negative or less than 1) float, so not set. You provided :" + mclinflationlabel.getText());
						updateEastPanelText(3);
						mclinflationlabel.setText(mclinflation);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Inflation parameter is not a valid float, so not set. You provided :" + mclinflationlabel.getText());
					updateEastPanelText(3);
					mclinflationlabel.setText(mclinflation);
					return;
				}
				mclinflation = mclinflationlabel.getText();
				displayTextEastPanel.add("[INFO] - Minimum percent overlap value : " + mclinflation);
				updateEastPanelText(1);
			}
		});
		
		hclusterminedgeweightlabel.setText(hclusterminedgeweight);
		hclusterminedgeweightlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					int thresh = Integer.parseInt(hclusterminedgeweightlabel.getText());
					if(thresh < 1) {
						displayTextEastPanel.add("[ERROR] : Minimum percent overlap value is an invalid (negative) integer, so not set. You provided :" + hclusterminedgeweightlabel.getText());
						updateEastPanelText(3);
						hclusterminedgeweightlabel.setText(hclusterminedgeweight);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Minimum percent overlap is not a valid number, so not set. You provided :" + hclusterminedgeweightlabel.getText());
					updateEastPanelText(3);
					hclusterminedgeweightlabel.setText(hclusterminedgeweight);
					return;
				}
				hclusterminedgeweight = hclusterminedgeweightlabel.getText();
				displayTextEastPanel.add("[INFO] - Minimum percent overlap value : " + hclusterminedgeweight);
				updateEastPanelText(1);
			}
		});
		
		hclusterminedgedensitylabel.setText(hclusterminedgedensity);
		hclusterminedgedensitylabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(hclusterminedgedensitylabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Minimum edge density between a join is an invalid (negative) float, so not set. You provided :" + hclusterminedgedensitylabel.getText());
						updateEastPanelText(3);
						hclusterminedgedensitylabel.setText(hclusterminedgedensity);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Minimum edge density between a join is an invalid (greater than 1.0) float, so not set. You provided :" + hclusterminedgedensitylabel.getText());
						updateEastPanelText(3);
						hclusterminedgedensitylabel.setText(hclusterminedgedensity);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Minimum edge density between a join is not a valid float, so not set. You provided :" + hclusterminedgedensitylabel.getText());
					updateEastPanelText(3);
					hclusterminedgedensitylabel.setText(hclusterminedgedensity);
					return;
				}
				hclusterminedgedensity = hclusterminedgedensitylabel.getText();
				displayTextEastPanel.add("[INFO] - Minimum edge density between a join value : " + hclusterminedgedensity);
				updateEastPanelText(1);
			}
		});
		
		hclustermaxsizelabel.setText(hclustermaxsize);
		hclustermaxsizelabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					int thresh = Integer.parseInt(hclustermaxsizelabel.getText());
					if(thresh < 1) {
						displayTextEastPanel.add("[ERROR] : Maximum size value is an invalid (negative) number, so not set. You provided :" + hclustermaxsizelabel.getText());
						updateEastPanelText(3);
						hclustermaxsizelabel.setText(hclustermaxsize);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Maximum size is not a valid number, so not set. You provided :" + hclustermaxsizelabel.getText());
					updateEastPanelText(3);
					hclustermaxsizelabel.setText(hclustermaxsize);
					return;
				}
				hclustermaxsize = hclustermaxsizelabel.getText();
				displayTextEastPanel.add("[INFO] - Maximum size value : " + hclustermaxsize);
				updateEastPanelText(1);
			}
		});

		hclusterbreakingedgedensitylabel.setText(hclusterbreakingedgedensity);
		hclusterbreakingedgedensitylabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					float thresh = Float.parseFloat(hclusterbreakingedgedensitylabel.getText());
					if(thresh < 0) {
						displayTextEastPanel.add("[ERROR] : Minimum edge density between a join is an invalid (negative) float, so not set. You provided :" + hclusterbreakingedgedensitylabel.getText());
						updateEastPanelText(3);
						hclusterbreakingedgedensitylabel.setText(hclusterbreakingedgedensity);
						return;
					} else if(thresh > 1) {
						displayTextEastPanel.add("[ERROR] : Minimum edge density between a join is an invalid (greater than 1.0) float, so not set. You provided :" + hclusterbreakingedgedensitylabel.getText());
						updateEastPanelText(3);
						hclusterbreakingedgedensitylabel.setText(hclusterbreakingedgedensity);
						return;
					}
				} catch (Exception e) {
					displayTextEastPanel.add("[ERROR] : Minimum edge density between a join is not a valid float, so not set. You provided :" + hclusterbreakingedgedensitylabel.getText());
					updateEastPanelText(3);
					hclusterbreakingedgedensitylabel.setText(hclusterbreakingedgedensity);
					return;
				}
				hclusterbreakingedgedensity = hclusterbreakingedgedensitylabel.getText();
				displayTextEastPanel.add("[INFO] - Minimum edge density between a join value : " + hclusterbreakingedgedensity);
				updateEastPanelText(1);
			}
		});
	
		droplist.setBackground(Color.WHITE);
		String analysis[] = {"Clustering on Neighborhood correlation (NC)", "Single Linkage Clustering (SiLiX)", "Markov clustering (MCL)", "Hierarchical clustering (hcluster_sg)", "High FIdelity Clustering (HiFiX)"};
		droplist.setModel(new DefaultComboBoxModel(analysis)); 
		ActionListener cbActionListener = new ActionListener() {//add actionlistner to listen for change
            public void actionPerformed(ActionEvent e) {

                Integer index = droplist.getSelectedIndex();//get the selected item

                switch (index) {//check for a match
                	case 0:
                		displayHelpForSelectedIndexNC(0);
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		displayTextEastPanel.add(" Required Arguments : (When ENABLED)");
                		displayTextEastPanel.add("                      1) Query sequence file (FASTA format)");
                		displayTextEastPanel.add("                      2) Output/input directory.");
                		displayTextEastPanel.add("                      3) Output filename.");
                		displayTextEastPanel.add("                      4) Blast resource path for finding makeblastdb and blastp/blastn. ");
                		displayTextEastPanel.add("                      5) Evalue limit for blast. ");
                		displayTextEastPanel.add("                      6) Blast type. ");
                		displayTextEastPanel.add("                      7) NC_Standalone resource path. ");
                		displayTextEastPanel.add("                      8) Threshold for Neighborhood Correlation (between 0 and 1). ");
                		displayTextEastPanel.add("                      9) Clustering method for combining families. ");	
                		displayTextEastPanel.add("                      10) Threshold for clustering. ");
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		updateEastPanelText(0);
                		progName = "NC";
                		break;
                	case 1:
                		displayHelpForSelectedIndexNC(1);
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		displayTextEastPanel.add(" Required Arguments : (When ENABLED)");
                		displayTextEastPanel.add("                      1) Query sequence file (FASTA format)");
                		displayTextEastPanel.add("                      2) Output/input directory.");
                		displayTextEastPanel.add("                      3) Output filename.");
                		displayTextEastPanel.add("                      4) Blast resource path for finding makeblastdb and blastp/blastn. ");
                		displayTextEastPanel.add("                      5) Evalue limit for blast. ");
                		displayTextEastPanel.add("                      6) Blast type. ");
                		displayTextEastPanel.add("                      7) Percentage identity. ");
                		displayTextEastPanel.add("                      8) Min Percent overlap. ");
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		updateEastPanelText(0);
                		progName = "silix";
                		break;
                	case 2:
                		displayHelpForSelectedIndexNC(2);
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		displayTextEastPanel.add(" Required Arguments : (When ENABLED)");
                		displayTextEastPanel.add("                      1) Query sequence file (FASTA format)");
                		displayTextEastPanel.add("                      2) Output/input directory.");
                		displayTextEastPanel.add("                      3) Output filename.");
                		displayTextEastPanel.add("                      4) Blast resource path for finding makeblastdb and blastp/blastn. ");
                		displayTextEastPanel.add("                      5) Evalue limit for blast. ");
                		displayTextEastPanel.add("                      6) Blast type. ");
                		displayTextEastPanel.add("                      7) Inflation parameter (I). ");
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		updateEastPanelText(0);
                		progName = "mcl";
                		break;
                	case 3:
                		displayHelpForSelectedIndexNC(3);
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		displayTextEastPanel.add(" Required Arguments : (When ENABLED)");
                		displayTextEastPanel.add("                      1) Query sequence file (FASTA format)");
                		displayTextEastPanel.add("                      2) Output/input directory.");
                		displayTextEastPanel.add("                      3) Output filename.");
                		displayTextEastPanel.add("                      4) Blast resource path for finding makeblastdb and blastp/blastn. ");
                		displayTextEastPanel.add("                      5) Evalue limit for blast. ");
                		displayTextEastPanel.add("                      6) Blast type. ");
                		displayTextEastPanel.add("                      7) minimum edge weight. ");
                		displayTextEastPanel.add("                      8) minimum edge density between a join. ");
                		displayTextEastPanel.add("                      9) maximum size. ");
                		displayTextEastPanel.add("                      10) breaking edge density. ");
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		updateEastPanelText(0);
                		progName = "hcluster";
                		break;
                	case 4:
                		displayHelpForSelectedIndexNC(1);
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		displayTextEastPanel.add(" Required Arguments : (When ENABLED)");
                		displayTextEastPanel.add("                      1) Query sequence file (FASTA format)");
                		displayTextEastPanel.add("                      2) Output/input directory.");
                		displayTextEastPanel.add("                      3) Output filename.");
                		displayTextEastPanel.add("                      4) Blast resource path for finding makeblastdb and blastp/blastn. ");
                		displayTextEastPanel.add("                      5) Evalue limit for blast. ");
                		displayTextEastPanel.add("                      6) Blast type. ");
                		displayTextEastPanel.add("                      7) Percentage identity. ");
                		displayTextEastPanel.add("                      8) Min Percent overlap. ");
                		displayTextEastPanel.add(" ----------------------------------------------------------------------------");
                		updateEastPanelText(0);
                		progName = "hifix";
                		break;
                }
            }
        };
        droplist.addActionListener(cbActionListener);

		blasttype.setBackground(Color.WHITE);
		String blastoptions[] = {"Protein", "Nucleotide"};
		blasttype.setModel(new DefaultComboBoxModel(blastoptions)); 
		ActionListener cbActionListener2 = new ActionListener() {//add actionlistner to listen for change
            public void actionPerformed(ActionEvent e) {

                Integer index = blasttype.getSelectedIndex();//get the selected item

                switch (index) {//check for a match
                	case 0:
                		proteinblast = true;
                		displayTextEastPanel.add("[INFO] - Blast for proteins (blastp) selected.");
                		updateEastPanelText(1);
                		break;
                	case 1:
                		proteinblast = false;
                		displayTextEastPanel.add("[INFO] - Blast for nucelotides (blastn) selected.");
                		updateEastPanelText(1);
                		break;
                }
            }
        };
        blasttype.addActionListener(cbActionListener2);
        
		clustermethod.setBackground(Color.WHITE);
		String clustermethodoptions[] = {"UPGMA", "SL", "CL"};
		clustermethod.setModel(new DefaultComboBoxModel(clustermethodoptions)); 
		ActionListener cbActionListener3 = new ActionListener() {//add actionlistner to listen for change
            public void actionPerformed(ActionEvent e) {

                Integer index = clustermethod.getSelectedIndex();//get the selected item

                switch (index) {//check for a match
                	case 0:
                		clusteringmethod = "a";
                		displayTextEastPanel.add("[INFO] - Average Linkage Clustering method selected.");
                		updateEastPanelText(1);
                		break;
                	case 1:
                		clusteringmethod = "s";
                		displayTextEastPanel.add("[INFO] - Single Linkage Clustering method selected.");
                		updateEastPanelText(1);
                		break;
                	case 2:
                		clusteringmethod = "c";
                		displayTextEastPanel.add("[INFO] - Complete Linkage Clustering method selected.");
                		updateEastPanelText(1);
                		break;
                }
            }
        };
        clustermethod.addActionListener(cbActionListener3);

		analysisPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		analysisPanel.add(droplist);
		analysisPanel.add(Box.createRigidArea(new Dimension(0, 7)));

		msaPanel.addSpecialComponent("Choose sequence file: ", new JLabel(), browseqSeqButtonNC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addSpecialComponent("Choose output folder: ", new JLabel(), browseOutputDirButtonNC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent("Choose output file name:", outputFileName, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addSpecialComponent("Choose Blast resource folder: ", new JLabel(), browseBlastResourceDirButtonNC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent(" Blast E-value Threshold:", blastthresholdlabel, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.addComponent(" Type of Blast:", blasttype, new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		msaPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		msaPanel.addSpecialComponent("All vs All blast file: ", new JLabel(), browseAvaBlastButtonNC(), new Dimension(600, 20));
		msaPanel.add(Box.createRigidArea(new Dimension(0, 7)));

		NCPanel.addSpecialComponent("Choose NC resource folder: ", new JLabel(), browseNCResourceDirButtonNC(), new Dimension(600, 20));
		NCPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		NCPanel.addComponent(" NC Threshold:", ncthresholdlabel, new Dimension(600, 20));
		NCPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		NCPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
				
		NCPanel.addComponent(" Clustering method:", clustermethod, new Dimension(600, 20));
		NCPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		NCPanel.addComponent(" Clustering distance threshold:", clusterthresholdlabel, new Dimension(600, 20));
		NCPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		NCPanel.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.LINE_START);
		
		NCPanel.addSpecialComponent("Neighborhood correlation score file: ", new JLabel(), browseNCButtonNC(), new Dimension(600, 20));
		NCPanel.add(Box.createRigidArea(new Dimension(0, 7)));

		silixPanel.addSpecialComponent("Choose SiLiX/HiFiX resource folder: ", new JLabel(), browseSiLiXResourceDirButtonNC(), new Dimension(600, 20));
		silixPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		silixPanel.addComponent(" Min % identity to accept blast hits :", silixpercentidentitylabel, new Dimension(600, 20));
		silixPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		silixPanel.addComponent(" Min % overlap to accept blast hits:", silixminpercentoverlaplabel, new Dimension(600, 20));
		silixPanel.add(Box.createRigidArea(new Dimension(0, 7)));

		mclPanel.addSpecialComponent("Choose MCL resource folder: ", new JLabel(), browseMCLResourceDirButtonNC(), new Dimension(600, 20));
		mclPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mclPanel.addComponent(" Inflation parameter:", mclinflationlabel, new Dimension(600, 20));
		mclPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		
		hclusterPanel.addSpecialComponent("Choose hcluster resource folder: ", new JLabel(), browsehclusterResourceDirButtonNC(), new Dimension(600, 20));
		hclusterPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		hclusterPanel.addComponent(" Minimum edge weight: ", hclusterminedgeweightlabel, new Dimension(600, 20));
		hclusterPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		hclusterPanel.addComponent(" Minimum edge density:", hclusterminedgedensitylabel, new Dimension(600, 20));
		hclusterPanel.add(Box.createRigidArea(new Dimension(0, 7)));				
		hclusterPanel.addComponent(" Maximum size of cluster:", hclustermaxsizelabel, new Dimension(600, 20));
		hclusterPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		hclusterPanel.addComponent(" Breaking edge density:", hclusterbreakingedgedensitylabel, new Dimension(600, 20));
		hclusterPanel.add(Box.createRigidArea(new Dimension(0, 7)));

        analysisPanel.setToolTipText("Algorithm to compute gene families");
		msaPanel.setToolTipText("General options applicable for all software");
		NCPanel.setToolTipText("Options specific for gene family inference from NC scores");
		silixPanel.setToolTipText("Options specific for SiLiX");
		mclPanel.setToolTipText("Options specific for Markov Clustering");
		hclusterPanel.setToolTipText("Options specific for hcluster_sg from Heng Li");
		

		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(analysisPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(msaPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(NCPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(silixPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(mclPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(hclusterPanel);

		droplist.setSelectedIndex(0);
		return panel;
	}
	
	private void getEastPanelGFC() {
		JLabel 				title;
		title = new JLabel("SimSynHomoLib - Library for inferring syntenically supported homologs and gene families");
		title.setForeground(Color.black);
		title.setFont(new Font("arial", Font.BOLD, 13));
		eastpanel.add(title,BorderLayout.WEST);
		
		displayTextEastPanel = new ArrayList<String>();
		panel = new JPanel();
		panel.setMinimumSize(new Dimension(615,785));
		panel.setBackground(Color.black);
//		panel.setBackground(new Color(192,192,192));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		eastScrollPane = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		eastScrollPane.setPreferredSize(new Dimension(630,800));
		eastpanel.add(eastScrollPane, BorderLayout.SOUTH);
		updateEastPanelText(0);
	}
	
	private void getEastPanelNC() {
		JLabel 				title;
		title = new JLabel("SimHomoLib - Library for inferring homologs and gene families based on sequence similarity only");
		title.setForeground(Color.black);
		title.setFont(new Font("arial", Font.BOLD, 13));
		eastpanel.add(title,BorderLayout.WEST);
		
		displayTextEastPanel = new ArrayList<String>();
		panel = new JPanel();
		panel.setMinimumSize(new Dimension(615,785));
		panel.setBackground(Color.black);
//		panel.setBackground(new Color(192,192,192));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		eastScrollPane = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		eastScrollPane.setPreferredSize(new Dimension(630,800));
		eastpanel.add(eastScrollPane, BorderLayout.SOUTH);
		updateEastPanelText(0);
	}
	
	private void displayHelpForSelectedDistanceMethod() {	
		querySyntenyButton.setEnabled(false);
		querySequenceButton.setEnabled(false);
		refSyntenyButton.setEnabled(false);
		refSequenceButton.setEnabled(false);
		browseBlastResourceDirButton.setEnabled(false);
		browseNCResourceDirButton.setEnabled(false);
		
		blastthresholdlabel.setEnabled(false);
		ncthresholdlabel.setEnabled(false);
		neighborhoodsizelabel.setEnabled(false);
		evaathresholdlabel.setEnabled(false);
		evabthresholdlabel.setEnabled(false);
		clusterthresholdlabel.setEnabled(false);
		
		blasttype.setEnabled(false);
		clustermethod.setEnabled(false);
		
		avaBlastButton.setEnabled(false);
		ncButton.setEnabled(false);
		syntenyextractorButton.setEnabled(false);
		syntenycorrelationButton.setEnabled(false);
		homologyevaluatorButton.setEnabled(false);
		genefamilyButton.setEnabled(false);
		
		browseOutputDirButton.setEnabled(true);
		outputFileName.setEnabled(true);

		if(moduleName.equals("dataprocessor")) {
			querySyntenyButton.setEnabled(true);
			querySequenceButton.setEnabled(true);
			refSyntenyButton.setEnabled(true);
			refSequenceButton.setEnabled(true);
			
			if(droplist.getSelectedIndex() == 1) {
				browseBlastResourceDirButton.setEnabled(true);
				browseNCResourceDirButton.setEnabled(true);
				
				blastthresholdlabel.setEnabled(true);
				ncthresholdlabel.setEnabled(true);
				neighborhoodsizelabel.setEnabled(true);
				evaathresholdlabel.setEnabled(true);
				evabthresholdlabel.setEnabled(true);
				clusterthresholdlabel.setEnabled(true);
				
				blasttype.setEnabled(true);
				clustermethod.setEnabled(true);
				displayTextEastPanel.add("[INFO] - Starting execution of all modules but starting from : preprocess data");
			} else {
				displayTextEastPanel.add("[INFO] - Selected module : preprocess data");
			}			
		} else if(moduleName.equals("avablast")) {
			querySequenceButton.setEnabled(true);
			refSequenceButton.setEnabled(true);
			
			browseBlastResourceDirButton.setEnabled(true);
			blastthresholdlabel.setEnabled(true);
			blasttype.setEnabled(true);
			
			if(droplist.getSelectedIndex() == 1) {
				querySyntenyButton.setEnabled(true);
				refSyntenyButton.setEnabled(true);
				
				browseNCResourceDirButton.setEnabled(true);
		
				ncthresholdlabel.setEnabled(true);
				neighborhoodsizelabel.setEnabled(true);
				evaathresholdlabel.setEnabled(true);
				evabthresholdlabel.setEnabled(true);
				clusterthresholdlabel.setEnabled(true);		
				
				clustermethod.setEnabled(true);
				displayTextEastPanel.add("[INFO] - Starting execution of all modules but starting from : All-vs-All Blast");
			} else {
				displayTextEastPanel.add("[INFO] - Selected module : All-vs-All Blast");
			}			
		} else if(moduleName.equals("nc")) {
			avaBlastButton.setEnabled(true);
			browseNCResourceDirButton.setEnabled(true);
			ncthresholdlabel.setEnabled(true);
			
			if(droplist.getSelectedIndex() == 1) {
				querySyntenyButton.setEnabled(true);
				refSyntenyButton.setEnabled(true);
				
				neighborhoodsizelabel.setEnabled(true);
				evaathresholdlabel.setEnabled(true);
				evabthresholdlabel.setEnabled(true);
				clusterthresholdlabel.setEnabled(true);
				
				clustermethod.setEnabled(true);
				displayTextEastPanel.add("[INFO] - Starting execution of all modules but starting from : neighborhood correlation");
			} else {
				displayTextEastPanel.add("[INFO] - Selected module : neighborhood correlation");
			}		
		} else if(moduleName.equals("sysc")) {
			querySyntenyButton.setEnabled(true);
			refSyntenyButton.setEnabled(true);
			ncButton.setEnabled(true);
			neighborhoodsizelabel.setEnabled(true);
			
			if(droplist.getSelectedIndex() == 1) {
				evaathresholdlabel.setEnabled(true);
				evabthresholdlabel.setEnabled(true);
				clusterthresholdlabel.setEnabled(true);
				
				clustermethod.setEnabled(true);
				displayTextEastPanel.add("[INFO] - Starting execution of all modules but starting from : synteny score module");
			} else {
				displayTextEastPanel.add("[INFO] - Selected module : synteny score module");
			}	
		} else if(moduleName.equals("syc")) {
			querySyntenyButton.setEnabled(true);
			refSyntenyButton.setEnabled(true);
			syntenyextractorButton.setEnabled(true);
			
			if(droplist.getSelectedIndex() == 1) {
				evaathresholdlabel.setEnabled(true);
				evabthresholdlabel.setEnabled(true);
				clusterthresholdlabel.setEnabled(true);
				
				clustermethod.setEnabled(true);
				displayTextEastPanel.add("[INFO] - Starting execution of all modules but starting from : synteny correlation module");
			} else {
				displayTextEastPanel.add("[INFO] - Selected module : synteny correlation module");
			}	
		} else if(moduleName.equals("eva")) {
			querySyntenyButton.setEnabled(true);
			refSyntenyButton.setEnabled(true);
			syntenycorrelationButton.setEnabled(true);
			evaathresholdlabel.setEnabled(true);
			evabthresholdlabel.setEnabled(true);
			
			if(droplist.getSelectedIndex() == 1) {
				clusterthresholdlabel.setEnabled(true);
				
				clustermethod.setEnabled(true);
				displayTextEastPanel.add("[INFO] - Starting execution of all modules but starting from : homology evaluation module");
			} else {
				displayTextEastPanel.add("[INFO] - Selected module : homology evaluation module");
			}	
		} else if(moduleName.equals("gfc")) {
			querySyntenyButton.setEnabled(true);
			refSyntenyButton.setEnabled(true);
			homologyevaluatorButton.setEnabled(true);
			clusterthresholdlabel.setEnabled(true);
			clustermethod.setEnabled(true);
			
			if(droplist.getSelectedIndex() == 1) {
				displayTextEastPanel.add("[INFO] - Starting execution of all modules but starting from : gene family module");
			} else {
				displayTextEastPanel.add("[INFO] - Selected module : gene family module");
			}	
		} else if(moduleName.equals("postprocess")) {
			querySyntenyButton.setEnabled(true);
			refSyntenyButton.setEnabled(true);
			genefamilyButton.setEnabled(true);
			displayTextEastPanel.add("[INFO] - Selected module : postprocess gene families.");
		}
		updateEastPanelText(1);
	}
	
	private void displayHelpForSelectedIndexGFC(int index) {
		runProgram.setEnabled(false);
		displayTextEastPanel.add(" ***** Welcome to GenFamClustX, your library for determining syntenic homologs *****");
		if(index == 0) {
			modulepanel.setEnabled(false);
			
			querySyntenyButton.setEnabled(true);
			querySequenceButton.setEnabled(true);
			refSyntenyButton.setEnabled(true);
			refSequenceButton.setEnabled(true);
			browseOutputDirButton.setEnabled(true);
			browseBlastResourceDirButton.setEnabled(true);
			browseNCResourceDirButton.setEnabled(true);
			
			blastthresholdlabel.setEnabled(true);
			ncthresholdlabel.setEnabled(true);
			neighborhoodsizelabel.setEnabled(true);
			evaathresholdlabel.setEnabled(true);
			evabthresholdlabel.setEnabled(true);
			clusterthresholdlabel.setEnabled(true);
			outputFileName.setEnabled(true);
			
			blasttype.setEnabled(true);
			clustermethod.setEnabled(true);
			
			avaBlastButton.setEnabled(false);
			ncButton.setEnabled(false);
			syntenyextractorButton.setEnabled(false);
			syntenycorrelationButton.setEnabled(false);
			homologyevaluatorButton.setEnabled(false);
			genefamilyButton.setEnabled(false);
			
			displayTextEastPanel.add("");
			displayTextEastPanel.add(" ---------------------- Complete Pipeline - GenFamClust -----------------------");
			displayTextEastPanel.add(" GenFamClust is a pipeline that infers homologous pairs and gene families");
			displayTextEastPanel.add(" with syntenic support. It is implemented in Java by Hashim, and");
			displayTextEastPanel.add(" designed by Lars and Hashim.");
		} else if(index == 1) {
			modulepanel.setEnabled(true);
			modulepanel.setSelectedIndex(0);

			displayTextEastPanel.add("");
			displayTextEastPanel.add(" --------------------------- Start from module --------------------------------");
			displayTextEastPanel.add(" Start GFC pipeline from a specific module and run till the end. ");
		} else if(index == 2) {
			modulepanel.setEnabled(true);
			modulepanel.setSelectedIndex(0);
			displayTextEastPanel.add("");
			displayTextEastPanel.add(" --------------------------- Execute module -----------------------------------");
			displayTextEastPanel.add(" Run a specific module from GenFamClust pipeline. ");
		}
	}
	
	private void displayHelpForSelectedIndexNC(int index) {
		runProgram.setEnabled(false);
		displayTextEastPanel.add(" ***** Welcome to SimHomoLib, your library for determining similarity-based homologs *****");

		querySequenceButton.setEnabled(true);
		browseOutputDirButton.setEnabled(true);
		outputFileName.setEnabled(true);
		blasttype.setEnabled(true);
		browseBlastResourceDirButton.setEnabled(true);
		blastthresholdlabel.setEnabled(true);
		avaBlastButton.setEnabled(true);
		
		browseNCResourceDirButton.setEnabled(false);			
		ncthresholdlabel.setEnabled(false);
		clusterthresholdlabel.setEnabled(false);
		clustermethod.setEnabled(false);
		ncButton.setEnabled(false);
		
		browseSiLiXResourceDirButton.setEnabled(false);
		silixpercentidentitylabel.setEnabled(false);
		silixminpercentoverlaplabel.setEnabled(false);
		
		browseMCLResourceDirButton.setEnabled(false);
		mclinflationlabel.setEnabled(false);
		
		browsehclusterResourceDirButton.setEnabled(false);
		hclusterminedgeweightlabel.setEnabled(false);
		hclusterminedgedensitylabel.setEnabled(false);
		hclustermaxsizelabel.setEnabled(false);
		hclusterbreakingedgedensitylabel.setEnabled(false);
		
		if(index == 0) {			
			browseNCResourceDirButton.setEnabled(true);			
			ncthresholdlabel.setEnabled(true);
			clusterthresholdlabel.setEnabled(true);
			clustermethod.setEnabled(true);
			ncButton.setEnabled(true);
			
			displayTextEastPanel.add("");
			displayTextEastPanel.add(" ---------- Clustering on Neighborhood Correlation - NCClust -----------------");
			displayTextEastPanel.add(" GenFamClust is a pipeline that infers homologous pairs and gene families");
			displayTextEastPanel.add(" with syntenic support. It is implemented in Java by Hashim, and");
			displayTextEastPanel.add(" designed by Lars and Hashim.");
		} else if(index == 1) {
			browseSiLiXResourceDirButton.setEnabled(true);
			silixpercentidentitylabel.setEnabled(true);
			silixminpercentoverlaplabel.setEnabled(true);

			displayTextEastPanel.add("");
			displayTextEastPanel.add(" ------------------ Single Linkage Clustering (SiLiX) -------------------------");
			displayTextEastPanel.add(" Start GFC pipeline from a specific module and run till the end. ");
		} else if(index == 2) {
			browseMCLResourceDirButton.setEnabled(true);
			mclinflationlabel.setEnabled(true);
			
			displayTextEastPanel.add("");
			displayTextEastPanel.add(" --------------------------- Markov Clustering (MCL) --------------------------");
			displayTextEastPanel.add(" Run a specific module from GenFamClust pipeline. ");
		} else if(index == 3) {
			browsehclusterResourceDirButton.setEnabled(true);
			hclusterminedgeweightlabel.setEnabled(true);
			hclusterminedgedensitylabel.setEnabled(true);
			hclustermaxsizelabel.setEnabled(true);
			hclusterbreakingedgedensitylabel.setEnabled(true);
			
			displayTextEastPanel.add("");
			displayTextEastPanel.add(" ------------- Hierarchical clustering (hcluster_sg) --------------------------");
			displayTextEastPanel.add(" Run a specific module from GenFamClust pipeline. ");
		}
	}
	
	public void updateEastPanelText(int level) {
		if(level == 0) {
			panel.removeAll();
			for(int i = 0; i < displayTextEastPanel.size(); i++) {
				JLabel 				title;
				title = new JLabel(" " + displayTextEastPanel.get(i));
				//title.setForeground(Color.black);
				title.setForeground(Color.white);
				panel.add(title);
			}
		} else {
			for(int i = 0; i < displayTextEastPanel.size(); i++) {
				JLabel 				title;
				title = new JLabel(" " + displayTextEastPanel.get(i));
				if(level == 1)
					title.setForeground(new Color(0, 255, 0));
				else if(level == 2)
					title.setForeground(new Color(255, 165, 0));
				else if(level == 3)
					title.setForeground(new Color(255, 0, 0));
				panel.add(title);
			}
		}
		panel.repaint();
		panel.revalidate();
		displayTextEastPanel.clear();
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseqSynButtonGFC() {
		querySyntenyButton.setBackground(Color.WHITE);
		querySyntenyButton.setToolTipText("Browse and locate the query synteny file in hashim's specified format (hsf)!");
		
		//Button listener - removes tree markings inside the graph.
		querySyntenyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("hsf")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "hsf files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : Synteny file selected not in hsf format. Query synteny file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					qsynfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(0).setText(qsynfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Query Synteny file is set to : " + qsynfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return querySyntenyButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseqSeqButtonGFC() {
		querySequenceButton.setBackground(Color.WHITE);
		querySequenceButton.setToolTipText("Browse and locate the query sequence file (in FASTA format)!");
//		qseqfile = null;
		
		//Button listener - removes tree markings inside the graph.
		querySequenceButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("fas")||extension.equals("txt") || extension.equals("out") || extension.equals("fa")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "Fasta files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						try{
//							new MSA(chooser.getSelectedFile());
						} catch (Exception e1) {
							displayTextEastPanel.add("[ERROR] : query sequence file selected not in either FASTA or indexed format. True MSA file is not set/changed. ");
							updateEastPanelText(3);
							return;
						}
					}
					
					qseqfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(1).setText(qseqfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : query sequence file is set to : " + qseqfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return querySequenceButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseqSeqButtonNC() {
		querySequenceButton.setBackground(Color.WHITE);
		querySequenceButton.setToolTipText("Browse and locate the query sequence file (in FASTA format)!");
//		qseqfile = null;
		
		//Button listener - removes tree markings inside the graph.
		querySequenceButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("fas")||extension.equals("txt") || extension.equals("out") || extension.equals("fa")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "Fasta files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						try{
//							new MSA(chooser.getSelectedFile());
						} catch (Exception e1) {
							displayTextEastPanel.add("[ERROR] : query sequence file selected not in either FASTA or indexed format. True MSA file is not set/changed. ");
							updateEastPanelText(3);
							return;
						}
					}
					
					qseqfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(0).setText(qseqfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : query sequence file is set to : " + qseqfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return querySequenceButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browserSynButtonGFC() {
		refSyntenyButton.setBackground(Color.WHITE);
		refSyntenyButton.setToolTipText("Browse and locate the reference synteny file in hashim's specified format (hsf)!");
		
		//Button listener - removes tree markings inside the graph.
		refSyntenyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("hsf")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "hsf files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : Synteny file selected not in hsf format. Reference synteny file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					rsynfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(2).setText(rsynfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Reference Synteny file is set to : " + rsynfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return refSyntenyButton;
	}
		
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browserSeqButtonGFC() {
		refSequenceButton.setBackground(Color.WHITE);
		refSequenceButton.setToolTipText("Browse and locate the reference sequence file (in FASTA format)!");
		rseqfile = null;
		
		//Button listener - removes tree markings inside the graph.
		refSequenceButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("fas")||extension.equals("txt") || extension.equals("out") || extension.equals("fa")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "Fasta files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						try{
//							new MSA(chooser.getSelectedFile());
						} catch (Exception e1) {
							displayTextEastPanel.add("[ERROR] : Reference sequence file selected not in either FASTA or indexed format. True MSA file is not set/changed. ");
							updateEastPanelText(3);
							return;
						}
					}
					
					rseqfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(3).setText(rseqfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : reference sequence file is set to : " + rseqfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return refSequenceButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseOutputDirButtonGFC() {
		browseOutputDirButton.setBackground(Color.WHITE);
		browseOutputDirButton.setToolTipText("Browse and locate the MSA file!");
		
		//Button listener - removes tree markings inside the graph.
		browseOutputDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "MSA files";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					path = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(4).setText(path.getAbsolutePath());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Ouput directory is set to : " + path.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		
		return browseOutputDirButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseOutputDirButtonNC() {
		browseOutputDirButton.setBackground(Color.WHITE);
		browseOutputDirButton.setToolTipText("Browse and locate the MSA file!");
		
		//Button listener - removes tree markings inside the graph.
		browseOutputDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "MSA files";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					path = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(1).setText(path.getAbsolutePath());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Ouput directory is set to : " + path.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		
		return browseOutputDirButton;
	}

	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseBlastResourceDirButtonGFC() {
		browseBlastResourceDirButton.setBackground(Color.WHITE);
		browseBlastResourceDirButton.setToolTipText("Browse and locate the folder containing makeblastdb and blastp/blastn programs!");
		
		//Button listener - removes tree markings inside the graph.
		browseBlastResourceDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "folder containing blast suite";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					boolean check = new File(chooser.getSelectedFile(), "makeblastdb").exists() && new File(chooser.getSelectedFile(), "makeblastdb").canExecute();
					boolean check1 = check && new File(chooser.getSelectedFile(), "blastp").exists() && new File(chooser.getSelectedFile(), "blastp").canExecute();
					boolean check2 = check1 && new File(chooser.getSelectedFile(), "blastn").exists() && new File(chooser.getSelectedFile(), "blastn").canExecute();
					if(check2 == true) {
						blastresourcepath = chooser.getSelectedFile();
						displaypanelLock.lock();
						msaPanel.labels.get(5).setText(blastresourcepath.getAbsolutePath());
						displaypanelLock.unlock();
						displayTextEastPanel.add("[INFO] : Blast suite directory is set to : " + blastresourcepath.getAbsolutePath());
						updateEastPanelText(1);
						enableExecuteButton();
					} else {
						displayTextEastPanel.add("[ERROR] : The directory does not contain blast suite files makeblastdb, blastp and/or blastn at " + chooser.getSelectedFile());
						displayTextEastPanel.add("[ERROR] : Or the programs can not be executed. Blast resource path not changed! ");
						updateEastPanelText(3);
					}
				}
			}
		});
		
		return browseBlastResourceDirButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseBlastResourceDirButtonNC() {
		browseBlastResourceDirButton.setBackground(Color.WHITE);
		browseBlastResourceDirButton.setToolTipText("Browse and locate the folder containing makeblastdb and blastp/blastn programs!");
		
		//Button listener - removes tree markings inside the graph.
		browseBlastResourceDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "folder containing blast suite";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					boolean check = new File(chooser.getSelectedFile(), "makeblastdb").exists() && new File(chooser.getSelectedFile(), "makeblastdb").canExecute();
					boolean check1 = check && new File(chooser.getSelectedFile(), "blastp").exists() && new File(chooser.getSelectedFile(), "blastp").canExecute();
					boolean check2 = check1 && new File(chooser.getSelectedFile(), "blastn").exists() && new File(chooser.getSelectedFile(), "blastn").canExecute();
					if(check2 == true) {
						blastresourcepath = chooser.getSelectedFile();
						displaypanelLock.lock();
						msaPanel.labels.get(2).setText(blastresourcepath.getAbsolutePath());
						displaypanelLock.unlock();
						displayTextEastPanel.add("[INFO] : Blast suite directory is set to : " + blastresourcepath.getAbsolutePath());
						updateEastPanelText(1);
						enableExecuteButton();
					} else {
						displayTextEastPanel.add("[ERROR] : The directory does not contain blast suite files makeblastdb, blastp and/or blastn at " + chooser.getSelectedFile());
						displayTextEastPanel.add("[ERROR] : Or the programs can not be executed. Blast resource path not changed! ");
						updateEastPanelText(3);
					}
				}
			}
		});
		
		return browseBlastResourceDirButton;
	}

	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseNCResourceDirButtonGFC() {
		browseNCResourceDirButton.setBackground(Color.WHITE);
		browseNCResourceDirButton.setToolTipText("Browse and locate the folder containing NC_standalone!");
		
		//Button listener - removes tree markings inside the graph.
		browseNCResourceDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "folder containing blast suite";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					boolean check = new File(chooser.getSelectedFile(), "NC_standalone").exists() && new File(chooser.getSelectedFile(), "NC_standalone").canExecute();
					if(check == true) {
						ncresourcepath = chooser.getSelectedFile();
						displaypanelLock.lock();
						msaPanel.labels.get(6).setText(ncresourcepath.getAbsolutePath());
						displaypanelLock.unlock();
						displayTextEastPanel.add("[INFO] : Neighborhood Correlation suite directory is set to : " + ncresourcepath.getAbsolutePath());
						updateEastPanelText(1);
						enableExecuteButton();
					} else {
						displayTextEastPanel.add("[ERROR] : The directory does not contain NC executable files NC_standalone at " + chooser.getSelectedFile());
						displayTextEastPanel.add("[ERROR] : Or the program can not be executed. NC resource path not changed! ");
						updateEastPanelText(3);
					}
				}
			}
		});
		
		return browseNCResourceDirButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseNCResourceDirButtonNC() {
		browseNCResourceDirButton.setBackground(Color.WHITE);
		browseNCResourceDirButton.setToolTipText("Browse and locate the folder containing NC_standalone!");
		
		//Button listener - removes tree markings inside the graph.
		browseNCResourceDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "folder containing blast suite";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					boolean check = new File(chooser.getSelectedFile(), "NC_standalone").exists() && new File(chooser.getSelectedFile(), "NC_standalone").canExecute();
					if(check == true) {
						ncresourcepath = chooser.getSelectedFile();
						displaypanelLock.lock();
						NCPanel.labels.get(0).setText(ncresourcepath.getAbsolutePath());
						displaypanelLock.unlock();
						displayTextEastPanel.add("[INFO] : Neighborhood Correlation suite directory is set to : " + ncresourcepath.getAbsolutePath());
						updateEastPanelText(1);
						enableExecuteButton();
					} else {
						displayTextEastPanel.add("[ERROR] : The directory does not contain NC executable files NC_standalone at " + chooser.getSelectedFile());
						displayTextEastPanel.add("[ERROR] : Or the program can not be executed. NC resource path not changed! ");
						updateEastPanelText(3);
					}
				}
			}
		});
		
		return browseNCResourceDirButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseSiLiXResourceDirButtonNC() {
		browseSiLiXResourceDirButton.setBackground(Color.WHITE);
		browseSiLiXResourceDirButton.setToolTipText("Browse and locate the folder containing silix!");
		
		//Button listener - removes tree markings inside the graph.
		browseSiLiXResourceDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "folder containing SiLiX/HiFiX suite";
					}
				}

				JFileChooser chooser = new JFileChooser();
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					if(droplist.getSelectedIndex() == 1) {
						boolean check = new File(chooser.getSelectedFile(), "silix").exists() && new File(chooser.getSelectedFile(), "silix").canExecute();
						if(check == true) {
							silixresourcepath = chooser.getSelectedFile();
							displaypanelLock.lock();
							silixPanel.labels.get(0).setText(silixresourcepath.getAbsolutePath());
							displaypanelLock.unlock();
							displayTextEastPanel.add("[INFO] : SiLiX suite directory is set to : " + silixresourcepath.getAbsolutePath());
							updateEastPanelText(1);
							enableExecuteButton();
						} else {
							displayTextEastPanel.add("[ERROR] : The directory does not contain SiLiX executable files silix at " + chooser.getSelectedFile());
							displayTextEastPanel.add("[ERROR] : Or the program can not be executed. SiLiX resource path not changed! ");
							updateEastPanelText(3);
						}
					} else if(droplist.getSelectedIndex() == 4) {
						boolean check = new File(chooser.getSelectedFile(), "hifix").exists() && new File(chooser.getSelectedFile(), "hifix").canExecute() && new File(chooser.getSelectedFile(), "silix").exists() && new File(chooser.getSelectedFile(), "silix").canExecute();
						if(check == true) {
							silixresourcepath = chooser.getSelectedFile();
							displaypanelLock.lock();
							silixPanel.labels.get(0).setText(silixresourcepath.getAbsolutePath());
							displaypanelLock.unlock();
							displayTextEastPanel.add("[INFO] : SiLiX/HiFiX suite directory is set to : " + silixresourcepath.getAbsolutePath());
							updateEastPanelText(1);
							enableExecuteButton();
						} else {
							displayTextEastPanel.add("[ERROR] : The directory does not contain SiLiX AND/OR HiFiX executable files silix and/or hifix at " + chooser.getSelectedFile());
							displayTextEastPanel.add("[ERROR] : Or the program can not be executed. HiFiX resource path not changed! ");
							updateEastPanelText(3);
						}
					}
				}
			}
		});
		
		return browseSiLiXResourceDirButton;
	}

	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseMCLResourceDirButtonNC() {
		browseMCLResourceDirButton.setBackground(Color.WHITE);
		browseMCLResourceDirButton.setToolTipText("Browse and locate the folder containing mcl!");
		
		//Button listener - removes tree markings inside the graph.
		browseMCLResourceDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "folder containing MCL suite";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					boolean check = new File(chooser.getSelectedFile(), "mcl").exists() && new File(chooser.getSelectedFile(), "mcl").canExecute();
					if(check == true) {
						mclresourcepath = chooser.getSelectedFile();
						displaypanelLock.lock();
						mclPanel.labels.get(0).setText(mclresourcepath.getAbsolutePath());
						displaypanelLock.unlock();
						displayTextEastPanel.add("[INFO] : MCL suite directory is set to : " + mclresourcepath.getAbsolutePath());
						updateEastPanelText(1);
						enableExecuteButton();
					} else {
						displayTextEastPanel.add("[ERROR] : The directory does not contain MCL executable files mcl at " + chooser.getSelectedFile());
						displayTextEastPanel.add("[ERROR] : Or the program can not be executed. MCL resource path not changed! ");
						updateEastPanelText(3);
					}
				}
			}
		});
		
		return browseMCLResourceDirButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browsehclusterResourceDirButtonNC() {
		browsehclusterResourceDirButton.setBackground(Color.WHITE);
		browsehclusterResourceDirButton.setToolTipText("Browse and locate the folder containing hcluster!");
		
		//Button listener - removes tree markings inside the graph.
		browsehclusterResourceDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return false;
						else
							return true;
					}

					public String getDescription() {  //Description of filter
						return "folder containing hcluster_sg suite";
					}
				}

				JFileChooser chooser = new JFileChooser();
//			    chooser.setCurrentDirectory(new java.io.File("."));
//			    chooser.setDialogTitle(choosertitle);
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    //
			    // disable the "All files" option.
			    //
			    chooser.addChoosableFileFilter(new MCMCFilter());

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					boolean check = new File(chooser.getSelectedFile(), "hcluster_sg").exists() && new File(chooser.getSelectedFile(), "hcluster_sg").canExecute();
					if(check == true) {
						hclusterresourcepath = chooser.getSelectedFile();
						displaypanelLock.lock();
						hclusterPanel.labels.get(0).setText(hclusterresourcepath.getAbsolutePath());
						displaypanelLock.unlock();
						displayTextEastPanel.add("[INFO] : hcluster suite directory is set to : " + hclusterresourcepath.getAbsolutePath());
						updateEastPanelText(1);
						enableExecuteButton();
					} else {
						displayTextEastPanel.add("[ERROR] : The directory does not contain hcluster_sg executable file hcluster at " + chooser.getSelectedFile());
						displayTextEastPanel.add("[ERROR] : Or the program can not be executed. hcluster_sg resource path not changed! ");
						updateEastPanelText(3);
					}
				}
			}
		});
		
		return browsehclusterResourceDirButton;
	}

	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseAvaBlastButtonGFC() {
		avaBlastButton.setBackground(Color.WHITE);
		avaBlastButton.setToolTipText("Browse and locate the all versus all blast file (tab separated in abc format)!");
		
		//Button listener - removes tree markings inside the graph.
		avaBlastButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("bl") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "blast file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : All versus All blast file selected is not in abc format. All versus all blast file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					avablastfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(7).setText(avablastfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : All versus All blast file is set to : " + avablastfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return avaBlastButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseAvaBlastButtonNC() {
		avaBlastButton.setBackground(Color.WHITE);
		avaBlastButton.setToolTipText("Browse and locate the all versus all blast file (tab separated in abc format)!");
		
		//Button listener - removes tree markings inside the graph.
		avaBlastButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("bl") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "blast file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : All versus All blast file selected is not in abc format. All versus all blast file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					avablastfile = chooser.getSelectedFile();
					blastresourcepath = null;
					blastthresholdlabel.setEnabled(false);
					blasttype.setEnabled(false);
					querySequenceButton.setEnabled(false);
					browseBlastResourceDirButton.setEnabled(false);
					if(droplist.getSelectedIndex() != 1)
						qseqfile = null;
					displaypanelLock.lock();
					if(droplist.getSelectedIndex() != 1)
						msaPanel.labels.get(0).setText("");
					msaPanel.labels.get(2).setText("");
					msaPanel.labels.get(3).setText(avablastfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : All versus All blast file is set to : " + avablastfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return avaBlastButton;
	}

	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseNCButtonGFC() {
		ncButton.setBackground(Color.WHITE);
		ncButton.setToolTipText("Browse and locate the Neighborhood correlation file (tab separated in abc format)!");
		
		//Button listener - removes tree markings inside the graph.
		ncButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("nnc") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "nc file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : Neighborhood correlation file selected is not in abc format. Neighborhodo correlation file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					ncfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(8).setText(ncfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Neighborhood correlation file is set to : " + ncfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return ncButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseNCButtonNC() {
		ncButton.setBackground(Color.WHITE);
		ncButton.setToolTipText("Browse and locate the Neighborhood correlation file (tab separated in abc format)!");
		
		//Button listener - removes tree markings inside the graph.
		ncButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("nnc") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "nc file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : Neighborhood correlation file selected is not in abc format. Neighborhodo correlation file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					ncfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					NCPanel.labels.get(1).setText(ncfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Neighborhood correlation file is set to : " + ncfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return ncButton;
	}

	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseSyntenyExtractorButtonGFC() {
		syntenyextractorButton.setBackground(Color.WHITE);
		syntenyextractorButton.setToolTipText("Browse and locate the synteny score file (tab separated in abcd format)!");
		
		//Button listener - removes tree markings inside the graph.
		syntenyextractorButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("SySc") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "synteny score file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : Synteny score file selected is not in abcd format. Syteny score file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					syntenyscorefile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(9).setText(syntenyscorefile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Syteny score file is set to : " + syntenyscorefile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return syntenyextractorButton;
	}
		
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseSyntenyCorrelationButtonGFC() {
		syntenycorrelationButton.setBackground(Color.WHITE);
		syntenycorrelationButton.setToolTipText("Browse and locate the synteny correlation file (tab separated in abcd format)!");
		
		//Button listener - removes tree markings inside the graph.
		syntenycorrelationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("SyC") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "synteny correlation file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : Synteny correlation file selected is not in abcd format. Syteny correlation file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					syntenycorrelationfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(10).setText(syntenycorrelationfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : Syteny correlation file is set to : " + syntenycorrelationfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return syntenycorrelationButton;
	}
		
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseHomologyEvaluatorButtonGFC() {
		homologyevaluatorButton.setBackground(Color.WHITE);
		homologyevaluatorButton.setToolTipText("Browse and locate the inferred homology file (tab separated in abc format)!");
		
		//Button listener - removes tree markings inside the graph.
		homologyevaluatorButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("SyC") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "homology evaluation file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : homology evaluation file selected is not in abc format. Homology evaluation file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					homologyevaluatorfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(11).setText(homologyevaluatorfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : homology evaluation file is set to : " + homologyevaluatorfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return homologyevaluatorButton;
	}
		
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private JButton browseGeneFamilyButtonGFC() {
		genefamilyButton.setBackground(Color.WHITE);
		genefamilyButton.setToolTipText("Browse and locate the inferred gene families file (tab separated in blast clust format)!");
		
		//Button listener - removes tree markings inside the graph.
		genefamilyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("bcl") || extension.equals("txt")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "gene family file";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					try {
//						new MSA(chooser.getSelectedFile(),false);
					} catch (Exception e) {
						displayTextEastPanel.add("[ERROR] : gene family file selected is not in blast clust format. Gene family file is not set/changed. ");
						updateEastPanelText(3);
						return;
					}
					
					genefamilyfile = chooser.getSelectedFile();
					displaypanelLock.lock();
					msaPanel.labels.get(12).setText(genefamilyfile.getName());
					displaypanelLock.unlock();
					displayTextEastPanel.add("[INFO] : gene family file is set to : " + genefamilyfile.getAbsolutePath());
					updateEastPanelText(1);
					enableExecuteButton();
				}
			}
		});
		return genefamilyButton;
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private void initDefaultButtonsGFC() {
		runProgram.setBackground(Color.WHITE);
		runProgram.setToolTipText("The button is enabled, when all compulsory arguments (in right text panel for selected program) are met! Run the program selected above with the parameter settings displayed!");
		runProgram.setEnabled(false);
		
		//Button listener - removes tree markings inside the graph.
		runProgram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for(int t=0; t<workerThreads.length; t++)
					workerThreads[t].interrupt();
				workerThreads[0] = new Thread() {
					public void run() {
						boolean run;
						Integer index = droplist.getSelectedIndex();//get the selected item
						if(index == 0 || (index == 1 && modulepanel.getSelectedIndex() == 0)) {
							displayTextEastPanel.add("[METHOD] - PERFORMING COMPLETE GENFAMCLUST ANALYSIS");
							displayTextEastPanel.add(" =====================================================================================");
							displayTextEastPanel.add("[STATUS] - *** Preprocessing and preparing module ***");
							updateEastPanelText(1);
							DataPreparator dp = new DataPreparator();
							if (rsynfile != null) {
								String arguments1[] = {qseqfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName, "-s", rsynfile.getName()};
								run = dp.main(window, arguments1);
							} else {
								String arguments1[] = {qseqfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};
								run = dp.main(window, arguments1);
							}
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - Data preprocessing failed. Check synteny and sequence file formats.");
								updateEastPanelText(3);
								return;
							}

							displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
							displayTextEastPanel.add("[STATUS] - *** Performing All-vs-All Blast ***");
							updateEastPanelText(1);
							
							BlastModule bm = new BlastModule();
							if (rseqfile != null) {
								if(proteinblast == true) {
									String arguments1[] = {outputName + ".fasta", "-p", path.getAbsolutePath(), "-s", rseqfile.getName(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
									run = bm.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".fasta", "-p", path.getAbsolutePath(), "-s", rseqfile.getName(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
									run = bm.main(window, arguments1);
								}
							} else {
								if(proteinblast == true) {
									String arguments1[] = {outputName + ".fasta", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
									run = bm.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".fasta", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
									run = bm.main(window, arguments1);
								}
							}
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - All-vs-All Blast failed. Check executable files and file formats etc.");
								updateEastPanelText(3);
								return;
							}
							
							displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
							displayTextEastPanel.add("[STATUS] - *** Calculating neighborhood correlation scores ***");
							updateEastPanelText(1);
							
							NeighborhoodCorrelation nc = new NeighborhoodCorrelation();
							String arguments2[] = {outputName + ".bl", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", ncresourcepath.getAbsolutePath(), "-t", ncthreshold};
							run = nc.main(window, arguments2);
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - NC calculation failed. Check executable files and file formats etc.");
								updateEastPanelText(3);
								return;
							}
							
							displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
							displayTextEastPanel.add("[STATUS] - *** Calculating synteny scores ***");
							updateEastPanelText(1);
							
							SyntenyExtractor se = new SyntenyExtractor();
							if (rsynfile != null) {
								String arguments1[] = {qsynfile.getName(), outputName + ".nnc", "-p", path.getAbsolutePath(), "-f", rsynfile.getName(), "-o", outputName, "-k", neighborhoodsize};
								run = se.main(window, arguments1);
							} else {
								String arguments1[] = {qsynfile.getName(), outputName + ".nnc", "-p", path.getAbsolutePath(), "-o", outputName, "-k", neighborhoodsize};
								run = se.main(window, arguments1);
							}
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - Synteny score calculation failed. Check executable files and file formats etc.");
								updateEastPanelText(3);
								return;
							}
							
							displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
							displayTextEastPanel.add("[STATUS] - *** Computing synteny correlation scores ***");
							updateEastPanelText(1);
							
							SyntenyCorrelationScore scs = new SyntenyCorrelationScore();
							if (rsynfile != null) {
								String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName};
								run = scs.main(window, arguments1);
							} else {
								String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};									
								run = scs.main(window, arguments1);
							}
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - Synteny correlation calculation failed. Check executable files and file formats etc.");
								updateEastPanelText(3);
								return;
							}
							
							displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
							displayTextEastPanel.add("[STATUS] - *** Evaluating homologs with syntenic support ***");
							updateEastPanelText(1);
							
							Evaluator eva = new Evaluator();
							String arguments[] = {outputName + ".syc", "-p", path.getAbsolutePath(), "-a", evaathreshold, "-b", evabthreshold,"-o", outputName};
							run = eva.main(window, arguments);
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - Homology score evaluation failed. Check executable files and file formats etc.");
								updateEastPanelText(3);
								return;
							}
							
							displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
							displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
							updateEastPanelText(1);
							
							SparseClusterer sc = new SparseClusterer();
							if (rsynfile != null) {
								String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
								run = sc.main(window, arguments1);
							} else {
								String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
								run = sc.main(window, arguments1);
							}
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - Gene family inference failed. Check executable files and file formats etc.");
								updateEastPanelText(3);
								return;
							}
							
							displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
							displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
							updateEastPanelText(1);
							
							PostProcessData pp = new PostProcessData();
							if (rsynfile != null) {
								String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
								run = pp.main(window, arguments1);
							} else {
								String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
								run = pp.main(window, arguments1);
							}
							if(run == false) {
								displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
								updateEastPanelText(3);
								return;
							}
							
							displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
							displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
							displayTextEastPanel.add(" =====================================================================================");
							updateEastPanelText(1);
						} else if (index == 1) {
							if(modulepanel.getSelectedIndex() == 1) {
								displayTextEastPanel.add("[METHOD] - PERFORMING GENFAMCLUST ANALYSIS - From Blast");
								displayTextEastPanel.add(" =====================================================================================");
								displayTextEastPanel.add("[STATUS] - *** Performing All-vs-All Blast ***");
								updateEastPanelText(1);
								
								BlastModule bm = new BlastModule();
								if (rseqfile != null) {
									if(proteinblast == true) {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-s", rseqfile.getName(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
										run = bm.main(window, arguments1);
									} else {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-s", rseqfile.getName(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
										run = bm.main(window, arguments1);
									}
								} else {
									if(proteinblast == true) {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
										run = bm.main(window, arguments1);
									} else {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
										run = bm.main(window, arguments1);
									}
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - All-vs-All Blast failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Calculating neighborhood correlation scores ***");
								updateEastPanelText(1);
								
								NeighborhoodCorrelation nc = new NeighborhoodCorrelation();
								String arguments2[] = {outputName + ".bl", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", ncresourcepath.getAbsolutePath(), "-t", ncthreshold};
								run = nc.main(window, arguments2);
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - NC calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Calculating synteny scores ***");
								updateEastPanelText(1);
								
								SyntenyExtractor se = new SyntenyExtractor();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), outputName + ".nnc", "-p", path.getAbsolutePath(), "-f", rsynfile.getName(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), outputName + ".nnc", "-p", path.getAbsolutePath(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Synteny score calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing synteny correlation scores ***");
								updateEastPanelText(1);
								
								SyntenyCorrelationScore scs = new SyntenyCorrelationScore();
								if (rsynfile != null) {
									String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName};
									run = scs.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};									
									run = scs.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Synteny correlation calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Evaluating homologs with syntenic support ***");
								updateEastPanelText(1);
								
								Evaluator eva = new Evaluator();
								String arguments[] = {outputName + ".syc", "-p", path.getAbsolutePath(), "-a", evaathreshold, "-b", evabthreshold,"-o", outputName};
								run = eva.main(window, arguments);
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Homology score evaluation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
								updateEastPanelText(1);
								
								SparseClusterer sc = new SparseClusterer();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
									run = sc.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
									run = sc.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Gene family inference failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
								updateEastPanelText(1);
								
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
								displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
								displayTextEastPanel.add(" =====================================================================================");
								updateEastPanelText(1);
							} else if(modulepanel.getSelectedIndex() == 2) {
								displayTextEastPanel.add("[METHOD] - PERFORMING GENFAMCLUST ANALYSIS - From NC");
								displayTextEastPanel.add(" =====================================================================================");
								displayTextEastPanel.add("[STATUS] - *** Calculating neighborhood correlation scores ***");
								updateEastPanelText(1);
								
								NeighborhoodCorrelation nc = new NeighborhoodCorrelation();
								String arguments2[] = {avablastfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", ncresourcepath.getAbsolutePath(), "-t", ncthreshold};
								run = nc.main(window, arguments2);
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - NC calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Calculating synteny scores ***");
								updateEastPanelText(1);
								
								SyntenyExtractor se = new SyntenyExtractor();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), outputName + ".nnc", "-p", path.getAbsolutePath(), "-f", rsynfile.getName(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), outputName + ".nnc", "-p", path.getAbsolutePath(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Synteny score calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing synteny correlation scores ***");
								updateEastPanelText(1);
								
								SyntenyCorrelationScore scs = new SyntenyCorrelationScore();
								if (rsynfile != null) {
									String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName};
									run = scs.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};									
									run = scs.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Synteny correlation calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Evaluating homologs with syntenic support ***");
								updateEastPanelText(1);
								
								Evaluator eva = new Evaluator();
								String arguments[] = {outputName + ".syc", "-p", path.getAbsolutePath(), "-a", evaathreshold, "-b", evabthreshold,"-o", outputName};
								run = eva.main(window, arguments);
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Homology score evaluation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
								updateEastPanelText(1);
								
								SparseClusterer sc = new SparseClusterer();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
									run = sc.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
									run = sc.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Gene family inference failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
								updateEastPanelText(1);
								
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
								displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
								displayTextEastPanel.add(" =====================================================================================");
								updateEastPanelText(1);
							} else if(modulepanel.getSelectedIndex() == 3) {
								displayTextEastPanel.add("[METHOD] - PERFORMING GENFAMCLUST ANALYSIS - From Synteny Score");
								displayTextEastPanel.add(" =====================================================================================");
								displayTextEastPanel.add("[STATUS] - *** Calculating synteny scores ***");
								updateEastPanelText(1);
								
								SyntenyExtractor se = new SyntenyExtractor();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), ncfile.getName(), "-p", path.getAbsolutePath(), "-f", rsynfile.getName(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), ncfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Synteny score calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing synteny correlation scores ***");
								updateEastPanelText(1);
								
								SyntenyCorrelationScore scs = new SyntenyCorrelationScore();
								if (rsynfile != null) {
									String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName};
									run = scs.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + "_k" + neighborhoodsize + "_ms.SySc", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};									
									run = scs.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Synteny correlation calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Evaluating homologs with syntenic support ***");
								updateEastPanelText(1);
								
								Evaluator eva = new Evaluator();
								String arguments[] = {outputName + ".syc", "-p", path.getAbsolutePath(), "-a", evaathreshold, "-b", evabthreshold,"-o", outputName};
								run = eva.main(window, arguments);
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Homology score evaluation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
								updateEastPanelText(1);
								
								SparseClusterer sc = new SparseClusterer();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
									run = sc.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
									run = sc.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Gene family inference failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
								updateEastPanelText(1);
								
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
								displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
								displayTextEastPanel.add(" =====================================================================================");
								updateEastPanelText(1);
							} else if(modulepanel.getSelectedIndex() == 4) {
								displayTextEastPanel.add("[METHOD] - PERFORMING GENFAMCLUST ANALYSIS - From Synteny correlation");
								displayTextEastPanel.add(" =====================================================================================");
								displayTextEastPanel.add("[STATUS] - *** Computing synteny correlation scores ***");
								updateEastPanelText(1);
								
								SyntenyCorrelationScore scs = new SyntenyCorrelationScore();
								if (rsynfile != null) {
									String arguments1[] = {syntenyscorefile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName};
									run = scs.main(window, arguments1);
								} else {
									String arguments1[] = {syntenyscorefile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};									
									run = scs.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Synteny correlation calculation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Evaluating homologs with syntenic support ***");
								updateEastPanelText(1);
								
								Evaluator eva = new Evaluator();
								String arguments[] = {outputName + ".syc", "-p", path.getAbsolutePath(), "-a", evaathreshold, "-b", evabthreshold,"-o", outputName};
								run = eva.main(window, arguments);
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Homology score evaluation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
								updateEastPanelText(1);
								
								SparseClusterer sc = new SparseClusterer();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
									run = sc.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
									run = sc.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Gene family inference failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
								updateEastPanelText(1);
								
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
								displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
								displayTextEastPanel.add(" =====================================================================================");
								updateEastPanelText(1);
							} else if(modulepanel.getSelectedIndex() == 5) {
								displayTextEastPanel.add("[METHOD] - PERFORMING GENFAMCLUST ANALYSIS - From homology evaluation");
								displayTextEastPanel.add(" =====================================================================================");
								displayTextEastPanel.add("[STATUS] - *** Evaluating homologs with syntenic support ***");
								updateEastPanelText(1);
								
								Evaluator eva = new Evaluator();
								String arguments[] = {syntenycorrelationfile.getName(), "-p", path.getAbsolutePath(), "-a", evaathreshold, "-b", evabthreshold,"-o", outputName};
								run = eva.main(window, arguments);
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Homology score evaluation failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
								updateEastPanelText(1);
								
								SparseClusterer sc = new SparseClusterer();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
									run = sc.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), outputName + ".eva", "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
									run = sc.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Gene family inference failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
								updateEastPanelText(1);
								
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
								displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
								displayTextEastPanel.add(" =====================================================================================");
								updateEastPanelText(1);
							} else if(modulepanel.getSelectedIndex() == 6) {
								displayTextEastPanel.add("[METHOD] - PERFORMING GENFAMCLUST ANALYSIS - From gene family inference");
								displayTextEastPanel.add(" =====================================================================================");
								displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
								updateEastPanelText(1);
								
								SparseClusterer sc = new SparseClusterer();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), homologyevaluatorfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
									sc.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), homologyevaluatorfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
									sc.main(window, arguments1);
								}
								
								displayTextEastPanel.add(" -------------------------------------------------------------------------------------");
								displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
								updateEastPanelText(1);
								
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								} else {
									String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
								displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
								displayTextEastPanel.add(" =====================================================================================");
								updateEastPanelText(1);
							} else if(modulepanel.getSelectedIndex() == 7) {
								displayTextEastPanel.add("[METHOD] - PERFORMING GENFAMCLUST ANALYSIS - Postprocessing indexed families");
								displayTextEastPanel.add(" =====================================================================================");
								displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
								updateEastPanelText(1);
								
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments1[] = {genefamilyfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								} else {
									String arguments1[] = {genefamilyfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
									run = pp.main(window, arguments1);
								}
								if(run == false) {
									displayTextEastPanel.add("[ERROR] - Post-processing inferred gene families failed. Check executable files and file formats etc.");
									updateEastPanelText(3);
									return;
								}
								
								displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished GenFamClust Analyis ***");
								displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
								displayTextEastPanel.add(" =====================================================================================");
								updateEastPanelText(1);
							}							
						} else if (index == 2) {
							run = true;
							if(modulepanel.getSelectedIndex() == 0) {
								DataPreparator dp = new DataPreparator();
								if (rsynfile != null) {
									String arguments1[] = {qseqfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName, "-s", rsynfile.getName()};
									run = dp.main(window, arguments1);
								} else {
									String arguments1[] = {qseqfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};
									run = dp.main(window, arguments1);
								}
							} else if(modulepanel.getSelectedIndex() == 1) {
								BlastModule bm = new BlastModule();
								if (rseqfile != null) {
									if(proteinblast == true) {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-s", rseqfile.getName(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
										run = bm.main(window, arguments1);
									} else {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-s", rseqfile.getName(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
										run = bm.main(window, arguments1);
									}
								} else {
									if(proteinblast == true) {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
										run = bm.main(window, arguments1);
									} else {
										String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
										run = bm.main(window, arguments1);
									}
								}
							} else if(modulepanel.getSelectedIndex() == 2) {
								NeighborhoodCorrelation nc = new NeighborhoodCorrelation();
								String arguments1[] = {avablastfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", ncresourcepath.getAbsolutePath(), "-t", ncthreshold};
								run = nc.main(window, arguments1);
							} else if(modulepanel.getSelectedIndex() == 3) {
								SyntenyExtractor se = new SyntenyExtractor();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), ncfile.getName(), "-p", path.getAbsolutePath(), "-f", rsynfile.getName(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), ncfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-k", neighborhoodsize};
									run = se.main(window, arguments1);
								}
							} else if(modulepanel.getSelectedIndex() == 4) {
								SyntenyCorrelationScore scs = new SyntenyCorrelationScore();
								if (rsynfile != null) {
									String arguments1[] = {syntenyscorefile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName};
									run = scs.main(window, arguments1);
								} else {
									String arguments1[] = {syntenyscorefile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};									
									run = scs.main(window, arguments1);
								}
							} else if(modulepanel.getSelectedIndex() == 5) {
								Evaluator eva = new Evaluator();
								String arguments[] = {syntenycorrelationfile.getName(), "-p", path.getAbsolutePath(), "-a", evaathreshold, "-b", evabthreshold,"-o", outputName};
								run = eva.main(window, arguments);
							} else if(modulepanel.getSelectedIndex() == 6) {
								SparseClusterer sc = new SparseClusterer();
								if (rsynfile != null) {
									String arguments1[] = {qsynfile.getName(), homologyevaluatorfile.getName(), "-p", path.getAbsolutePath(), "-r", rsynfile.getName(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};
									run = sc.main(window, arguments1);
								} else {
									String arguments1[] = {qsynfile.getName(), homologyevaluatorfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
									run = sc.main(window, arguments1);
								}
							} else if(modulepanel.getSelectedIndex() == 7) {
								PostProcessData pp = new PostProcessData();
								if (rsynfile != null) {
									String arguments[] = {genefamilyfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-sy", rsynfile.getName(), "-o", outputName};
									run = pp.main(window, arguments);
								} else {
									String arguments[] = {genefamilyfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName};
									run = pp.main(window, arguments);
								}
							}
							if(run == false) {
								displayTextEastPanel.add(" [ERROR] - Module execution failed. Check README for format and write permissions in the output directory.");
								updateEastPanelText(3);
								return;
							}
							displayTextEastPanel.add(" =====================================================================================");
							updateEastPanelText(1);
						} 
						runProgram.setEnabled(false);
					}
				};
				workerThreads[0].start();
			}
		});
		
		helpButton.setBackground(Color.WHITE);
		helpButton.setToolTipText("The button is enabled, when all compulsory arguments (in right text panel for selected program) are met! Run the program selected above with the parameter settings displayed!");
		helpButton.setEnabled(true);
		
		//Button listener - removes tree markings inside the graph.
		helpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				workerThreads[0] = new Thread() {
					public void run() {
						Integer index = droplist.getSelectedIndex();//get the selected item
						if(index == 0) {
							displayTextEastPanel.add(" **** Run complete GenFamClust as a complete pipeline****");
						} else if (index == 1) {
							displayTextEastPanel.add(" **** Run GenFamClust starting from a particular module");
							displayTextEastPanel.add(" to the end of the pipeline****");
						} else if (index == 2) {
							displayTextEastPanel.add(" **** Run a particular module of GenFamClust only.");
						}
						updateEastPanelText(2);
					}
				};
				workerThreads[0].start();
			}
		});
	}
	
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private void initDefaultButtonsNC() {
		runProgram.setBackground(Color.WHITE);
		runProgram.setToolTipText("The button is enabled, when all compulsory arguments (in right text panel for selected program) are met! Run the program selected above with the parameter settings displayed!");
		runProgram.setEnabled(false);
		
		//Button listener - removes tree markings inside the graph.
		runProgram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for(int t=0; t<workerThreads.length; t++)
					workerThreads[t].interrupt();
				workerThreads[0] = new Thread() {
					public void run() {
						Integer index = droplist.getSelectedIndex();//get the selected item
						boolean status;
						if(index == 0) {
							status = executeNCClustering();
							if(status == false) {
								displayTextEastPanel.add("[ERROR] - Unable to perform NC score calculation. Gene family inference by clustering on NC aborted.");
								updateEastPanelText(3);
							}
						} else if (index == 1) {
							status = executeSiLiXClustering(true);
							if(status == false) {
								displayTextEastPanel.add("[ERROR] - Unable to perform single linkage clustering. Gene family inference by SiLiX aborted.");
								updateEastPanelText(3);
							}
						} else if (index == 2) {
							
						} else if (index == 3) {
							status = executeHierarchicalClustering();
							if(status == false) {
								displayTextEastPanel.add("[ERROR] - Unable to perform hierarchical clustering. Gene family inference by hcluster aborted.");
								updateEastPanelText(3);
							}
						} else if (index == 4) {
							status = executeSiLiXClustering(false);
							if(status == false) {
								displayTextEastPanel.add("[ERROR] - Unable to perform single linkage clustering. Gene family inference by SiLiX aborted.");
								updateEastPanelText(3);
								return;
							}
							status = executeHiFiXClustering();
							if(status == false) {
								displayTextEastPanel.add("[ERROR] - Unable to perform high fidelity clustering. Gene family inference by HiFiX aborted.");
								updateEastPanelText(3);
							}
						}
						runProgram.setEnabled(false);
					}
				};
				workerThreads[0].start();
			}
		});
		
		helpButton.setBackground(Color.WHITE);
		helpButton.setToolTipText("The button is enabled, when all compulsory arguments (in right text panel for selected program) are met! Run the program selected above with the parameter settings displayed!");
		helpButton.setEnabled(true);
		
		//Button listener - removes tree markings inside the graph.
		helpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				workerThreads[0] = new Thread() {
					public void run() {
						Integer index = droplist.getSelectedIndex();//get the selected item
						if(index == 0) {
							displayTextEastPanel.add(" =====================================================================================");
							displayTextEastPanel.add(" **** Help for custom clustering on Neighborhood Correlation ****");
							displayTextEastPanel.add(" [INFO] - Software installation required: NCBI Blast and Neighborhood Correlation.");
							displayTextEastPanel.add(" *************************************************************************************");
							displayTextEastPanel.add(" [SOFTWARE] - NCBI Blast ");
							displayTextEastPanel.add(" [VERSION] - 2.6.0+");
							displayTextEastPanel.add(" [WEBSITE] - https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download");
							displayTextEastPanel.add(" [WEBSITE FOR DOWNLOAD] - ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.6.0/");
							displayTextEastPanel.add(" [INSTALLATION PREREQUISITES] - GCC compiler.");
							displayTextEastPanel.add(" [INSTALLATION INSTRUCTIONS] - Install using configure and make commands in case of source code.");
							displayTextEastPanel.add(" [TEST INSTALLATION] - 	Successful installation should produce blastp, blastn and makeblastdb");
							displayTextEastPanel.add(" 							executable files among other executable files in the ReleaseMT/bin");
							displayTextEastPanel.add(" 							folder.");
							displayTextEastPanel.add(" [CITATION] - Altschul, S.F., Madden, T.L., Schäffer, A.A., Zhang, J., Zhang, Z., Miller, W. & Lipman, D.J.");
							displayTextEastPanel.add(" 				(1997) \"Gapped BLAST and PSI-BLAST: a new generation of protein database search programs.\"");
							displayTextEastPanel.add(" 			 	Nucleic Acids Res. 25:3389-3402.");
							displayTextEastPanel.add(" *************************************************************************************");
							displayTextEastPanel.add(" [SOFTWARE] - Neighborhood Correlation (NC) ");
							displayTextEastPanel.add(" [VERSION] - 2.1");
							displayTextEastPanel.add(" [WEBSITE] - http://www.neighborhoodcorrelation.org/");
							displayTextEastPanel.add(" [WEBSITE FOR DOWNLOAD] - http://www.neighborhoodcorrelation.org/#download");
							displayTextEastPanel.add(" [INSTALLATION PREREQUISITES] - GCC compiler, Python 2.7 and Numpy numerical package for Python.");
							displayTextEastPanel.add(" [INSTALLATION INSTRUCTIONS] - All installation instructions are written in README on website.");
							displayTextEastPanel.add(" [TEST INSTALLATION] - 	Successful installation should produce NC_Standalone executable");
							displayTextEastPanel.add(" 							file, that should be ran using \"python2.7 NC_Standalone -h\" command");
							displayTextEastPanel.add(" 							from the folder containing NC_Standalone executable.");
							displayTextEastPanel.add(" [CITATION] - Song N, Joseph JM, Davis GB, Durand D");
							displayTextEastPanel.add(" 				(2008) \"Sequence Similarity Network Reveals Common Ancestry of Multidomain Proteins.\"");
							displayTextEastPanel.add(" 			 	PLoS Computational Biology 4(5): e1000063. doi:10.1371/journal.pcbi.1000063");							
							displayTextEastPanel.add(" =====================================================================================");
							updateEastPanelText(2);
						} else if (index == 1) {
							displayTextEastPanel.add(" =====================================================================================");
							displayTextEastPanel.add(" **** Help for single linkage clustering using SiLiX ****");
							displayTextEastPanel.add(" [INFO] - Software installation required: SiLiX.");
							displayTextEastPanel.add(" *************************************************************************************");
							displayTextEastPanel.add(" [SOFTWARE] - SiLiX ");
							displayTextEastPanel.add(" [VERSION] - 1.2.9");
							displayTextEastPanel.add(" [WEBSITE] - http://lbbe.univ-lyon1.fr/Overview.html?lang=fr");
							displayTextEastPanel.add(" [WEBSITE FOR DOWNLOAD] - ftp://pbil.univ-lyon1.fr/pub/logiciel/silix/");
							displayTextEastPanel.add(" [INSTALLATION PREREQUISITES] - G++ compiler, C++ Boost:program_options package.");
							displayTextEastPanel.add(" [INSTALLATION INSTRUCTIONS] - Install using configure and make commands in case of source code.");
							displayTextEastPanel.add(" [TEST INSTALLATION] - 	Successful installation should produce silix executable");
							displayTextEastPanel.add(" 							file among other executable files. ");
							displayTextEastPanel.add(" [CITATION] - Miele V, Penel S, Duret L");
							displayTextEastPanel.add(" 				(2011) \"Ultra-fast sequence clustering from similarity networks with SiLiX.\"");
							displayTextEastPanel.add(" 			 	BMC Bioinformatics 12:116.");
							displayTextEastPanel.add(" =====================================================================================");
							updateEastPanelText(2);
						} else if (index == 2) {
							displayTextEastPanel.add(" **** Help for gene family inference using Markov Clustering (MCL) ****");							
						} else if (index == 3) {
							displayTextEastPanel.add(" =====================================================================================");
							displayTextEastPanel.add(" **** Help for hierarchical clustering using hcluster ****");
							displayTextEastPanel.add(" [INFO] - Software installation required: hcluster");
							displayTextEastPanel.add(" *************************************************************************************");
							displayTextEastPanel.add(" [SOFTWARE] - hcluster_sg ");
							displayTextEastPanel.add(" [VERSION] - 1.0");
							displayTextEastPanel.add(" [WEBSITE] - http://lbbe.univ-lyon1.fr/Overview,3031.html?lang=fr");
							displayTextEastPanel.add(" [WEBSITE FOR DOWNLOAD] - ftp://pbil.univ-lyon1.fr/pub/logiciel/hifix/");
							displayTextEastPanel.add(" [INSTALLATION PREREQUISITES] - SiLiX, MAFFT, HMMER3, Louvain and Python modules :");
							displayTextEastPanel.add(" [INSTALLATION PREREQUISITES] - NumPy, argparse, subprocess, multiprocessing, BioPython.");
							displayTextEastPanel.add(" [INSTALLATION INSTRUCTIONS] - \"python setup.py test/” and \"python setup.py install\".");
							displayTextEastPanel.add(" [TEST INSTALLATION] - 	Successful installation should produce hifix executable");
							displayTextEastPanel.add(" 							file among other executable files. ");
							displayTextEastPanel.add(" [CITATION] - Miele V, Penel S, Daubin V, Picard F, Kahn D, Duret L");
							displayTextEastPanel.add(" 				(2012) \"High-quality sequence clustering guided by network topology and multiple alignment likelihood.\"");
							displayTextEastPanel.add(" 			 	Bioinformatics 28 (8): 1078-1085.");
							displayTextEastPanel.add(" =====================================================================================");
						} else if (index == 4) {
							displayTextEastPanel.add(" =====================================================================================");
							displayTextEastPanel.add(" **** Help for high fidelity clustering using HiFiX ****");
							displayTextEastPanel.add(" [INFO] - Software installation required: SiLiX, HiFiX");
							displayTextEastPanel.add(" *************************************************************************************");
							displayTextEastPanel.add(" [SOFTWARE] - HiFiX ");
							displayTextEastPanel.add(" [VERSION] - 1.0.5");
							displayTextEastPanel.add(" [WEBSITE] - http://lbbe.univ-lyon1.fr/Overview,3031.html?lang=fr");
							displayTextEastPanel.add(" [WEBSITE FOR DOWNLOAD] - ftp://pbil.univ-lyon1.fr/pub/logiciel/hifix/");
							displayTextEastPanel.add(" [INSTALLATION PREREQUISITES] - SiLiX, MAFFT, HMMER3, Louvain and Python modules :");
							displayTextEastPanel.add(" [INSTALLATION PREREQUISITES] - NumPy, argparse, subprocess, multiprocessing, BioPython.");
							displayTextEastPanel.add(" [INSTALLATION INSTRUCTIONS] - \"python setup.py test/” and \"python setup.py install\".");
							displayTextEastPanel.add(" [TEST INSTALLATION] - 	Successful installation should produce hifix executable");
							displayTextEastPanel.add(" 							file among other executable files. ");
							displayTextEastPanel.add(" [CITATION] - Miele V, Penel S, Daubin V, Picard F, Kahn D, Duret L");
							displayTextEastPanel.add(" 				(2012) \"High-quality sequence clustering guided by network topology and multiple alignment likelihood.\"");
							displayTextEastPanel.add(" 			 	Bioinformatics 28 (8): 1078-1085.");
							displayTextEastPanel.add(" =====================================================================================");
						}
						updateEastPanelText(2);
					}
				};
				workerThreads[0].start();
			}
		});
	}
	
	private boolean executeNCClustering() {
		boolean status = prepSyntenyFileFromSeqFile();
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to create Synteny file from sequence file. Gene family inference by clustering on NC aborted.");
			updateEastPanelText(3);
			return false;
		}
		DataPreparator dp = new DataPreparator();
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[STATUS] - *** Program Arguments ***");
		displayTextEastPanel.add("[INFO] - QSeqFile = " + qseqfile.getName());
		displayTextEastPanel.add("[INFO] - QSynFile = " + qsynfile.getName());
		displayTextEastPanel.add("[INFO] - path = " + "-p " + path.getAbsolutePath());
		displayTextEastPanel.add("[INFO] - output = " + "-o " + outputName);
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[STATUS] - *** Preprocessing data ***");
		updateEastPanelText(1);
		String arguments3[] = {qseqfile.getName(), qsynfile.getName(), "-p", path.getAbsolutePath() , "-o", outputName};
		status = dp.main(window, arguments3);
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to preprocess data. Gene family inference by clustering on NC aborted.");
			updateEastPanelText(3);
			return false;
		}

		displayTextEastPanel.add(" ---------------------------------------------------------");
		displayTextEastPanel.add("[STATUS] - *** Performing All-vs-All Blast ***");
		updateEastPanelText(1);
		
		BlastModule bm = new BlastModule();
		if(proteinblast == true) {
			String arguments1[] = {outputName + ".fasta", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
			status = bm.main(window, arguments1);
		} else {
			String arguments1[] = {outputName + ".fasta", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
			status = bm.main(window, arguments1);
		}
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform All-versus-All blast. Gene family inference by clustering on NC aborted.");
			updateEastPanelText(3);
			return false;
		}

		displayTextEastPanel.add(" ---------------------------------------------------------");
		displayTextEastPanel.add("[STATUS] - *** Calculating neighborhood correlation scores ***");
		updateEastPanelText(1);
		
		NeighborhoodCorrelation nc = new NeighborhoodCorrelation();
		String arguments2[] = {outputName + ".bl", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", ncresourcepath.getAbsolutePath(), "-t", ncthreshold};
		status = nc.main(window, arguments2);
		
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform NC score calculation. Gene family inference by clustering on NC aborted.");
			updateEastPanelText(3);
			return false;
		}
		
		displayTextEastPanel.add(" ---------------------------------------------------------");
		displayTextEastPanel.add("[STATUS] - *** Computing gene families ***");
		updateEastPanelText(1);
		
		SparseClusterer sc = new SparseClusterer();
		String arguments4[] = {qsynfile.getName(), outputName + ".nnc", "-p", path.getAbsolutePath(), "-o", outputName, "-t", clusterthreshold, "-m", clusteringmethod};									
		status = sc.main(window, arguments4);
		
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform gene family clustering. Gene family inference by clustering on NCg aborted.");
			updateEastPanelText(3);
			return false;
		}
		
		displayTextEastPanel.add(" ---------------------------------------------------------");
		displayTextEastPanel.add("[STATUS] *** Postprocessing gene families ***");
		updateEastPanelText(1);
		
		PostProcessData pp = new PostProcessData();
		String arguments1[] = {outputName + ".bcl", qsynfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName + "_Result"};
		status = pp.main(window, arguments1);
		
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform gene family clustering. Gene family inference by clustering on NC aborted.");
			updateEastPanelText(3);
			return false;
		}
		
		qsynfile.delete();
		
		displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY finished gene family inference by clustering on NC analyis ***");
		displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + "_Result.bcl");
		displayTextEastPanel.add(" ---------------------------------------------------------");
		updateEastPanelText(1);
		return true;
	}
	
	private boolean executeSiLiXClustering(boolean forSilix) {
		boolean status;
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[STATUS] - *** Program Arguments ***");
		displayTextEastPanel.add("[INFO] - QSeqFile = " + qseqfile.getName());
		displayTextEastPanel.add("[INFO] - path = " + "-p " + path.getAbsolutePath());
		displayTextEastPanel.add("[INFO] - output = " + "-o " + outputName);
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[METHOD] - *** Performing complete All-vs-All Blast ***");
		updateEastPanelText(1);
		
		BlastModule bm = new BlastModule();
		if(proteinblast == true) {
			String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p", "-cb"};
			status = bm.main(window, arguments1);
		} else {
			String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n", "-cb"};
			status = bm.main(window, arguments1);
		}
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform All-versus-All blast. Gene family inference by Silix aborted.");
			updateEastPanelText(3);
			return false;
		}
		
		displayTextEastPanel.add(" ---------------------------------------------------------");
		displayTextEastPanel.add("[METHOD] - *** Calculating gene families using SiLiX ***");
		updateEastPanelText(1);
		
		Silix silix = new Silix();
		if(forSilix) {
			String[] arguments2 = {qseqfile.getName(), outputName + ".bl", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", silixresourcepath.getAbsolutePath(), "-i", silixpercentidentity, "-r", silixminpercentoverlap};
			status = silix.main(window, arguments2);
		} else {
			String[] arguments2 = {qseqfile.getName(), outputName + ".bl", "-p", path.getAbsolutePath(), "-o", outputName + "_SLX", "-rp", silixresourcepath.getAbsolutePath(), "-i", silixpercentidentity, "-r", silixminpercentoverlap};
			status = silix.main(window, arguments2);
		}
		
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform silix computation. Gene family inference by Silix aborted.");
			updateEastPanelText(3);
			return false;
		}
		
		if(forSilix) {
			try {
				File silixResFile = new File(path.getAbsolutePath() + "/", outputName + ".fnodes");
				if(!silixResFile.exists()) {
					displayTextEastPanel.add("[ERROR] - SiLiX result file does not exist on " + path.getAbsolutePath() + "/" + outputName + ".bcl. Gene family inference by Silix aborted.");
					updateEastPanelText(3);
					return false;
				}
				silix.convertToBlastclust(silixResFile, path.getAbsolutePath() + "/", outputName + ".bcl");
			} catch(Exception e) {
				displayTextEastPanel.add("[ERROR] - Unable to convert SiLiX result file to Blastclust format. Gene family inference by Silix aborted.");
				updateEastPanelText(3);
				return false;
			}
			displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished SiLiX gene family analyis ***");
			displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + ".bcl");
			displayTextEastPanel.add(" ---------------------------------------------------------");
			updateEastPanelText(1);
		}
		return true;
	}
	
	private boolean executeHiFiXClustering() {
		boolean status;
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[STATUS] - *** Program Arguments ***");
		displayTextEastPanel.add("[INFO] - QSeqFile = " + qseqfile.getName());
		displayTextEastPanel.add("[INFO] - path = " + "-p " + path.getAbsolutePath());
		displayTextEastPanel.add("[INFO] - output = " + "-o " + outputName);
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[METHOD] - *** Calculating gene families using HiFiX ***");
		updateEastPanelText(1);
		
		Hifix hifix = new Hifix();
		String[] arguments2 = {qseqfile.getName() + ".fasta", outputName + ".net", outputName + "_SLX.fnodes", "-p", path.getAbsolutePath(), "-o", outputName, "-rp", silixresourcepath.getAbsolutePath()};
		status = hifix.main(window, arguments2);
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform hifix computation. Gene family inference by Hifix aborted.");
			updateEastPanelText(3);
			return false;
		}
		try {
			File silixResFile = new File(path.getAbsolutePath() + "/", outputName + ".fnodes");
			if(!silixResFile.exists()) {
				displayTextEastPanel.add("[ERROR] - HiFiX result file does not exist on " + path.getAbsolutePath() + "/" + outputName + ".fnodes. Gene family inference by HiFiX aborted.");
				updateEastPanelText(3);
				return false;
			}
			hifix.convertToBlastclust(silixResFile, path.getAbsolutePath() + "/", outputName + ".bcl");
		} catch(Exception e) {
			displayTextEastPanel.add("[ERROR] - Unable to convert HiFiX result file to Blastclust format. Gene family inference by HiFiX aborted.");
			updateEastPanelText(3);
			return false;
		}
		displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished HiFiX gene family Analyis ***");
		displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + ".bcl");
		displayTextEastPanel.add(" ---------------------------------------------------------");
		updateEastPanelText(1);
		return true;
	}
	
	private boolean executeHierarchicalClustering() {
		boolean status;
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[STATUS] - *** Program Arguments ***");
		displayTextEastPanel.add("[INFO] - QSeqFile = " + qseqfile.getName());
		displayTextEastPanel.add("[INFO] - path = " + "-p " + path.getAbsolutePath());
		displayTextEastPanel.add("[INFO] - output = " + "-o " + outputName);
		displayTextEastPanel.add(" *********************************************************");
		displayTextEastPanel.add("[METHOD] - *** Performing complete All-vs-All Blast ***");
		updateEastPanelText(1);
		
		BlastModule bm = new BlastModule();
		if(proteinblast == true) {
			String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "p"};
			status = bm.main(window, arguments1);
		} else {
			String arguments1[] = {qseqfile.getName(), "-p", path.getAbsolutePath(), "-o", outputName, "-rp", blastresourcepath.getAbsolutePath(), "-e", blastevalue, "-t", "n"};
			status = bm.main(window, arguments1);
		}
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform All-versus-All blast. Gene family inference by hcluster aborted.");
			updateEastPanelText(3);
			return false;
		}
		
		displayTextEastPanel.add(" ---------------------------------------------------------");
		displayTextEastPanel.add("[METHOD] - *** Calculating gene families using hcluster ***");
		updateEastPanelText(1);
		
		Hcluster hcluster = new Hcluster();
		String[] arguments2 = {outputName + ".bl", "-w", hclusterminedgeweight, "-s", hclusterminedgedensity, "-m", hclustermaxsize, "-b", hclusterbreakingedgedensity, "-p", path.getAbsolutePath(), "-o", outputName, "-rp", hclusterresourcepath.getAbsolutePath()};
		status = hcluster.main(window, arguments2);
		if(status == false) {
			displayTextEastPanel.add("[ERROR] - Unable to perform hcluster computation. Gene family inference by hcluster aborted.");
			updateEastPanelText(3);
			return false;
		}
		try {
			File hclusterResFile = new File(path.getAbsolutePath() + "/", outputName + ".hcl");
			if(!hclusterResFile.exists()) {
				displayTextEastPanel.add("[ERROR] - hcluster result file does not exist on " + path.getAbsolutePath() + "/" + outputName + ".hcl. Gene family inference by hcluster aborted.");
				updateEastPanelText(3);
				return false;
			}
			hcluster.convertToBlastclust(qseqfile, hclusterResFile, path.getAbsolutePath() + "/", outputName + ".bcl");
		} catch(Exception e) {
			displayTextEastPanel.add("[ERROR] - Unable to convert hcluster result file to Blastclust format. Gene family inference by hcluster aborted.");
			updateEastPanelText(3);
			return false;
		}
		displayTextEastPanel.add("[METHOD] - *** SUCCESSFULLY Finished hcluster gene family Analyis ***");
		displayTextEastPanel.add("[RESULT] - Gene families can be found in " + path.getAbsolutePath() + "/" + outputName + ".bcl");
		displayTextEastPanel.add(" ---------------------------------------------------------");
		updateEastPanelText(1);
		return true;
	}
	
	private void enableExecuteButton() {
		runProgram.setEnabled(false);
		if(path != null && !outputName.equals("")) {
			if(progName.equals("GFC")) {
				if(droplist.getSelectedIndex() == 0) {
					if(blastresourcepath != null && ncresourcepath != null && qsynfile != null && qseqfile != null)
						runProgram.setEnabled(true);
				} else if(droplist.getSelectedIndex() == 1) {
					if(qsynfile != null){
						if(modulepanel.getSelectedIndex() == 0 || modulepanel.getSelectedIndex() == 1) {
							if(blastresourcepath != null && ncresourcepath != null && qseqfile != null)
								runProgram.setEnabled(true);
						} else if(modulepanel.getSelectedIndex() == 2) {
							if(ncresourcepath != null && avablastfile != null)
								runProgram.setEnabled(true);
						} else if(modulepanel.getSelectedIndex() == 3) {
							if(ncfile != null)
								runProgram.setEnabled(true);
						} else if(modulepanel.getSelectedIndex() == 4) {
							if(syntenyscorefile != null)
								runProgram.setEnabled(true);
						} else if(modulepanel.getSelectedIndex() == 5) {
							if(syntenycorrelationfile != null)
								runProgram.setEnabled(true);
						} else if(modulepanel.getSelectedIndex() == 6) {
							if(homologyevaluatorfile != null)
								runProgram.setEnabled(true);
						} else if(modulepanel.getSelectedIndex() == 7) {
							if(genefamilyfile != null)
								runProgram.setEnabled(true);
						}
					}
				} else {
					if(modulepanel.getSelectedIndex() == 0) {
						if(path != null && qsynfile != null && qseqfile != null)
							runProgram.setEnabled(true);
					} else if(modulepanel.getSelectedIndex() == 1) {
						if(path != null && blastresourcepath != null && qseqfile != null)
							runProgram.setEnabled(true);
					} else if(modulepanel.getSelectedIndex() == 2) {
						if(path != null && ncresourcepath != null && avablastfile != null)
							runProgram.setEnabled(true);
					} else if(modulepanel.getSelectedIndex() == 3) {
						if(path != null && qsynfile != null && ncfile != null)
							runProgram.setEnabled(true);
					} else if(modulepanel.getSelectedIndex() == 4) {
						if(path != null && qsynfile != null && syntenyscorefile != null)
							runProgram.setEnabled(true);
					} else if(modulepanel.getSelectedIndex() == 5) {
						if(path != null && qsynfile != null && syntenycorrelationfile != null)
							runProgram.setEnabled(true);
					} else if(modulepanel.getSelectedIndex() == 6) {
						if(path != null && qsynfile != null && homologyevaluatorfile != null)
							runProgram.setEnabled(true);
					} else if(modulepanel.getSelectedIndex() == 7) {
						if(path != null && qsynfile != null && genefamilyfile != null)
							runProgram.setEnabled(true);
					}
				}
			} else {
				if (progName.equals("NC")) {
					if(((blastresourcepath != null && qseqfile != null) || (avablastfile != null)) && ncresourcepath != null)
						runProgram.setEnabled(true);
				} else if (progName.equals("silix") || progName.equals("hifix")) {
					if(((blastresourcepath != null) || (avablastfile != null)) && qseqfile != null && silixresourcepath != null)
						runProgram.setEnabled(true);
				} else if (progName.equals("mcl")) {
					if(((blastresourcepath != null && qseqfile != null) || (avablastfile != null)) && mclresourcepath != null)
						runProgram.setEnabled(true);
				}  else if (progName.equals("hcluster")) {
					if(((blastresourcepath != null && qseqfile != null) || (avablastfile != null)) && hclusterresourcepath != null)
						runProgram.setEnabled(true);
				}
			}
		}
	}
	
	private boolean prepSyntenyFileFromSeqFile() {
		SimpleFileIO reader = new SimpleFileIO();
		try {
			File synteny = new File(path.getAbsolutePath() + "/" + qseqfile.getName() + ".hsf");
			if(synteny.exists())
				synteny.delete();
			reader.readSequenceFile(path.getAbsolutePath() + "/", qseqfile.getName());
			ArrayList<Sequence> sequences = reader.getSequences();
			for(int i = 0; i < sequences.size(); i++) {
				SequenceFileWriter.writeAndAppendLine(path.getAbsolutePath() + "/", qseqfile.getName() + ".hsf", "Spec1\tChr1\t" + sequences.get(i).getIdentifier() + "\t" + i);
			}
			qsynfile = new File(path.getAbsolutePath(), qseqfile.getName() + ".hsf");
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public void setparent() { window = this; }

	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
