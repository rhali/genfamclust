package miscellaneous.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class HomologyMain extends JFrame {
	private HomologyMain window;
	private static final long serialVersionUID = 1L;
	private JTabbedPane 		tabs = new JTabbedPane();
	
	public HomologyMain() {
		tabs.setBackground(new Color(0xFFDDDDFF));
		tabs.setFocusable(false);
		tabs.setBorder(BorderFactory.createLineBorder(new Color(0xFFEEEEFF), 2));
		
		GFCWindow gfcmain = new GFCWindow("GFC");
		gfcmain.setparent();
		addTab("Syteny supported homology", gfcmain.getMainPanel());
		GFCWindow ncmain = new GFCWindow("NC");
		ncmain.setparent();
		addTab("Similarity based homology", ncmain.getMainPanel());
		
		this.setContentPane(tabs);
		this.setMinimumSize(new Dimension(1260, 980));
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public void addTab(String title, Component panel) {
		tabs.add(title, panel);
		this.pack();
	}
	public void appear() {tabs.setVisible(true);}
	public void windowAppear() {this.setVisible(true);}
	public void selectTab(int index) {tabs.setSelectedIndex(index);}
	public void getTab(int index) {tabs.getComponentAt(index);}
	public JTabbedPane getTabs() {return tabs;}
	public void setparent() { window = this; }
}
