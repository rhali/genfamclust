package miscellaneous.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

public class GFCX {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private HomologyMain window;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	/** Definition: 			Default constructor for VMCMC.										
	 *	<p>Usage: 				When no filename is provided as input parameter.						
 	 *	<p>Function:			Opens up the first window that is used to supply input file. 				
 	 *	<p>Classes: 			MCMCWindow.
 	 *	<p>Internal Functions:	createMenuBar(). 		
 	 *	@return: 				A new graphical basic window by invoking MCMCWindow.					
	 */
	public GFCX() {
		if (System.getProperty("os.name").contains("Mac") || System.getProperty("os.name").contains("mac")) {
			System.setProperty("apple.laf.useScreenMenuBar", "true");
			System.setProperty("apple.awt.application.name", "GenFamClustX");
		}
		/* ******************** FUNCTION VARIABLES ******************************** */
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		window 				= new HomologyMain();
		window.setparent();
		
		/* ******************** FUNCTION BODY ************************************* */
		window.setTitle("GenFamClustX");	
		window.setJMenuBar(createMenuBar());	
		window.validate();

		UIManager.put("TabbedPane.selected"			, new Color(0xFFEEEEFF));	
		UIManager.put("TabbedPane.contentAreaColor"	, new Color(0xFFEEEEFF));
		UIManager.put("TabbedPane.shadow"			, new Color(0xFF000000));
		
		/* ******************** END OF FUNCTION *********************************** */				
	}
	
	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	/** Definition: 			Private method for opening the first graphical window for VMCMC.										
	<p>Usage: 				When no filename is provided as input parameter, the default constructor with no file input refers to this function for coming up with the first interface.		
 	<p>Function:			Populates the first window and all its other options like about and exit etc. that are displayed in the first window. 	
 	<p>Classes: 			MCMCDataContainer, MCMCFileReader, MCMCMath.		
	<p>Internal Functions:	None.		
 	@return: 				A populated graphical basic window and handles all options used inside it.					
	 */
	private JMenuBar createMenuBar() {
		/* ******************** FUNCTION VARIABLES ******************************** */
		JMenuBar 		menubar;
		JMenu 			menuFile;
		JMenu 			VMCMCExamples;
		JMenuItem 		itemOpen;
		JMenuItem 		itemClose;
//		final JMenuItem parallelChain;
		JMenu 			menuAbout;  
		JMenuItem 		itemAbout;
		JMenuItem 		tutorial;
//		JMenuItem 		PrimeExampleItem;
//		JMenuItem		JprimeExampleItem;
//		JMenuItem		MrbayesExampleItem;
//		JMenuItem		LargeExampleItem;
//		JMenuItem		MrbayesParallelExampleItem;
//		JMenuItem		MrBayesStationaryTreeExampleItem;
//		JMenuItem		BeastExampleItem;
//		JMenuItem		SampleParallelRunItem;
//		JMenuItem		SampleParallelConvergedItem;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		menubar =  new JMenuBar();
		menuFile = new JMenu("File");
		itemOpen = new JMenuItem("Open file");
//		parallelChain = new JMenuItem("Parallel Chain Analysis");
		itemClose = new JMenuItem("Exit");
		menuAbout = new JMenu("Help");  
		itemAbout = new JMenuItem("About GFC");
		tutorial = new JMenuItem("Help");
		VMCMCExamples = new JMenu("Examples");
//		PrimeExampleItem = new JMenuItem("Sample Run - PrIMe");
//		JprimeExampleItem = new JMenuItem("Sample Run - JPrIMe");
//		LargeExampleItem = new JMenuItem("Sample Run - Uniform Tree Posterior");
//		MrbayesParallelExampleItem = new JMenuItem("Sample Run (Parallel) - MrBayes");
//		MrbayesExampleItem = new JMenuItem("Sample Run - MrBayes");
//		MrBayesStationaryTreeExampleItem	= new JMenuItem("Sample Run - MrBayes Stationary Tree");
//		BeastExampleItem	= new JMenuItem("Sample Run - Beast");
//		SampleParallelRunItem = new JMenuItem("Sample Parallel Run - JPrIMe");
//		SampleParallelConvergedItem = new JMenuItem("Sample Parallel Run - PrIMe");
		
		/* ******************** FUNCTION BODY ************************************* */
//		parallelChain.setVisible(false);
		
		//Menu actionlistener responsible for closing application.
		itemClose.addActionListener(new ActionListener() {   
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		//Menu actionlistener responsible for creating filechooser, datacontainer and tabs.
		itemOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {   
				File file = null;

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;
						
						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("fas")||extension.equals("txt") || extension.equals("out") || extension.equals("fsa")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "Synteny/Sequence File";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					file = chooser.getSelectedFile();
//					parallelChain.setVisible(true);
//					MCMCFileAnalyser.fileOpener(file, window, false, false);
				}
			}
		});
		
/*		parallelChain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {   
				File file1 = null;

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;

						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension 		= name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("mcmc")||extension.equals("txt") ) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "MCMC Files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION)
					file1 = chooser.getSelectedFile();

				if(file1 != null) {					
//					MCMCFileAnalyser.fileOpener(file1, window, true, true);
//					MCMCFileAnalyser.parallelFileOpener(file1, window);
					parallelChain.setVisible(false);
				}
			}
		});
*/
		//Menu actionlistener responsible for displaying "about" window
		itemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				MCMCAboutAndHelpItems.itemAboutMenu(window);
			}
		});
		
/*		PrimeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.mcmcExample(window, "PrIMe");
			}
		});
*//*		
		JprimeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.mcmcExample(window, "JPrIMe");
			}
		});
*/		
/*		LargeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.mcmcLargeDatasets(window);
			}
		});
*/		
/*		MrBayesStationaryTreeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.mcmcExample(window, "MrBayesStationary");
			}
		});
*/		
/*		BeastExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.beastExample(window);
			}
		});
*/		
/*		MrbayesParallelExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.mrBayesParallelExample(window);
			}
		});
*/		
/*		MrbayesExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.mrBayesExample(window);
			}
		});
*/		
/*		SampleParallelRunItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCExamples.mcmcParallelExample(window);
			}
		});
*/		
/*		SampleParallelConvergedItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mcmcConvergedParallelExample(window);
			}
		});
*/		
		tutorial.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
//				MCMCAboutAndHelpItems.openTutorial(window);
			}
		});
		menuFile.add(itemOpen);
		menuFile.addSeparator();
//		VMCMCExamples.add(PrimeExampleItem);
//		VMCMCExamples.add(JprimeExampleItem);
//		VMCMCExamples.add(BeastExampleItem);
//		VMCMCExamples.add(MrbayesExampleItem);
//		VMCMCExamples.add(MrBayesStationaryTreeExampleItem);
//		VMCMCExamples.add(LargeExampleItem);
		VMCMCExamples.addSeparator();
//		VMCMCExamples.add(MrbayesParallelExampleItem);
//		VMCMCExamples.add(SampleParallelRunItem);
//		VMCMCExamples.add(SampleParallelConvergedItem);
		menuFile.addSeparator();
//		menuFile.add(parallelChain);
		menuFile.add(itemClose);
		menubar.add(menuFile);
		menubar.add(VMCMCExamples);
		menuAbout.add(tutorial);
		menuAbout.add(itemAbout);
		menubar.add(menuAbout);
		return menubar;
		
		/* ******************** END OF FUNCTION *********************************** */
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */	
	/** Definition: 			Returns the private variable .											  		
 	@return 					Graphical window.				
	 */
	public HomologyMain getWindow() {return window;}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
