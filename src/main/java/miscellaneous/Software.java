package miscellaneous;

/**
 * 
 * @author Sayyed Auwn Muhammad
 * @author Raja Hashim Ali
 *
 */
public interface Software {
//	static final String ABSOLUTE_PATH = "/bubo/sw/apps/bioinfo/blast/2.2.25+/kalkyl/bin/";
	static final String ABSOLUTE_PATH = "/Users/rhali/Documents/Eclipse/Resources/Blast/bin/";
	public void setRelativePath(String resourcePath, String dataPath, String resultPath);
	public String[] makeCommand();
	public boolean runCommand(String[] command);
}
