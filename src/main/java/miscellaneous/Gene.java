package miscellaneous;

import java.util.ArrayList;

import miscellaneous.Couple;

/**
 * The class that contains all information and methods for the gene. A gene has an
 * identifier, a start position, index in the chromosome, some blast results, some 
 * Neighborhood Correlation and Syntney Correlation results with other genes. All of
 * these are stored in this class and can be retrieved.
 * @author Raja Hashim Ali
 *
 */
public class Gene {
	/*'******************************************************************************
	 * 							CLASS VARIABLES										*
	 ********************************************************************************/
	/** The unique identifier or name of the gene */
	private String 								geneID;
	
	/** The start position in terms of base pair start on the chromosome for the gene */
	private int 								startPos;
	
	/** The index of the gene in the format speciesIndex.chrIndex.geneIndex */
	private String 								index;
	
	private int 								positioninCluster;
	
	/** List of all the genes that this gene has some similarity with and their
	 * similarity scores using the Blast algorithm.
	 */
	private ArrayList<Couple<String, Float>> 	blastResult;
	
	/** List of all the genes that this gene has some similarity with and their
	 * NC scores using the Neighborhood Correlation algorithm. Value between 0 and 1.
	 */
	private ArrayList<Couple<String, Float>>	ncScore;
	
	/** A list of of genes, which share some similarity (NC score > threshold) and their 
	 * Synteny scores.
	 */
	private ArrayList<Float> 	syntenyScores;
	
	/** A list of of genes, which share some similarity (NC score > threshold) and their 
	 * Synteny Correlation scores.
	 */
	private ArrayList<Float> 	syntenyCorrelationScores;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	/** Sets the identifier of the gene and the start position of the gene on the 
	 * chromosome (essential attributes) and initializes the lists of blast results, 
	 * neighborhood correlation results and synteny results 
	 * */
	public Gene(String geneID, int startPos, int positioninCluster) {
		this.geneID = geneID;
		this.startPos = startPos;
		blastResult = new ArrayList<Couple<String, Float>>();
		ncScore = new ArrayList<Couple<String, Float>>();
		syntenyScores = new ArrayList<Float>();
		syntenyCorrelationScores = new ArrayList<Float>();
		this.positioninCluster = positioninCluster;
	}
	
	/*'******************************************************************************
	 * 							CLASS PUBLIC FUNCTIONS								*
	 ********************************************************************************/	
	/** Add the blast results one by one for each gene using the gene identifier and
	 * the bit score.
	 */
	public void addBlastResult(String blastGeneID, Float bitScore) {
		blastResult.add(new Couple<String, Float>(blastGeneID, bitScore));
	}
	
	/** Add the neighborhood correlation results one by one for each gene using the gene 
	 * identifier and the neighborhood correlation (NC) score.
	 */
	public void addncScore(String ncGeneID, Float ncScore) {
		this.ncScore.add(new Couple<String, Float>(ncGeneID, ncScore));
	}
	
	/** Add the syntenic score */
	public void addSyntenyScore(Float syntenyScore) {
		syntenyScores.add(syntenyScore);
	}
	
	/** Add the Syntenic Correlation score */
	public void addSyntenyCorrelationScore(Float syntenyCorrelationScore) {
		syntenyCorrelationScores.add(syntenyCorrelationScore);
	}
	
	/** Get the list of all the blast results for this gene */
	public ArrayList<Couple<String, Float>> getGeneBlastResult() { return blastResult; }
	
	/** Get the list of all the neighborhood correlation (NC) results for this gene */
	public ArrayList<Couple<String, Float>> getGenencScore() { return ncScore; }
	
	/** Get the list of all the syntney scores results for this gene */
	public ArrayList<Float> getSyntenyScore() 	{ return syntenyScores; }
	
	/** Get the list of all the syntney correlation scores results for this gene */
	public ArrayList<Float> getSyntenyCorrelationScore() 	{ return syntenyCorrelationScores; }
	
	/** Get the identifier/name of this gene */
	public String getGeneID() 				{ return geneID; }
	
	/** Get the start position of this gene on the chromosome in base pairs */
	public int getStartpos() 				{ return startPos; }
	
	/** Set the index of this gene */
	public void setindex(String index) 		{ this.index = index; }
	
	/** Get the index of this gene */
	public String getIndex() 				{ return index; }
	
	public int getPositionInCluster() 		{ return positioninCluster; }
	
	/*'******************************************************************************
	 * 							END OF CLASS										*
	 ********************************************************************************/
}
