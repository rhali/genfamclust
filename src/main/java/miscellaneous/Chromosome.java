package miscellaneous;

import java.util.ArrayList;
/**
 * A class containing all the data and methods related to chromosome i.e. chromosome
 * name, index and a list of genes it contains. 
 * @author Raja Hashim Ali
 *
 */
public class Chromosome {
	/*'******************************************************************************
	 * 							CLASS VARIABLES										*
	 ********************************************************************************/
	/** Name of the chromosome */
	private String 			chromosomeName;
	
	/** Type of chromosome whether circular or linear. */
	private String 			type;
	
	/** List of all genes contained on this chromosome. */
	private ArrayList<Gene>	geneList;
	
	/** Index with which to access this chromosome from the container species. */
	private int 			chromosomeIndex;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	/** Sets the name and type of the chromosome (essential attributes) and initializes
	 * the list of genes.
	 * @param chromosomeName the name of the chromosome.
	 * @param type the type of chromosome whether circular of linear.
	 */
	public Chromosome(String chromosomeName, String type) {
		this.chromosomeName = chromosomeName; 
		this.type = type; 
		geneList = new ArrayList<Gene>();
	}
	
	/*'******************************************************************************
	 * 							CLASS PRIVATE FUNCTIONS								*
	 ********************************************************************************/
	/** Sets the index of the gene at the position in genelist */
	private void setGeneIndex(int position, String index) { 
		geneList.get(position).setindex(index); 
	}
	
	/*'******************************************************************************
	 * 							CLASS PUBLIC FUNCTIONS								*
	 ********************************************************************************/
	/** Get the total genes contained in the chromosome */
	public int getNumberOfGenes() 				{ return geneList.size(); }
	
	/** Get the name of the chromosome */
	public String getChrName() 					{ return chromosomeName; }
	
	/** Get the type of the chromosome */
	public String getChrType() 					{ return type; }
	
	/** Get the list of genes contained in the chromosome */
	public ArrayList<Gene> getGeneList()		{ return geneList; }
	
	/** Add a gene in the genelist of this chromosome */
	public void addGene(Gene gene)				{ geneList.add(gene); }
	
	/** Set the index of this chromosome */
	public void setIndex(int chromosomeIndex) 	{ this.chromosomeIndex = chromosomeIndex; }
	
	/** Get the index of this chromosome */
	public int getIndex() 						{ return chromosomeIndex; }
	
	/** Set the indexes of all genes contained in this chromosome in the format
	 * speciesIndex.chromomeIndex.geneIndex */
	public void setGeneIndices(String speciesIndex, String chromosomeIndex) {
		for(int i = 0; i < geneList.size(); i++)
			setGeneIndex(i, speciesIndex + "." + chromosomeIndex + "." + i);
	}
	
	/*'******************************************************************************
	 * 							END OF CLASS										*
	 ********************************************************************************/
}
