package modules.dataPostprocess;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Handles all regular application parameters for VMCMC. Used by MCMCMainApplication class for parameter parsing in the main function.
 * @author Raja Hashim Ali.
 */
public class Parameters {

	/** Required parameters: Application/Module to run */
	@Parameter(description = "<Indexed gene family file> <Query synteny file>")
	public List<String> files = new ArrayList<String>();
	
	/** Help. */
	@Parameter(names = {"-h", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-p","--path"}, description = "Path for finding the data and outputting results.")
	public String path = "./";
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-sy","--refsynteny"}, description = "File containing query synteny information.")
	public String refsynteny = "";
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-o","--output"}, description = "Name of all output files with corresponding file extensions.")
	public String output = "GenFam.bcl";
}
