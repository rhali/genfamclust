package modules.dataPostprocess;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import miscellaneous.Species;
import miscellaneous.gui.GFCWindow;
import modules.GenFamModules;

public class PostProcessData implements GenFamModules {
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public PostProcessData() {
	}
	
	public PostProcessData(String path, String refSyntenyFile, String querySyntenyFile, String clusterFile, String output) throws Exception {
		SimpleFileIO reader;
		ArrayList<Species> speciesList;

		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df.format(cal.getTime()));
		
		reader = new SimpleFileIO();
		
		if(refSyntenyFile != "") {
			System.out.print("Reading synteny information from " + refSyntenyFile + " ... ");
			reader.readSyntenyFile(path, refSyntenyFile);
			System.out.println(" DONE \n");
		}
		else
			System.out.print("No ref sequence information provided ... Taking query cases as reference cases as well ...");
		
		System.out.print("Reading synteny information from " + querySyntenyFile + " ... ");
		reader.readSyntenyFile(path, querySyntenyFile);
		System.out.println(" DONE \n");
				
		System.out.println("Processing data ... Please wait ... ");
		
		try{
			String str;
			speciesList = reader.getSpeciesList();
			
			BufferedReader bufferedreader;
			bufferedreader = new BufferedReader(new FileReader(path + clusterFile));
			while ((str = bufferedreader.readLine()) != null) {
				String[] strArr = str.split("\\s");
				String[] strArr1 = strArr[0].split("\\.");
				SequenceFileWriter.writeAndAppendString(path, output, speciesList.get(Integer.parseInt(strArr1[0])).getChromosomes().get(Integer.parseInt(strArr1[1])).getGeneList().get(Integer.parseInt(strArr1[2])).getGeneID());
				for(int i = 1; i < strArr.length; i++) {
					strArr1 = strArr[i].split("\\.");
					SequenceFileWriter.writeAndAppendString(path, output, "\t" + speciesList.get(Integer.parseInt(strArr1[0])).getChromosomes().get(Integer.parseInt(strArr1[1])).getGeneList().get(Integer.parseInt(strArr1[2])).getGeneID());
				}
				SequenceFileWriter.writeAndAppendLine(path, output, "");
			}
			bufferedreader.close();
		} catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		Calendar cal1 = Calendar.getInstance();
	    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df1.format(cal1.getTime()));
		
		System.out.println("Dataset Processed and Prepared ... COMPLETED");
	}
	
	public PostProcessData(GFCWindow window, String path, String refSyntenyFile, String querySyntenyFile, String clusterFile, String output) throws Exception {
		SimpleFileIO reader;
		ArrayList<Species> speciesList;

		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	    window.addMessageForReader(" [METHOD] - *** Starting to execute postprocessor *** ");
		window.addMessageForReader(" [METHOD] - Current time: " + df.format(cal.getTime()));
		window.updateEastPanelText(2);
		
		reader = new SimpleFileIO();
		
		if(refSyntenyFile != "") {
			reader.readSyntenyFile(path, refSyntenyFile);
		    window.addMessageForReader(" [METHOD] - Read synteny information from " + refSyntenyFile + " ... ");
			window.updateEastPanelText(2);
		} else {
			window.addMessageForReader(" [METHOD] - No ref sequence information provided ... Taking query cases as ref cases as well ...");
			window.updateEastPanelText(2);
		}
		
		reader.readSyntenyFile(path, querySyntenyFile);
		window.addMessageForReader(" [METHOD] - Read synteny information from " + querySyntenyFile + " ... ");
		window.updateEastPanelText(2);
				
		window.addMessageForReader(" [METHOD] - Processing data ... Please wait ... ");
		window.updateEastPanelText(2);
		
		try{
			String str;
			int count = 0;
			speciesList = reader.getSpeciesList();
			
			BufferedReader bufferedreader;
			bufferedreader = new BufferedReader(new FileReader(path + clusterFile));
			while ((str = bufferedreader.readLine()) != null) {
				count++;
				String[] strArr = str.split("\\s");
				String[] strArr1 = strArr[0].split("\\.");
				SequenceFileWriter.writeAndAppendString(path, output, speciesList.get(Integer.parseInt(strArr1[0])).getChromosomes().get(Integer.parseInt(strArr1[1])).getGeneList().get(Integer.parseInt(strArr1[2])).getGeneID());
				for(int i = 1; i < strArr.length; i++) {
					strArr1 = strArr[i].split("\\.");
					SequenceFileWriter.writeAndAppendString(path, output, "\t" + speciesList.get(Integer.parseInt(strArr1[0])).getChromosomes().get(Integer.parseInt(strArr1[1])).getGeneList().get(Integer.parseInt(strArr1[2])).getGeneID());
				}
				SequenceFileWriter.writeAndAppendLine(path, output, "");
				if(count%100 == 0) {
					window.addMessageForReader(" [METHOD] - Post-processed " + count + " gene families ... Please wait");
					window.updateEastPanelText(2);
				}
			}
			bufferedreader.close();
			Calendar cal1 = Calendar.getInstance();
		    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    window.addMessageForReader(" [METHOD] - Current time: " + df1.format(cal1.getTime()));
		    window.addMessageForReader(" [METHOD] - Dataset Processed and Prepared ... COMPLETED");
			window.updateEastPanelText(2);		
		} catch(Exception e) {
			window.addMessageForReader(" [ERROR] - Error in post processing data : " + e.getMessage());
			window.updateEastPanelText(3);
		}
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public String getModuleName() {
		return "DataPostProcessor";
	}

	public void main(String[] args) {
		Parameters params;
		String path;
		String refSyntenyFile;
		String indexedClusteredFile;
		String querySyntenyFile;
		String output;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"DataPostProcessor is ........... \n\n" +
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar DataPreparator [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}
			
			path = params.path;
			indexedClusteredFile = params.files.get(0);
			querySyntenyFile = params.files.get(1);
			refSyntenyFile = params.refsynteny;
			output = params.output + ".bcl";
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			new PostProcessData(path, refSyntenyFile, querySyntenyFile, indexedClusteredFile, output);
		} catch (Exception e) {
			System.out.println("Data Preparator Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		String path;
		String refSyntenyFile;
		String indexedClusteredFile;
		String querySyntenyFile;
		String output;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);
			
			path = params.path;
			indexedClusteredFile = params.files.get(0);
			querySyntenyFile = params.files.get(1);
			refSyntenyFile = params.refsynteny;
			output = params.output + ".bcl";
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			new PostProcessData(window, path, refSyntenyFile, querySyntenyFile, indexedClusteredFile, output);
			return true;
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Data Preparator Failed : " + e.getMessage());
			window.updateEastPanelText(3);		
			return false;
		}
	}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
