package modules.evaluator;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

public class Parameters {
	/** Required parameters: Application/Module to run */
	@Parameter(description = "<NC and SNC file>")
	public List<String> files = new ArrayList<String>();
	
	@Parameter(names = {"-h", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Path from where to get file and output the combined file. */
	@Parameter(names = {"-p","--path"}, description = "Path for finding the data and outputting results.")
	public String path = "./";
	
	/** a (x-weight) to be used in the ellipse equation [{(x^2)/(a^2)} + {(y^2)/(b^2)} = 1]  */
	@Parameter(names = {"-a","--avertex"}, description = "Value of a-vertex to be used in the standard ellipse equation with center at origin.")
	public String avertex = "0.5";
	
	/** b (y-weight) to be used in the ellipse equation [{(x^2)/(a^2)} + {(y^2)/(b^2)} = 1]  */
	@Parameter(names = {"-b","--bvertex"}, description = "Value of co-vertex to be used in the standard ellipse equation with center at origin.")
	public String covertex = "1.0";
	
	@Parameter(names = {"-o", "--output"}, description = "Output file name with .eva")
	public String output = "genFam";
}
