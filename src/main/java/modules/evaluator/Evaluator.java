package modules.evaluator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.SequenceFileWriter;
import miscellaneous.gui.GFCWindow;
import modules.GenFamModules;

/**
 * @author Raja Hashim Ali
 **/
public class Evaluator implements GenFamModules {
	public Evaluator () {
	}

	public String getModuleName() {
		return "Evaluator";
	}

	public void main(String[] args) throws Exception {
		Parameters params;
		Calendar cal;
		SimpleDateFormat df;
		String path;
		String ncAndSncFile;
		String output;
		float avertex;
		float covertex;
		float nc;
		float snc;
		float value;
		float maximum = 0;
		
		BufferedReader br;
		String str;
		String[] strArr;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"Evaluator is an implementation of user defined function that allows to merge NC and SNC\n" +
						"scores into a single Strength of prediction score based on a user defined fuction and then\n" +
						"normalizes the rest of scores into between 0.5 and 1.0." + 
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar ClusteringModule [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}
			cal = Calendar.getInstance();
			df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df.format(cal.getTime()) + '\n');
			
			path = params.path;
			ncAndSncFile = params.files.get(0);
			output = params.output;
			avertex = Float.parseFloat(params.avertex);
			covertex = Float.parseFloat(params.covertex);
						
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			System.out.println("Evaluating " + path + ncAndSncFile + ". Please wait.");
			br = new BufferedReader(new FileReader(path + ncAndSncFile));
			
			while ((str = br.readLine()) != null ) {
				strArr = str.split("\t");
				nc = Float.parseFloat(strArr[2]);
				snc = Float.parseFloat(strArr[3]);
				value = (covertex * covertex) * (nc * nc) + (avertex * avertex) * (snc * snc) - (covertex * covertex * avertex * avertex);
				if(value > maximum)
					maximum = value;
			}
			br.close();
			
			System.out.println("Transforming and normalizing " + path + ncAndSncFile + ". Please wait.");
			
			br = new BufferedReader(new FileReader(path + ncAndSncFile));			
			while ((str = br.readLine()) != null ) {
				strArr = str.split("\t");
				nc = Float.parseFloat(strArr[2]);
				snc = Float.parseFloat(strArr[3]);
				value = (covertex * covertex) * (nc * nc) + (avertex * avertex) * (snc * snc) - (covertex * covertex * avertex * avertex);					
				if(value > 0) {
					value = value/maximum;
					value = (float) ((float)(value/(float)2) + (float)0.5);
					SequenceFileWriter.writeAndAppendLine(path, output + ".eva", strArr[0] + "\t" + strArr[1] + "\t" + value);
				}
			}
			br.close();
			
			System.out.println("Evaluation Completed.");
			
			cal = Calendar.getInstance();
			df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("\n# Current time: " + df.format(cal.getTime()));
		} catch (Exception e) {
			System.out.println("Evaluator Failed : " + e.getMessage());
			System.exit(-1);
		}
	}

	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		Calendar cal;
		SimpleDateFormat df;
		String path;
		String ncAndSncFile;
		String output;
		float avertex;
		float covertex;
		float nc;
		float snc;
		float value;
		float maximum = 0;
		
		BufferedReader br;
		String str;
		String[] strArr;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);

			cal = Calendar.getInstance();
			df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			window.addMessageForReader(" [STATUS] - Current time: " + df.format(cal.getTime()));
			window.updateEastPanelText(2);
			
			path = params.path;
			ncAndSncFile = params.files.get(0);
			output = params.output;
			avertex = Float.parseFloat(params.avertex);
			covertex = Float.parseFloat(params.covertex);
						
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			window.addMessageForReader(" [STATUS] - Evaluating " + path + ncAndSncFile + ". Please wait.");
			window.updateEastPanelText(2);
			br = new BufferedReader(new FileReader(path + ncAndSncFile));
			
			while ((str = br.readLine()) != null ) {
				strArr = str.split("\t");
				nc = Float.parseFloat(strArr[2]);
				snc = Float.parseFloat(strArr[3]);
				value = (covertex * covertex) * (nc * nc) + (avertex * avertex) * (snc * snc) - (covertex * covertex * avertex * avertex);
				if(value > maximum)
					maximum = value;
			}
			br.close();
			
			window.addMessageForReader(" [STATUS] - Transforming and normalizing " + path + ncAndSncFile + ". Please wait.");
			window.updateEastPanelText(2);
			
			br = new BufferedReader(new FileReader(path + ncAndSncFile));
			int count = 0;
			while ((str = br.readLine()) != null ) {
				count++;
				strArr = str.split("\t");
				nc = Float.parseFloat(strArr[2]);
				snc = Float.parseFloat(strArr[3]);
				value = (covertex * covertex) * (nc * nc) + (avertex * avertex) * (snc * snc) - (covertex * covertex * avertex * avertex);					
				if(value > 0) {
					value = value/maximum;
					value = (float) ((float)(value/(float)2) + (float)0.5);
					SequenceFileWriter.writeAndAppendLine(path, output + ".eva", strArr[0] + "\t" + strArr[1] + "\t" + value);
				}
				if(count%100 == 0) {
					window.addMessageForReader(" [STATUS] - Post-processed " + count + " data points ... Please wait.");
					window.updateEastPanelText(2);
				}
			}
			br.close();
			
			cal = Calendar.getInstance();
			df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			window.addMessageForReader(" [STATUS] - Current time: " + df.format(cal.getTime()));
			window.addMessageForReader(" [STATUS] - Evaluation Completed.");
			window.updateEastPanelText(2);
			return true;
		} catch (Exception e) {
			window.addMessageForReader(" [ERROR] - Evaluator Failed : " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}
}
