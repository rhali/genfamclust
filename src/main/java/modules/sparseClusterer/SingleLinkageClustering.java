package modules.sparseClusterer;

import java.util.ArrayList;

import miscellaneous.Cluster;
import miscellaneous.Couple;
import miscellaneous.SequenceFileWriter;
import miscellaneous.gui.GFCWindow;

public class SingleLinkageClustering extends ClusterLinkageAlgorithm {
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public SingleLinkageClustering(String path, String querySyntenyFile, String evaluationFile, String reference) throws Exception {
		super(path, querySyntenyFile, evaluationFile, reference);
	}
	
	public SingleLinkageClustering(GFCWindow window, String path, String querySyntenyFile, String evaluationFile, String reference) throws Exception {
		super(window, path, querySyntenyFile, evaluationFile, reference);
	}

	/*'******************************************************************************
	 * 							CLASS VARIABLES										*
	 ********************************************************************************/
	/** Given the indexes of two clusters (x and y) that have the minimum score, this method will 
	 * form a new cluster (z), add the distances of other neighboring clusters (w) of both x and y 
	 * according to the clustering method (min of score of (x,w) and (y,w) for Single Linkage). For 
	 * any missing edge, a max possible distance of 1 is assumed.
	 * @param1 The index of first cluster.
	 * @param2 The index of second cluster.
	 * @param3 The method with which to combine both clusters. **/
	public void updateCluster(Integer xIndex, Integer yIndex) {
		int index;
		Cluster cluster;
		Float yScore;
		Float minScore;
		Integer xHitIndex;
		Integer yHitIndex;
		boolean single;

		index = clusterList.get(clusterList.size() - 1).getIndex() + 1;
		cluster = new Cluster(index);
		single = true;

		for(int i = 0; i < clusterList.get(xIndex).getMembers().size(); i++) 
			cluster.addMember(clusterList.get(xIndex).getMembers().get(i));
		for(int i = 0; i < clusterList.get(yIndex).getMembers().size(); i++) 
			cluster.addMember(clusterList.get(yIndex).getMembers().get(i));

		for(int i = 0; i < clusterList.get(xIndex).getEvalScore().size(); i++) {
			minScore = clusterList.get(xIndex).getEvalScore().get(i).second;
			xHitIndex = clusterList.get(xIndex).getEvalScore().get(i).first;
			single = true;

			if(!xHitIndex.equals(yIndex)) {
				for (int j = 0; j < clusterList.get(yIndex).getEvalScore().size(); j++) {
					if(xHitIndex.equals(clusterList.get(yIndex).getEvalScore().get(j).first)) {
						yScore = clusterList.get(yIndex).getEvalScore().get(j).second;
						if(Float.compare(yScore, minScore) < 0)
							minScore = yScore;
						clusterList.get(yIndex).removeEvalListMember(j);
						single = false;
						break;
					}
				}

				cluster.addEvalScore(new Couple<Integer, Float>(xHitIndex, minScore));

				if(single == true) {
					for(int j = 0; j < clusterList.get(xHitIndex).getEvalScore().size(); j++) {
						if(clusterList.get(xHitIndex).getEvalScore().get(j).first.equals(xIndex)) {
							clusterList.get(xHitIndex).removeEvalListMember(j);
							clusterList.get(xHitIndex).addEvalScore(new Couple<Integer, Float>(cluster.getIndex(), minScore));
							break;
						}
					}
				} else {
					for(int j = 0; j < clusterList.get(xHitIndex).getEvalScore().size(); j++) {
						if(clusterList.get(xHitIndex).getEvalScore().get(j).first.equals(yIndex)) 
							clusterList.get(xHitIndex).removeEvalListMember(j);
					}
					for(int j = 0; j < clusterList.get(xHitIndex).getEvalScore().size(); j++) {
						if(clusterList.get(xHitIndex).getEvalScore().get(j).first.equals(xIndex)) {
							clusterList.get(xHitIndex).removeEvalListMember(j);
							clusterList.get(xHitIndex).addEvalScore(new Couple<Integer, Float>(cluster.getIndex(), minScore));
						} 
					}
				} 
			}
		}	

		for(int i = 0; i < clusterList.get(yIndex).getEvalScore().size(); i++) {
			yHitIndex = clusterList.get(yIndex).getEvalScore().get(i).first;
			minScore = clusterList.get(yIndex).getEvalScore().get(i).second;
			if(!yHitIndex.equals(xIndex)) {
				cluster.addEvalScore(clusterList.get(yIndex).getEvalScore().get(i));
				for(int j = 0; j < clusterList.get(yHitIndex).getEvalScore().size(); j++) {
					if(clusterList.get(yHitIndex).getEvalScore().get(j).first.equals(yIndex)) {
						clusterList.get(yHitIndex).removeEvalListMember(j);
						clusterList.get(yHitIndex).addEvalScore(new Couple<Integer, Float>(cluster.getIndex(), minScore));
						break;
					}
				}
			}
		}

		clusterList.get(xIndex).removeClusterWithTrace();
		clusterList.get(yIndex).removeClusterWithTrace();
		clusterList.add(cluster);
	}

	/** After the clusters have been identified, each edge that belongs to members of the cluster 
	 * are added to the graph. Depending on the method of clustering, the edges are written out
	 * in the GEXF format in the output file. **/
	public int addEdgesforCluster(String path, String output, ArrayList<String> members, float threshold, int edgeCount) throws Exception {
		String edgeLabel;
		String node1;
		String node2;
		ArrayList<Integer> usedIndex;
		
		SequenceFileWriter.writeAndAppendLine(path, output + ".log", " Cluster Size = " + members.size());
		for(int i = 0; i < members.size(); i++) 
			SequenceFileWriter.writeAndAppendLine(path, output + ".log", "\tMember " + i + "= " + members.get(i));

		usedIndex = new ArrayList<Integer>();
		for(int i = 0; i < evalGene1.size(); i++) {
			if(((!evalGene1.get(i).equals(evalGene2.get(i))) && (1 - Float.parseFloat(evalScore.get(i))) <= threshold)) {
				for(int j = 0; j < members.size(); j++) {
					if(evalGene1.get(i).equals(members.get(j))) {
						edgeLabel = evalScore.get(i);
						node1 = evalGene1.get(i);
						node2 = evalGene2.get(i);

						SequenceFileWriter.writeAndAppendLine(path, output + ".log", "\t\t" + node1 + "\t" + node2 + "\t" + edgeLabel);
						SequenceFileWriter.writeAndAppendLine(path, output + ".gexf", "\t\t\t<edge id=\"" + edgeCount + "\" label=\"" + edgeLabel + "\" source=\"" + node1 + "\" target=\"" + node2 + "\" start=\"2.0\" end=\"8.0\">" 
								+ "\n\t\t\t\t<attvalues>" 
								+ "\n\t\t\t\t\t<attvalue for=\"weight\" value=\"1\" start=\"2.0\" end=\"4.0\"></attvalue>" 
								+ "\n\t\t\t\t\t<attvalue for=\"weight\" value=\"3\" start=\"4.0\" end=\"8.0\"></attvalue>" 
								+ "\n\t\t\t\t</attvalues>" 
								+ "\n\t\t\t</edge>");
						usedIndex.add(i);
						edgeCount++;
						break;
					}
				} 
			} else {
				usedIndex.add(i);
			} 
		}

		for(int i = 0; i < usedIndex.size(); i++) {
			evalGene1.remove(usedIndex.get(i) - i);
			evalGene2.remove(usedIndex.get(i) - i); 
			evalScore.remove(usedIndex.get(i) - i);
		}

		SequenceFileWriter.writeAndAppendLine(path, output + ".gexf", "\t\t</edges>" + "\n\t</graph>");
		return edgeCount;
	}
}
