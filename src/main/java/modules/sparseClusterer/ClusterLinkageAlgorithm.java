package modules.sparseClusterer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import miscellaneous.Cluster;
import miscellaneous.ClusterLinkageInterface;
import miscellaneous.Couple;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import miscellaneous.Species;
import miscellaneous.Triple;
import miscellaneous.gui.GFCWindow;

public abstract class ClusterLinkageAlgorithm implements ClusterLinkageInterface {
	/*'******************************************************************************
	 * 							CLASS VARIABLES										*
	 ********************************************************************************/
	protected ArrayList<Cluster> clusterList;
	protected ArrayList<String> evalGene1;
	protected ArrayList<String> evalGene2;
	protected ArrayList<String> evalScore;

	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	/** Actual Constructor. Makes each individual gene a standalone cluster using reference synteny
	 * and query synteny files and then loads data into each cluster from the evaluation file 
	 * to initialize clusters.**/
	public ClusterLinkageAlgorithm(String path, String querySyntenyFile, String evaluationFile, String reference) throws Exception {
		String str;
		String[] strArr;
		SimpleFileIO reader;
		Integer count;
		Integer counter;
		Integer refSpecies;
		Integer refGenes;
		BufferedReader bufferedreader;
		ArrayList<Species> speciesList;
		Calendar cal;
		SimpleDateFormat df;

		counter = 0;
		refGenes = 0;
		reader = new SimpleFileIO();
		
		System.out.print("Reading synteny file and adding clusters ... ");
		if(!reference.equals("")) {
			reader.readSyntenyFile(path, reference);
			refSpecies = reader.getNumberofSpecies();
			refGenes = reader.getNumberOfGenes();
		} else 
			refSpecies = 0;
		reader.readSyntenyFile(path, querySyntenyFile);
		speciesList = reader.getSpeciesList();
		count = reader.getNumberOfGenes();
		clusterList = new ArrayList<Cluster>();

		for(int i = refGenes; i < count; i++) {
			Cluster cluster = new Cluster(i - refGenes);
			clusterList.add(cluster);
		}

		for(Integer i = refSpecies; i < speciesList.size(); i++) 
			for(Integer j = 0; j < speciesList.get(i).getChromosomes().size(); j++) 
				for(Integer k = 0; k < speciesList.get(i).getChromosomes().get(j).getGeneList().size(); k++) {
					String member = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getIndex();
					clusterList.get(counter).addMember(member);
					counter++;
				}

		System.out.println("Done");

		cal = Calendar.getInstance();
		df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df.format(cal.getTime()) + '\n');

		System.out.println("Adding entries from Homology file");
		
		bufferedreader = new BufferedReader(new FileReader(path + evaluationFile));
		String previous = "";
		int index = -1;
		counter = 0;
		try {
			while ((str = bufferedreader.readLine()) != null) {
				strArr = str.split("\t");
				counter++;
				if(!strArr[0].equals(strArr[1])) {
					if(!previous.equals(strArr[0])) {
						for(int i = 0; i < clusterList.size(); i++) {
							if(strArr[0].equals(clusterList.get(i).getMembers().get(0))) {
								for(int j = 0; j < clusterList.size(); j++) {
									if(strArr[1].equals(clusterList.get(j).getMembers().get(0))) {
										Float score = 1 - Float.parseFloat(strArr[2]); 
										Couple<Integer, Float> ncHit = new Couple<Integer, Float>(j, score);
										clusterList.get(i).addEvalScore(ncHit);
										break;
									}
								}
								previous = strArr[0];
								index = i;
								break;
							}
						}
					} else {
						for(int j = 0; j < clusterList.size(); j++) {
							if(strArr[1].equals(clusterList.get(j).getMembers().get(0))) {
								Float score = 1 - Float.parseFloat(strArr[2]); 
								Couple<Integer, Float> ncHit = new Couple<Integer, Float>(j, score);
								clusterList.get(index).addEvalScore(ncHit);
								break;
							}
						}
					}
				}

				if(counter % 50000 == 0) 
					System.out.println(counter + " Entries added");
			}
		} finally {
			bufferedreader.close();
		}

		System.out.println("Cluster Evaluation added");

		cal = Calendar.getInstance();
		df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df.format(cal.getTime()) + '\n');
	}
	
	/** Actual Constructor. Makes each individual gene a standalone cluster using reference synteny
	 * and query synteny files and then loads data into each cluster from the evaluation file 
	 * to initialize clusters.**/
	public ClusterLinkageAlgorithm(GFCWindow window, String path, String querySyntenyFile, String evaluationFile, String reference) throws Exception {
		String str;
		String[] strArr;
		SimpleFileIO reader;
		Integer count;
		Integer counter;
		Integer refSpecies;
		Integer refGenes;
		BufferedReader bufferedreader;
		ArrayList<Species> speciesList;
		Calendar cal;
		SimpleDateFormat df;

		counter = 0;
		refGenes = 0;
		reader = new SimpleFileIO();
		
		window.addMessageForReader("[METHOD] - Reading synteny file and adding clusters ... ");
		window.updateEastPanelText(2);
		
		if(!reference.equals("")) {
			reader.readSyntenyFile(path, reference);
			refSpecies = reader.getNumberofSpecies();
			refGenes = reader.getNumberOfGenes();
		} else 
			refSpecies = 0;
		reader.readSyntenyFile(path, querySyntenyFile);
		speciesList = reader.getSpeciesList();
		count = reader.getNumberOfGenes();
		clusterList = new ArrayList<Cluster>();

		for(int i = refGenes; i < count; i++) {
			Cluster cluster = new Cluster(i - refGenes);
			clusterList.add(cluster);
		}

		for(Integer i = refSpecies; i < speciesList.size(); i++) 
			for(Integer j = 0; j < speciesList.get(i).getChromosomes().size(); j++) 
				for(Integer k = 0; k < speciesList.get(i).getChromosomes().get(j).getGeneList().size(); k++) {
					String member = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getIndex();
					clusterList.get(counter).addMember(member);
					counter++;
				}

		window.addMessageForReader("[METHOD] - Done");
		window.updateEastPanelText(2);
		
		cal = Calendar.getInstance();
		df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		window.addMessageForReader("[METHOD] - Current time: " + df.format(cal.getTime()) + '\n');
		window.addMessageForReader("[METHOD] - Adding entries from Homology file");
		window.updateEastPanelText(2);
				
		bufferedreader = new BufferedReader(new FileReader(path + evaluationFile));
		String previous = "";
		int index = -1;
		counter = 0;
		try {
			while ((str = bufferedreader.readLine()) != null) {
				strArr = str.split("\t");
				if(strArr.length == 1)
					strArr = str.split("\\s");
				counter++;
				if(!strArr[0].equals(strArr[1])) {
					if(!previous.equals(strArr[0])) {
						for(int i = 0; i < clusterList.size(); i++) {
							if(strArr[0].equals(clusterList.get(i).getMembers().get(0))) {
								for(int j = 0; j < clusterList.size(); j++) {
									if(strArr[1].equals(clusterList.get(j).getMembers().get(0))) {
										Float score = 1 - Float.parseFloat(strArr[2]); 
										Couple<Integer, Float> ncHit = new Couple<Integer, Float>(j, score);
										clusterList.get(i).addEvalScore(ncHit);
										break;
									}
								}
								previous = strArr[0];
								index = i;
								break;
							}
						}
					} else {
						for(int j = 0; j < clusterList.size(); j++) {
							if(strArr[1].equals(clusterList.get(j).getMembers().get(0))) {
								Float score = 1 - Float.parseFloat(strArr[2]); 
								Couple<Integer, Float> ncHit = new Couple<Integer, Float>(j, score);
								clusterList.get(index).addEvalScore(ncHit);
								break;
							}
						}
					}
				}

				if(counter % 50000 == 0) {
					window.addMessageForReader("[METHOD] - " + counter + " Entries added.");
					window.updateEastPanelText(2);
				}
			}
		} finally {
			bufferedreader.close();
		}

		cal = Calendar.getInstance();
		df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		window.addMessageForReader("[METHOD] - Finished initializing clusters.");
		window.addMessageForReader("[METHOD] - Current time: " + df.format(cal.getTime()) + '\n');
		window.updateEastPanelText(2);
	}

	/** Recursive call for each Linkage Clustering method. Performs Linkage Clustering using 
	 * updateCluster method and recursively calling itself until the minimum threshold is reached. **/
	public void performLinkage(Float threshold, Integer run) {
		if(run%100==0)
			System.out.println("Clustering Run: = " + run);
		Triple<Integer, Integer, Float> getMinimum = getMinimum();
		Float minimum = getMinimum.third;
		Integer xIndex = getMinimum.first;
		Integer yIndex = getMinimum.second;

		if(minimum < threshold) {
			updateCluster(xIndex, yIndex);			
			performLinkage(threshold, run + 1);
		} else {
			System.out.println("The minimum value in graph is " + minimum + " and threshold is " + threshold);
			return;
		}
	}
	
	/** Recursive call for each Linkage Clustering method. Performs Linkage Clustering using 
	 * updateCluster method and recursively calling itself until the minimum threshold is reached. **/
	public void performLinkage(GFCWindow window, Float threshold, Integer run) {
		if(run%100==0) {
			window.addMessageForReader("[METHOD] - Clustering Run: = " + run);
			window.updateEastPanelText(2);
		}
		Triple<Integer, Integer, Float> getMinimum = getMinimum();
		Float minimum = getMinimum.third;
		Integer xIndex = getMinimum.first;
		Integer yIndex = getMinimum.second;

		if(minimum < threshold) {
			updateCluster(xIndex, yIndex);			
			performLinkage(window, threshold, run + 1);
		} else {
			window.addMessageForReader("[METHOD] - The minimum value in graph is " + minimum + " and threshold is " + threshold);
			window.updateEastPanelText(2);
			return;
		}
	}


	/** Get the minimum distance among any two clusters that are present in the list 
	 * of clusters.
	 * @returns indexes of both clusters and score between them. **/
	public Triple<Integer, Integer, Float> getMinimum() {
		Float minimum = (float)1;
		Integer xIndex = 0;
		Integer yIndex = 0;
		for(int i = 0; i < clusterList.size(); i++) {
			for(int j = 0; j < clusterList.get(i).getEvalScore().size(); j++) {
				if(clusterList.get(i).getEvalScore().get(j).second < minimum) {
					minimum = clusterList.get(i).getEvalScore().get(j).second;
					xIndex = clusterList.get(i).getIndex();
					yIndex = clusterList.get(i).getEvalScore().get(j).first;
				}
			}
		}
		return new Triple<Integer, Integer, Float>(xIndex, yIndex, minimum);
	}

	/** Calls individual clustering methods and responsible for writing out the final cluster 
	 * information. The ouptput is in GEXF format and can be opened conveniently in gephi.**/
	public void method(String path, String refSyntenyFile, String querySyntenyFile, String evaluationFile, String output, Float threshold, boolean graphics) throws Exception {
		String line;
		SimpleFileIO reader;
		ArrayList<Cluster> clusters;
		BufferedReader bufferedreader;
		String[] strArr1;

		performLinkage(threshold, 0);

		reader = new SimpleFileIO();
		if(!refSyntenyFile.equals(""))
			reader.readSyntenyFile(path, refSyntenyFile);
		reader.readSyntenyFile(path, querySyntenyFile);

		clusters = new ArrayList<Cluster>();
		for(int i = 0; i < clusterList.size(); i++) {
			if(clusterList.get(i).getMembers().size() > 0)
				clusters.add(clusterList.get(i));
		}

		bufferedreader = new BufferedReader(new FileReader(path + evaluationFile));
		evalGene1 = new ArrayList<String>();
		evalGene2 = new ArrayList<String>();
		evalScore = new ArrayList<String>();

		try {
			while ((line = bufferedreader.readLine()) != null) {
				strArr1 = line.split("\t");
				if(strArr1.length == 1)
					strArr1 = line.split("\\s");
				evalGene1.add(strArr1[0]);
				evalGene2.add(strArr1[1]);
				evalScore.add(strArr1[2]);
			}
		} finally {
			try {
				bufferedreader.close();					
			} catch (Exception e) {
				System.out.println("Closing reader failed: " + e.getMessage());
				System.exit(-5);
			}
		}

		for(int i = 0; i < clusterList.size(); i++) {
			if(clusterList.get(i).getMembers().size()>0)
				SequenceFileWriter.writeAndAppendString(path, output + ".bcl", clusterList.get(i).getMembers().get(0));
			for(int j = 1; j < clusterList.get(i).getMembers().size(); j++)
				SequenceFileWriter.writeAndAppendString(path, output + ".bcl", "\t" + clusterList.get(i).getMembers().get(j));
			if(clusterList.get(i).getMembers().size()>0)
				SequenceFileWriter.writeAndAppendLine(path, output + ".bcl", "");
		}
	}
	
	/** Calls individual clustering methods and responsible for writing out the final cluster 
	 * information. The ouptput is in GEXF format and can be opened conveniently in gephi.**/
	public void method(GFCWindow window, String path, String refSyntenyFile, String querySyntenyFile, String evaluationFile, String output, Float threshold, boolean graphics) throws Exception {
		String line;
		SimpleFileIO reader;
		ArrayList<Cluster> clusters;
		BufferedReader bufferedreader;
		String[] strArr1;

		performLinkage(window, threshold, 0);

		reader = new SimpleFileIO();
		if(!refSyntenyFile.equals(""))
			reader.readSyntenyFile(path, refSyntenyFile);
		reader.readSyntenyFile(path, querySyntenyFile);

		clusters = new ArrayList<Cluster>();
		for(int i = 0; i < clusterList.size(); i++) {
			if(clusterList.get(i).getMembers().size() > 0)
				clusters.add(clusterList.get(i));
		}

		bufferedreader = new BufferedReader(new FileReader(path + evaluationFile));
		evalGene1 = new ArrayList<String>();
		evalGene2 = new ArrayList<String>();
		evalScore = new ArrayList<String>();

		try {
			while ((line = bufferedreader.readLine()) != null) {
				strArr1 = line.split("\t");
				if(strArr1.length == 1)
					strArr1 = line.split("\\s");
				evalGene1.add(strArr1[0]);
				evalGene2.add(strArr1[1]);
				evalScore.add(strArr1[2]);
			}
		} finally {
			try {
				bufferedreader.close();					
			} catch (Exception e) {
				window.addMessageForReader("[ERROR] - Closing reader failed: " + e.getMessage());
				window.updateEastPanelText(3);
			}
		}

		for(int i = 0; i < clusterList.size(); i++) {
			if(clusterList.get(i).getMembers().size()>0)
				SequenceFileWriter.writeAndAppendString(path, output + ".bcl", clusterList.get(i).getMembers().get(0));
			for(int j = 1; j < clusterList.get(i).getMembers().size(); j++)
				SequenceFileWriter.writeAndAppendString(path, output + ".bcl", "\t" + clusterList.get(i).getMembers().get(j));
			if(clusterList.get(i).getMembers().size()>0)
				SequenceFileWriter.writeAndAppendLine(path, output + ".bcl", "");
		}
	}

	/** Prints the members of the cluster. Only used for diagnostic purposes. **/
	public void printCluster(String path, Integer xIndex, Integer yIndex) {
		try {
			SequenceFileWriter.writeAndAppendLine(path, "ClusterResult.log", "Combining = " + xIndex + " and " + yIndex);
			for(int i = 0; i < clusterList.size(); i++) {
				SequenceFileWriter.writeAndAppendLine(path, "ClusterResult.log", "Cluster = " + clusterList.get(i).getIndex());

				for (int j = 0; j < clusterList.get(i).getMembers().size(); j++) 
					SequenceFileWriter.writeAndAppendLine(path, "ClusterResult.log", "\t" + clusterList.get(i).getMembers().get(j));

				for(int j = 0; j < clusterList.get(i).getEvalScore().size(); j++) 
					SequenceFileWriter.writeAndAppendLine(path, "ClusterResult.log", "\t\t" + clusterList.get(i).getEvalScore().get(j).first + "\t" + clusterList.get(i).getEvalScore().get(j).second);
			}
		} catch (Exception e) {
			System.out.println("Error in printing cluster: " + e.getMessage());
			System.exit(-1);
		}
	}
}