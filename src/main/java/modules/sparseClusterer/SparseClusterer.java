package modules.sparseClusterer;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.gui.GFCWindow;
import modules.GenFamModules;

/**
 * The class that contains the methods for clustering data into gene families after the data 
 * been evaluated by the modelEvaluation module. It performs the average linkage clustering,
 * aka UPGMA method, single linkage clustering aka nearest neighbor and complete linkage 
 * clustering aka farthest neighbor.
 * @author Raja Hashim Ali
 **/
public class SparseClusterer implements GenFamModules{
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	/** Default Constructor. Do not use for other than Module call. **/
	public SparseClusterer() {
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	/** Returns the name of the module displayed in the main modules program. **/
	public String getModuleName() {
		return "SparseClusterer";
	}

	public void main(String[] args) throws Exception {
		Parameters params;
		Calendar cal;
		SimpleDateFormat df;
		String path;
		String querySyntenyFile;
		String evaluationFile;
		String method;
		String referenceSyntenyFile;
		String output;
		float threshold;
		boolean graphics;

		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"SparseClusteringModule is an implementation of a custom version of Single Linkage clustering,\n" +
						"Average Linkage Clustering and Complete Linkage clustering. \n " + 
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar ClusteringModule [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}
			cal = Calendar.getInstance();
			df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df.format(cal.getTime()) + '\n');

			path = params.path;
			method = params.method;
			querySyntenyFile = params.files.get(0);
			evaluationFile = params.files.get(1);
			referenceSyntenyFile = params.reference;
			output = params.output;
			threshold = Float.parseFloat(params.threshold);
			graphics = params.graphics;

			if(!path.endsWith("/"))
				path = path.concat("/");
			
			if(method.equals("s")) {
				SingleLinkageClustering slc = new SingleLinkageClustering(path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
				System.out.println("Single Linkage Clustering");
				slc.method(path, referenceSyntenyFile, querySyntenyFile, evaluationFile, output, threshold, graphics);
			} else if(method.equals("c")) {
				CompleteLinkageClustering clc = new CompleteLinkageClustering(path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
				System.out.println("Complete Linkage Clustering");
				clc.method(path, referenceSyntenyFile, querySyntenyFile, evaluationFile, output, threshold, graphics);
			} else if (method.equals("a")){
				AverageLinkageClustering alc = new AverageLinkageClustering(path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
				System.out.println("Average Linkage Clustering");
				alc.method(path, referenceSyntenyFile, querySyntenyFile, evaluationFile, output, threshold, graphics);
			} else {
				System.out.println("Invalid option. Please input \"s\", \"c\" or \"a\" as Clustering method.");
				System.exit(-5);
			}
		} catch (Exception e) {
			System.out.println("Clustering Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		Calendar cal;
		SimpleDateFormat df;
		String path;
		String querySyntenyFile;
		String evaluationFile;
		String method;
		String referenceSyntenyFile;
		String output;
		float threshold;
		boolean graphics;

		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);

			cal = Calendar.getInstance();
			df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			window.addMessageForReader("[METHOD] - Current time: " + df.format(cal.getTime()) + '\n');
			window.updateEastPanelText(2);

			path = params.path;
			method = params.method;
			querySyntenyFile = params.files.get(0);
			evaluationFile = params.files.get(1);
			referenceSyntenyFile = params.reference;
			output = params.output;
			threshold = Float.parseFloat(params.threshold);
			graphics = params.graphics;

			if(!path.endsWith("/"))
				path = path.concat("/");
			
			if(method.equals("s")) {
				window.addMessageForReader("[METHOD] - Performing Single Linkage Clustering");
				window.updateEastPanelText(2);
				SingleLinkageClustering slc = new SingleLinkageClustering(window, path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
				slc.method(window, path, referenceSyntenyFile, querySyntenyFile, evaluationFile, output, threshold, graphics);
			} else if(method.equals("c")) {
				window.addMessageForReader("[METHOD] - Performing Complete Linkage Clustering");
				window.updateEastPanelText(2);
				CompleteLinkageClustering clc = new CompleteLinkageClustering(window, path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
				clc.method(window, path, referenceSyntenyFile, querySyntenyFile, evaluationFile, output, threshold, graphics);
			} else if (method.equals("a")){
				window.addMessageForReader("[METHOD] - Performing Average Linkage Clustering");
				window.updateEastPanelText(2);
				AverageLinkageClustering alc = new AverageLinkageClustering(window, path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
				alc.method(window, path, referenceSyntenyFile, querySyntenyFile, evaluationFile, output, threshold, graphics);
			}
			window.addMessageForReader("[METHOD] - Successfully completed clustering.");
			window.updateEastPanelText(2);
			return true;
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Clustering Failed : " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}
}