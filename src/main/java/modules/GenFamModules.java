package modules;

/**
 * Interface for GenFamClust modules. All the moduels must adhere to this
 * interface to be aceessable to starter application.
 * @author Raja Hashim Ali
 *
 */
public interface GenFamModules {
	/**
	 * Returns the module's name.
	 * @return name of module.
	 */
	public String getModuleName();
	
	/**
	 * Starts the application.
	 * @param args the application arguments.
	 */
	public void main(String[] args) throws Exception;
}
