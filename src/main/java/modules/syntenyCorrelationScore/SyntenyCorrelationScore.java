package modules.syntenyCorrelationScore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

//import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;





import miscellaneous.Chromosome;
import miscellaneous.Correlation;
import miscellaneous.Couple;
import miscellaneous.Gene;
import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import miscellaneous.Species;
import miscellaneous.gui.GFCWindow;
import modules.GenFamModules;
/**
 * 
 * @author Raja Hashim Ali
 *
 */
public class SyntenyCorrelationScore implements GenFamModules {
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public SyntenyCorrelationScore() {
	}

	public SyntenyCorrelationScore(String path, String refSyntenyFile, String querySyntenyFile, String ncAndSNSfile, String output) throws Exception {
		int testSpecies;
		int refSpecies;

		Gene gene;
		Gene ncGene;

		Float syntenyCorr = (float)0;
		Float syntenyScore;
		Float ncSyntenyScore;

		Float[] geneArr;
		Float[] ncArray;

		String gene1;
		String ncHit;
		String ncHit1;
		String ncNCHit;

		Calendar cal;
		Calendar cal1;
		Calendar cal2;

		Species species;

		Chromosome chromosome;

		SimpleFileIO reader;

		SimpleDateFormat df;

		ArrayList<Float> ncSyntenyList;
		ArrayList<Float> geneSyntenyList;
		ArrayList<Float> ncSyntenyScoreList;
		ArrayList<Float> geneCommonSyntenyScoreList;

		ArrayList<Species> speciesList;

		ArrayList<Couple<String, Float>> ncNCList; 
		ArrayList<Couple<String, Float>> geneNCHitList;
		ArrayList<Couple<String, Float>> geneSyntenyScoreList;

		refSpecies = 0;
		reader = new SimpleFileIO();

		if(!refSyntenyFile.equals("")) {
			System.out.print("Reading Reference Synteny File ... ");
			reader.readSyntenyFile(path, refSyntenyFile);
			System.out.println("COMPLETED\n");
			refSpecies = reader.getNumberofSpecies();
		}

		cal = Calendar.getInstance();
		df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df.format(cal.getTime()));

		System.out.print("Reading Query Synteny File ... ");
		reader.readSyntenyFile(path, querySyntenyFile);
		System.out.println("COMPLETED\n");
		testSpecies = reader.getNumberofSpecies();

		speciesList = reader.getSpeciesList();

		cal1 = Calendar.getInstance();
		System.out.println("# Current time: " + df.format(cal1.getTime()));

		System.out.print("Reading Neighborhood Corr and Syntenic Scores File ... ");
		speciesList = reader.readNCandSyntenyFile(path, ncAndSNSfile, speciesList);
		System.out.println("COMPLETED");

		cal2 = Calendar.getInstance();
		System.out.println("# Current time: " + df.format(cal2.getTime()));

		System.out.print("Computing Syntenic Correlation Scores ... ");
		for(int i = refSpecies; i < testSpecies; i++) {
			species = speciesList.get(i);
			for(int j = 0; j < species.getChromosomes().size(); j++) {
				chromosome = species.getChromosomes().get(j);
				for(int k = 0; k < chromosome.getNumberOfGenes(); k++) {
					gene = chromosome.getGeneList().get(k);
					geneNCHitList = gene.getGenencScore();
					geneSyntenyList = gene.getSyntenyScore();
					geneSyntenyScoreList = new ArrayList<Couple<String, Float>>();
					for(int l = 0; l < gene.getGenencScore().size(); l++) {
						ncHit = geneNCHitList.get(l).first;
						syntenyScore = geneSyntenyList.get(l);
						geneSyntenyScoreList.add(new Couple<String, Float>(ncHit, syntenyScore));
					}

					for(int l = 0; l < gene.getGenencScore().size(); l++) {
						ncHit1 = geneNCHitList.get(l).first;
						String[] ncArr = ncHit1.split("\\.");
						if(Integer.parseInt(ncArr[0]) >= refSpecies) {
							ncGene = speciesList.get(Integer.parseInt(ncArr[0])).getChromosomes().get(Integer.parseInt(ncArr[1])).getGeneList().get(Integer.parseInt(ncArr[2]));
							ncNCList = ncGene.getGenencScore(); 
							ncSyntenyList = ncGene.getSyntenyScore();
							ncSyntenyScoreList = new ArrayList<Float>();
							geneCommonSyntenyScoreList = new ArrayList<Float>();
							for(int m = 0; m < ncGene.getGenencScore().size(); m++) {
								ncNCHit = ncNCList.get(m).first;
								for(int n = 0; n < geneSyntenyScoreList.size(); n++) {
									if(ncNCHit.equals(geneSyntenyScoreList.get(n).first)) {
										ncSyntenyScore = ncSyntenyList.get(m);
										ncSyntenyScoreList.add(ncSyntenyScore);
										geneCommonSyntenyScoreList.add(geneSyntenyScoreList.get(n).second);
										break;
									}
								}
							}
							ncArray = ncSyntenyScoreList.toArray(new Float[ncSyntenyScoreList.size()]);
							gene1 = speciesList.get(Integer.parseInt(ncArr[0])).getChromosomes().get(Integer.parseInt(ncArr[1])).getGeneList().get(Integer.parseInt(ncArr[2])).getGeneID();
							if(geneCommonSyntenyScoreList.size() > 0) {
								geneArr = geneCommonSyntenyScoreList.toArray(new Float[geneCommonSyntenyScoreList.size()]);
								
//								syntenyCorr = Correlation.getPearsonCorrelation(geneArr, ncArray);
								syntenyCorr = (float)Correlation.computeCorrelation(geneArr, ncArray);
								//							double correlation = new PearsonsCorrelation().correlation(geneArr, ncArray);
								if(syntenyCorr < 0 || Float.isNaN(syntenyCorr))
									syntenyCorr = (float)0;
								if(syntenyCorr > 1)
									syntenyCorr = (float)1;
								if(!gene.getGeneID().equals(gene1)) 
									SequenceFileWriter.writeAndAppendLine(path, output + ".syc", gene.getIndex() + "\t" + ncHit1 + "\t" + geneNCHitList.get(l).second + "\t" + syntenyCorr);
							} else if(!gene.getGeneID().equals(gene1)) 
								SequenceFileWriter.writeAndAppendLine(path, output + ".syc", gene.getIndex() + "\t" + ncHit1 + "\t" + geneNCHitList.get(l).second + "\t" + 0.0);								
						}
					}
				}
			}
		}
		System.out.println("COMPLETED\n");
	}
	
	private boolean computeSyntenyCorrScore(GFCWindow window, String path, String refSyntenyFile, String querySyntenyFile, String ncAndSNSfile, String output) {
		int testSpecies;
		int refSpecies;

		Gene gene;
		Gene ncGene;

		Float syntenyCorr = (float)0;
		Float syntenyScore;
		Float ncSyntenyScore;

		Float[] geneArr;
		Float[] ncArray;

		String gene1;
		String ncHit;
		String ncHit1;
		String ncNCHit;

		Species species;

		Chromosome chromosome;

		SimpleFileIO reader;

		ArrayList<Float> ncSyntenyList;
		ArrayList<Float> geneSyntenyList;
		ArrayList<Float> ncSyntenyScoreList;
		ArrayList<Float> geneCommonSyntenyScoreList;

		ArrayList<Species> speciesList;

		ArrayList<Couple<String, Float>> ncNCList; 
		ArrayList<Couple<String, Float>> geneNCHitList;
		ArrayList<Couple<String, Float>> geneSyntenyScoreList;

		refSpecies = 0;
		reader = new SimpleFileIO();

		if(!refSyntenyFile.equals("")) {
			try {
				window.addMessageForReader("[STATUS] - Reading Reference Synteny File ... ");
				window.updateEastPanelText(2);
				reader.readSyntenyFile(path, refSyntenyFile);
				window.addMessageForReader("[STATUS] - Finished loading ref synteny.");
				window.updateEastPanelText(2);
				refSpecies = reader.getNumberofSpecies();
			} catch (Exception e) {
				window.addMessageForReader("[ERROR] - Reading reference synteny file failed. Check file path and file format etc.");
				window.updateEastPanelText(3);
				return false;
			}
		}

		try {
			window.addMessageForReader("[STATUS] - Reading Query Synteny File ... ");
			window.updateEastPanelText(2);
			reader.readSyntenyFile(path, querySyntenyFile);
			window.addMessageForReader("[STATUS] - Finished loading query synteny.");
			window.updateEastPanelText(2);
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Reading query synteny file failed. Check file path and file format etc.");
			window.updateEastPanelText(3);
			return false;
		}

		testSpecies = reader.getNumberofSpecies();
		speciesList = reader.getSpeciesList();

		try {
			window.addMessageForReader("[STATUS] - Reading Neighborhood Corr and Syntenic Scores File ... ");
			window.updateEastPanelText(2);
			speciesList = reader.readNCandSyntenyFile(path, ncAndSNSfile, speciesList);
			window.addMessageForReader("[STATUS] - Finished loading Neighborhood Corr and Syntenic Scores File.");
			window.updateEastPanelText(2);
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Reading NC and SySc score file failed. Check file path and file format etc.");
			window.updateEastPanelText(3);
			return false;
		}

		window.addMessageForReader("[METHOD] - Computing Syntenic Correlation Scores ... ");
		window.updateEastPanelText(2);
		
		int count = 0;
		for(int i = refSpecies; i < testSpecies; i++) {
			species = speciesList.get(i);
			for(int j = 0; j < species.getChromosomes().size(); j++) {
				chromosome = species.getChromosomes().get(j);
				for(int k = 0; k < chromosome.getNumberOfGenes(); k++) {
					gene = chromosome.getGeneList().get(k);
					geneNCHitList = gene.getGenencScore();
					geneSyntenyList = gene.getSyntenyScore();
					geneSyntenyScoreList = new ArrayList<Couple<String, Float>>();
					for(int l = 0; l < gene.getGenencScore().size(); l++) {
						ncHit = geneNCHitList.get(l).first;
						syntenyScore = geneSyntenyList.get(l);
						geneSyntenyScoreList.add(new Couple<String, Float>(ncHit, syntenyScore));
					}
					
					count++;
					if(count%10 == 0) {
						window.addMessageForReader("[METHOD] - Computed correlation scores for " + count + " genes ...");
						window.updateEastPanelText(2);
					}

					for(int l = 0; l < gene.getGenencScore().size(); l++) {
						ncHit1 = geneNCHitList.get(l).first;
						String[] ncArr = ncHit1.split("\\.");
						if(Integer.parseInt(ncArr[0]) >= refSpecies) {
							ncGene = speciesList.get(Integer.parseInt(ncArr[0])).getChromosomes().get(Integer.parseInt(ncArr[1])).getGeneList().get(Integer.parseInt(ncArr[2]));
							ncNCList = ncGene.getGenencScore(); 
							ncSyntenyList = ncGene.getSyntenyScore();
							ncSyntenyScoreList = new ArrayList<Float>();
							geneCommonSyntenyScoreList = new ArrayList<Float>();
							for(int m = 0; m < ncGene.getGenencScore().size(); m++) {
								ncNCHit = ncNCList.get(m).first;
								for(int n = 0; n < geneSyntenyScoreList.size(); n++) {
									if(ncNCHit.equals(geneSyntenyScoreList.get(n).first)) {
										ncSyntenyScore = ncSyntenyList.get(m);
										ncSyntenyScoreList.add(ncSyntenyScore);
										geneCommonSyntenyScoreList.add(geneSyntenyScoreList.get(n).second);
										break;
									}
								}
							}
							ncArray = ncSyntenyScoreList.toArray(new Float[ncSyntenyScoreList.size()]);
							gene1 = speciesList.get(Integer.parseInt(ncArr[0])).getChromosomes().get(Integer.parseInt(ncArr[1])).getGeneList().get(Integer.parseInt(ncArr[2])).getGeneID();
							try {
								if(geneCommonSyntenyScoreList.size() > 0) {
									geneArr = geneCommonSyntenyScoreList.toArray(new Float[geneCommonSyntenyScoreList.size()]);

									//								syntenyCorr = Correlation.getPearsonCorrelation(geneArr, ncArray);
									syntenyCorr = (float)Correlation.computeCorrelation(geneArr, ncArray);
									//							double correlation = new PearsonsCorrelation().correlation(geneArr, ncArray);
									if(syntenyCorr < 0 || Float.isNaN(syntenyCorr))
										syntenyCorr = (float)0;
									if(syntenyCorr > 1)
										syntenyCorr = (float)1;
									if(!gene.getGeneID().equals(gene1)) 
										SequenceFileWriter.writeAndAppendLine(path, output + ".syc", gene.getIndex() + "\t" + ncHit1 + "\t" + geneNCHitList.get(l).second + "\t" + syntenyCorr);
								} else if(!gene.getGeneID().equals(gene1)) 
									SequenceFileWriter.writeAndAppendLine(path, output + ".syc", gene.getIndex() + "\t" + ncHit1 + "\t" + geneNCHitList.get(l).second + "\t" + 0.0);			
							} catch (Exception e) {
								window.addMessageForReader("[ERROR] - Calculating synteny correlation score failed. Check that you have write permissions for files etc.");
								window.updateEastPanelText(3);
								return false;
							}
						}
					}
				}
			}
		}
		window.addMessageForReader("[METHOD] - Finished computing synteny correlation scores!");
		window.updateEastPanelText(2);
		return true;
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public String getModuleName() {
		return "SyntenyCorrelation";
	}

	public void main(String[] args) throws Exception {
		Parameters params;
		String path;
		String ncAndSNSfile;
		String refSyntenyFile;
		String querySyntenyFile;
		String output;

		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"SyntenyCorrelationModule is ........... \n\n" +
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar SyntenyCorrelationModule [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}

			path = params.path;
			ncAndSNSfile = params.files.get(0);
			refSyntenyFile = params.refSynteny;
			querySyntenyFile = params.files.get(1);
			output = params.output;

			if(!path.endsWith("/"))
				path = path.concat("/");

			new SyntenyCorrelationScore(path, refSyntenyFile, querySyntenyFile, ncAndSNSfile, output);

			Calendar cal1 = Calendar.getInstance();
			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df1.format(cal1.getTime()));
		} catch (Exception e) {
			System.out.println("Synteny Correlation Module Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		String path;
		String ncAndSNSfile;
		String refSyntenyFile;
		String querySyntenyFile;
		String output;

		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);

			path = params.path;
			ncAndSNSfile = params.files.get(0);
			refSyntenyFile = params.refSynteny;
			querySyntenyFile = params.files.get(1);
			output = params.output;

			if(!path.endsWith("/"))
				path = path.concat("/");

			SyntenyCorrelationScore sc = new SyntenyCorrelationScore();
			return sc.computeSyntenyCorrScore(window, path, refSyntenyFile, querySyntenyFile, ncAndSNSfile, output);
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Synteny Correlation Module Failed : " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}


	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */

}
