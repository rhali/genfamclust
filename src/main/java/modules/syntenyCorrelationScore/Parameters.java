package modules.syntenyCorrelationScore;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Handles all regular application parameters for Synteny Correlation Module. Used for parameter parsing for SNC module.
 * @author Raja Hashim Ali.
 */
public class Parameters {
	/** Required parameters: Application/Module to run */
	@Parameter(description = "<Neighborhood Correlation and Synteny Score File> <Query synteny file>")
	public List<String> files = new ArrayList<String>();
	
	/** Help. */
	@Parameter(names = {"-h", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Path for finding the data and outputting results */
	@Parameter(names = {"-p","--path"}, description = "Path for finding the data and outputting results.")
	public String path = "./";
	
	/** Name of synteny Correlation output file without extension (.snc is used by default) */
	@Parameter(names = {"-o","--output"}, description = "Name of Synteny Correlation output file without extension (.snc is used by default).")
	public String output = "GenFam";
	
	/** Reference synteny file if any in hsf format. */
	@Parameter(names = {"-r","--refsynteny"}, description = "reference synteny file if any in hsf format.")
	public String refSynteny = "";
}
