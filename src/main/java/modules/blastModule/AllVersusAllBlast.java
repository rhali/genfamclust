package modules.blastModule;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import miscellaneous.Sequence;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import miscellaneous.Software;
import miscellaneous.gui.GFCWindow;

/**
 * The class that contains the gist of blast module. It performs the blasting of all
 * the test sequences in the database consisting of reference sequences using the
 * makeblastdb and blastp programs in the <code>Blastp</code> and <code>makeblastdb</code>
 * classes.
 * @author Raja Hashim Ali
 *
 **/
public class AllVersusAllBlast implements Software {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private String 		outFile;
	private String 		dataPath;
	private String 		resourcePath;
	private String 		resultPath;
	private MakeBlastdb makeblastdb;
	private Blastp 		blastp;
	private Blastn 		blastn;
	ArrayList<Sequence> sequences;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public AllVersusAllBlast(String path, String resourcePath) {
		dataPath = path;
		this.resourcePath = resourcePath;
		resultPath = path;
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void setRelativePath(String resourcePath, String dataPath, String resultPath) {
		this.resourcePath = resourcePath;
		this.dataPath = dataPath;
		this.resultPath = resultPath;
	}

	public String[] makeCommand() {
		return null;
	}

	public boolean runCommand(String[] arguments) {
		try {
			String 			result;
			String 			databaseFile;
			Sequence 		sequence;
			SimpleFileIO 	reader;
			
			String testSeqFile = arguments[0];
			String refSeqFile = arguments[1];
			outFile = arguments[2] + ".bl";
			int start = Integer.parseInt(arguments[3]) - 1;
			int end = Integer.parseInt(arguments[4]) - 1;
			String evalue = arguments[5];
			int completeBlastInteger = Integer.parseInt(arguments[6]);
			boolean completeBlast;
			result 	= "";
			
			makeblastdb = new MakeBlastdb(resourcePath, resultPath, refSeqFile, arguments[2], arguments[7]);
			makeblastdb.runCommand(makeblastdb.makeCommand());
			
			if(completeBlastInteger == 0)
				completeBlast = false;
			else
				completeBlast = true;

			if(arguments[7].equals("p")) {
				blastp = new Blastp(arguments[2], evalue, completeBlast);
				blastp.setRelativePath(resourcePath, resultPath, resultPath);
			} else {
				blastn = new Blastn(arguments[2], evalue, completeBlast);
				blastn.setRelativePath(resourcePath, resultPath, resultPath);
			}
			
			reader = new SimpleFileIO();
			reader.readSequenceFile(dataPath, testSeqFile);
			sequences = reader.getSequences();

			if(end == 0)
				end = sequences.size();
			
			int size = end - start;
			System.out.println("Blasting " + size + " sequences ... INITIATED" );
			
			for (int i = start; i < end; i++) {
				sequence = sequences.get(i);
				SequenceFileWriter.writeSequenceInFasta(resultPath, "input.fa", sequence);
				
				int displaySize = size/100;
				if(displaySize != 0 && (i % displaySize) == 0) {
					float totalblastCompleted = ((float)((i - start)/displaySize));
					int blastCompleted = (int)totalblastCompleted;
					Calendar cal = Calendar.getInstance();
				    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SS");
					System.out.println("# Current time: " + df.format(cal.getTime()));
					System.out.print("Sequences Blasted Status " + blastCompleted + "% ... ");
					System.out.println(i + " sequences" );
				}

				if(arguments[7].equals("p")) 
					blastp.runCommand(blastp.makeCommand());
				else
					blastn.runCommand(blastn.makeCommand());
					
				result = SimpleFileIO.FileReader(resultPath + "output.blp");
				SequenceFileWriter.writeAndAppendString(dataPath, outFile, result);
			}
			System.out.print("Sequences Blasted = 100% ... ");
			System.out.println(size + " sequences" );
			
			SimpleFileIO.FileDeleter(resultPath + "output.blp");
			SimpleFileIO.FileDeleter(resultPath + "input.fa");

			if(arguments[7].equals("p")) {
				databaseFile = resultPath + arguments[2] + ".psq";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".phr";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".pin";
				SimpleFileIO.FileDeleter(databaseFile);
			} else {
				databaseFile = resultPath + arguments[2] + ".nsq";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".nhr";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".nin";
				SimpleFileIO.FileDeleter(databaseFile);
			}
		} catch(Exception E) {
			System.out.println(E.getMessage());
			System.exit(-1);
		}
		return true;
	}
	
	public boolean runCommand(GFCWindow window, String[] arguments) {
		try {
			String 			result;
			String 			databaseFile;
			Sequence 		sequence;
			SimpleFileIO 	reader;
			
			String testSeqFile = arguments[0];
			String refSeqFile = arguments[1];
			outFile = arguments[2] + ".bl";
			int start = Integer.parseInt(arguments[3]) - 1;
			int end = Integer.parseInt(arguments[4]) - 1;
			String evalue = arguments[5];
			int completeBlastInteger = Integer.parseInt(arguments[6]);
			boolean completeBlast;
			result 	= "";
			
			if(new File(outFile).exists()) {
				window.addMessageForReader("[ERROR] - Blast file already exists at " + outFile + ". Delete file first OR consider using all-vs-all blast file option!");
				window.updateEastPanelText(3);
				return false;
			}
			
			makeblastdb = new MakeBlastdb(resourcePath, resultPath, refSeqFile, arguments[2], arguments[7]);
			makeblastdb.runCommand(window, makeblastdb.makeCommand());
			
			if(completeBlastInteger == 0)
				completeBlast = false;
			else
				completeBlast = true;

			if(arguments[7].equals("p")) {
				blastp = new Blastp(arguments[2], evalue, completeBlast);
				blastp.setRelativePath(resourcePath, resultPath, resultPath);
			} else {
				blastn = new Blastn(arguments[2], evalue, completeBlast);
				blastn.setRelativePath(resourcePath, resultPath, resultPath);
			}
			
			reader = new SimpleFileIO();
			reader.readSequenceFile(dataPath, testSeqFile);
			sequences = reader.getSequences();

			if(end == 0)
				end = sequences.size();
			
			int size = end - start;
			window.addMessageForReader("[STATUS] - Blasting " + size + " sequences ... INITIATED");
			window.updateEastPanelText(2);
			
			for (int i = start; i < end; i++) {
				sequence = sequences.get(i);
				SequenceFileWriter.writeSequenceInFasta(resultPath, "input.fa", sequence);
				
				int displaySize = size/100;
				if(displaySize != 0 && (i % displaySize) == 0) {
					float totalblastCompleted = ((float)((i - start)/displaySize));
					int blastCompleted = (int)totalblastCompleted;
					
					window.addMessageForReader("[STATUS] - Sequences Blasted Status " + blastCompleted + "% ... " + i + " sequences");
					window.updateEastPanelText(2);

				}

				if(arguments[7].equals("p")) 
					blastp.runCommand(blastp.makeCommand());
				else
					blastn.runCommand(blastn.makeCommand());
					
				result = SimpleFileIO.FileReader(resultPath + "output.blp");
				SequenceFileWriter.writeAndAppendString(dataPath, outFile, result);
			}
			window.addMessageForReader("[STATUS] - Sequences Blasted = 100% ..." + size + " sequences");
			window.updateEastPanelText(2);
			
			SimpleFileIO.FileDeleter(resultPath + "output.blp");
			SimpleFileIO.FileDeleter(resultPath + "input.fa");

			if(arguments[7].equals("p")) {
				databaseFile = resultPath + arguments[2] + ".psq";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".phr";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".pin";
				SimpleFileIO.FileDeleter(databaseFile);
			} else {
				databaseFile = resultPath + arguments[2] + ".nsq";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".nhr";
				SimpleFileIO.FileDeleter(databaseFile);
				databaseFile = resultPath + arguments[2] + ".nin";
				SimpleFileIO.FileDeleter(databaseFile);
			}
		} catch(Exception e) {
			window.addMessageForReader("[ERROR] - All vs All blast failed : " + e.getMessage());
			window.updateEastPanelText(3);
		}
		return true;
	}

	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
