package modules.blastModule;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import miscellaneous.Software;
import miscellaneous.gui.GFCWindow;

/**
 * 
 * @author Raja Hashim Ali
 **/
public class MakeBlastdb implements Software{
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private String softwareName;
	private String dataPath;
	private String resourcePath;
	private String refSeqFile;
	private String dbTitle;
	private String blastType;

	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public MakeBlastdb(String resourcePath, String path, String refSeqFile, String dbTitle, String blastType){
		softwareName = "makeblastdb";
		dataPath = path;
		this.resourcePath = resourcePath;
		this.refSeqFile = refSeqFile;
		this.dbTitle = dbTitle;
		this.blastType = blastType;
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void displayHelp() {
		String[] command = {resourcePath + softwareName, "-help"};
		runCommand(command);
	}

	public void setRelativePath(String resourcePath, String dataPath, String resultPath) {
		this.resourcePath 	= resourcePath;
		this.dataPath 		= dataPath;
	}

	public String[] makeCommand() {
		if(blastType.equals("p")) {
			String[] command = {resourcePath + softwareName, "-dbtype", "prot", "-in", dataPath + refSeqFile, "-out", dataPath + dbTitle};
			return command;
		} else {
			String[] command = {resourcePath + softwareName, "-dbtype", "nucl", "-in", dataPath + refSeqFile, "-out", dataPath + dbTitle};
			return command;
		}
	}

	public boolean runCommand(String command[]) {
		int 				exitVal;
		String 				line;
		Runtime 			runtime;
		Process 			process;
		InputStream 		stdin;
		InputStream 		stderr;
		BufferedReader 		bufferedreader;
		InputStreamReader 	inputstreamreader;

		try {
			runtime = Runtime.getRuntime();
			process = runtime.exec(command, null, null);
			stdin = process.getInputStream();
			inputstreamreader = new InputStreamReader(stdin);
			bufferedreader = new BufferedReader(inputstreamreader);
			while ( (line = bufferedreader.readLine()) != null)
				System.out.println(line);

			exitVal = process.waitFor();
			if(exitVal != 0) {
				stderr = process.getErrorStream();
				inputstreamreader = new InputStreamReader(stderr);
				bufferedreader = new BufferedReader(inputstreamreader);
				while ( (line = bufferedreader.readLine()) != null)
					System.out.println(line);
			}
			System.out.println("makeblastdb exitValue: " + exitVal);
			return true;
		} catch(Exception e) {
			System.out.println(e.toString());
			System.exit(-1);
			return false;
		}
	}
	
	public boolean runCommand(GFCWindow window, String command[]) {
		int 				exitVal;
		String 				line;
		Runtime 			runtime;
		Process 			process;
		InputStream 		stdin;
		InputStream 		stderr;
		BufferedReader 		bufferedreader;
		InputStreamReader 	inputstreamreader;

		try {
			runtime = Runtime.getRuntime();
			process = runtime.exec(command, null, null);
			stdin = process.getInputStream();
			inputstreamreader = new InputStreamReader(stdin);
			bufferedreader = new BufferedReader(inputstreamreader);
			window.addMessageForReader("[STATUS] - Making blast database ... Please wait.");
			window.updateEastPanelText(2);
			while ( (line = bufferedreader.readLine()) != null) {
				window.addMessageForReader("[STATUS] - makeblastdb - " + line);
				window.updateEastPanelText(2);
			}

			exitVal = process.waitFor();
			if(exitVal != 0) {
				stderr = process.getErrorStream();
				inputstreamreader = new InputStreamReader(stderr);
				bufferedreader = new BufferedReader(inputstreamreader);
				while ( (line = bufferedreader.readLine()) != null) {
					window.addMessageForReader("[ERROR] - makeblastdb - " + line);
					window.updateEastPanelText(3);
					return false;
				}
			}
			return true;
		} catch(Exception e) {
			window.addMessageForReader("[ERROR] - makeblastdb - " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}

	public String getModuleName() {
		return "makeblastdb";
	}

	public void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

	}
}
