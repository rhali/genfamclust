package modules.blastModule;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Handles all regular application parameters for Blast Module. Used for parameter parsing for blast module.
 * @author Raja Hashim Ali.
 */
public class Parameters {

	/** Required parameters: Application/Module to run */
	@Parameter(description = "<Test sequences file>")
	public List<String> files = new ArrayList<String>();
	
	/** Help. */
	@Parameter(names = {"-h", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Name of File Containing Reference Sequences in Fasta format */
	@Parameter(names = {"-s","--refsequencefile"}, description = "Reference Sequence file in fasta format.")
	public String refseqfile = "";
	
	/** Path for finding the data and outputting results */
	@Parameter(names = {"-p","--path"}, description = "Path for finding the data and outputting results.")
	public String path = "./";
	
	/** Name of blast output file without extension (.bl is used by default) */
	@Parameter(names = {"-o","--output"}, description = "Name of blast output file without extension (.bl is used by default).")
	public String output = "GenFam";
	
	/** Test Sequence from which to start Blast with the reference database */
	@Parameter(names = {"-bs","--blaststart"}, description = "Test Sequence from which to start Blast with the reference database. Useful in cases, where blasting can be done in multiple processes instead of a single process e.g. in a cluster.")
	public int start = 1;
	
	/** Test Sequence on which to end Blast with the reference database */
	@Parameter(names = {"-be","--blastend"}, description = "Test Sequence on which to end Blast with the reference database. Useful in cases, where blasting can be done in multiple processes instead of a single process e.g. in a cluster.")
	public int end = 1;
	
	/** Blast resource path, where this module can find blastp and makeblastdb programs of NCBI blast+ package */
	@Parameter(names = {"-rp","--resourcepath"}, description = "Blast resource path, where this module can find blastp and makeblastdb programs of NCBI blast+ package. ")
	public String resourcepath = "./";
	
	/** Evalue threshold for blastp of NCBI blast+ package */
	@Parameter(names = {"-e","--evalue"}, description = "Evalue threshold for blastp of NCBI blast+ package. ")
	public String evalue = "0.1";
	
	/** Blast type - protein or nucleotide */
	@Parameter(names = {"-t","--typeblast"}, description = "Perform nucleotide (n) or protein blast (p)")
	public String type = "p";
	
	/** */
	@Parameter(names = {"-cb","--completeblast"}, description = "Whether one requires only bitscore or all the outfmt -6 information. ")
	public boolean completeBlast = false;
}
