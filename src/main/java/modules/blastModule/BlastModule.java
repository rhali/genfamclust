package modules.blastModule;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.gui.GFCWindow;

import com.beust.jcommander.JCommander;

import modules.GenFamModules;

/**
 * The blast class that is the entry point to the blast application for GenFamClust.
 * Defines the parameters and verifies the input provided to the 
 * <code>AllVersusAllBlast</code> class.
 * @author Raja Hashim Ali
 **/
public class BlastModule implements GenFamModules {
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public BlastModule() {
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void main(String[] args) {
		Parameters params;
		String resourcePath;
		String path;
		String querySeqFile;
		String refSeqFile;
		String outputFile;
		String start;
		String end;
		String evalue;
		String blastType;
		int completeBlast;
		AllVersusAllBlast avab;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"BlastModule is an implementation of a custom version of All versus All Blast which\n" +
						"only blasts all the test sequences (say M) against all the reference sequences \n " + 
						"(say N) as per the requirement and advantages of M versus N blast in the Neighbor-\n" +
						"-hood Correlation score calculation in the next module. \n\n" +
						"The main advantages of using this specialized blast is improved speed, option of \n" +
						"breaking up the single operation into smaller multiple operations and above all \n" +
						"fewer number of overall operations resulting in a slight complexity advantage when \n" +
						"compared with the actual all-versus-all blast [O(NM) versus O(N^2) with M << N] \n\n" +
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Basic local alignment search tool, \n" + 
						"    Altschul et al., J. Mol. Biol. 215:403-410\n\n" +
						"    Gapped BLAST and PSI-Blast: a new generation of protein database search programs ,\n" +
						"    Altschul et al., Nucleic Acids Res. 25:3389-3402 \n\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar BlastModule [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}

			Calendar cal = Calendar.getInstance();
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df.format(cal.getTime()) + '\n');
			
			querySeqFile = params.files.get(0);
			refSeqFile = params.refseqfile;
			outputFile = params.output;
			start = String.valueOf(params.start);
			end = String.valueOf(params.end);
			evalue = params.evalue;
			path = params.path;
			resourcePath = params.resourcepath;
			blastType = params.type;
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			if(!resourcePath.endsWith("/"))
				resourcePath = resourcePath.concat("/");
			
			if(refSeqFile == "") {
				System.out.println("No reference sequence file found. Taking query data as reference data as well.");
				refSeqFile = querySeqFile;
			}
			
			if(params.completeBlast == false)
				completeBlast = 0;
			else
				completeBlast = 1;
				
			
			String[] arguments = {querySeqFile, refSeqFile, outputFile, start, end, evalue, String.valueOf(completeBlast), blastType};
			
			avab = new AllVersusAllBlast(path, resourcePath);
			avab.runCommand(arguments);
			
			Calendar cal1 = Calendar.getInstance();
		    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df1.format(cal1.getTime()) + '\n');
			
			System.out.println("Blasting Status ... COMPLETED");
		} catch (Exception e) {
			System.out.println("Blast Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		String resourcePath;
		String path;
		String querySeqFile;
		String refSeqFile;
		String outputFile;
		String start;
		String end;
		String evalue;
		String blastType;
		int completeBlast;
		AllVersusAllBlast avab;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);

			Calendar cal = Calendar.getInstance();
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    window.addMessageForReader("[METHOD] - time: " + df.format(cal.getTime()));
			window.updateEastPanelText(2);
			
			querySeqFile = params.files.get(0);
			refSeqFile = params.refseqfile;
			outputFile = params.output;
			start = String.valueOf(params.start);
			end = String.valueOf(params.end);
			evalue = params.evalue;
			path = params.path;
			resourcePath = params.resourcepath;
			blastType = params.type;
			
			if(new File(outputFile).exists()) {
				window.addMessageForReader("[ERROR] - Blast file already exists at " + outputFile + ". Delete file first OR consider using all-vs-all blast file option!");
				window.updateEastPanelText(3);
				return false;
			}
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			if(!resourcePath.endsWith("/"))
				resourcePath = resourcePath.concat("/");
			
			if(refSeqFile == "") {
			    window.addMessageForReader("[METHOD] - No reference sequence file found. Taking query data as reference data as well.");
				window.updateEastPanelText(2);
				refSeqFile = querySeqFile;
			}
			
			if(params.completeBlast == false)
				completeBlast = 0;
			else
				completeBlast = 1;
				
			
			String[] arguments = {querySeqFile, refSeqFile, outputFile, start, end, evalue, String.valueOf(completeBlast), blastType};
			
			avab = new AllVersusAllBlast(path, resourcePath);
			avab.runCommand(window, arguments);
			
			Calendar cal1 = Calendar.getInstance();
		    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    window.addMessageForReader("[METHOD] - time: " + df1.format(cal1.getTime()));
			window.updateEastPanelText(2);
			
			window.addMessageForReader("[METHOD] - All vs All blast scores computed");
			window.updateEastPanelText(2);
			return true;
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - All vs All blast scores failed : " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}

	public String getModuleName() {
		return "BlastModule";
	}

	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
