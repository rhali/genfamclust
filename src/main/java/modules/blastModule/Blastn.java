package modules.blastModule;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import miscellaneous.Software;

public class Blastn implements Software {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private String softwareName;
	private String resourcePath;
	private String resultPath;
	private String dbTitle;
	private String evalue;
	private boolean full;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public Blastn(String dbTitle, String evalue, boolean completeBlast){
		softwareName = "blastn";
		resourcePath = ABSOLUTE_PATH + "Resources/Blast/bin/";
		resultPath = ABSOLUTE_PATH + "Results/";
		this.dbTitle = dbTitle;
		this.evalue = evalue;
		full = completeBlast;
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void displayHelp() {
		String[] command = {resourcePath + softwareName, "-h"};
		runCommand(command);
	}

	public void setRelativePath(String resourcePath, String dataPath, String resultPath) {
		this.resourcePath = resourcePath;
		this.resultPath = resultPath;
	}
	
	public String[] makeCommand() {
		if(full != true) {
			String[] command = {resourcePath + softwareName, "-db", resultPath + dbTitle, "-query", resultPath + "input.fa", "-out", resultPath + "output.blp", "-outfmt", "6 qseqid sseqid bitscore", "-evalue", evalue};
			return command;
		} else {
			String[] command = {resourcePath + softwareName, "-db", resultPath + dbTitle, "-query", resultPath + "input.fa", "-out", resultPath + "output.blp", "-outfmt", "6", "-evalue", evalue};
			return command;
		}
	}

	public boolean runCommand(String[] command) {
		int 				exitVal;
		String 				line;
		Runtime 			runtime;
		Process 			process;
		InputStream 		stdin;
		InputStream 		stderr;
		BufferedReader 		bufferedreader;
		InputStreamReader 	inputstreamreader;
				
		try {
			runtime = Runtime.getRuntime();
			process = runtime.exec(command, null, null);
			stdin = process.getInputStream();
			inputstreamreader = new InputStreamReader(stdin);
			bufferedreader = new BufferedReader(inputstreamreader);
            while ( (line = bufferedreader.readLine()) != null)
                System.out.println(line);
			
			exitVal = process.waitFor();
			if(exitVal != 0) {
				stderr = process.getErrorStream();
				inputstreamreader = new InputStreamReader(stderr);
				bufferedreader = new BufferedReader(inputstreamreader);
	            while ( (line = bufferedreader.readLine()) != null)
	                System.out.println(line);
			}
			return true;
		} catch(Exception e) {
			System.out.println("Error in blastn: " + e.getMessage());
			System.exit(-1);
			return false;
		}
	}

	public String getModuleName() {
		return "blastn";
	}

	public void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
