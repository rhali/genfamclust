package modules;

import java.util.Set;
import java.util.TreeMap;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

import miscellaneous.gui.*;

/**
 * Starter class for all modules. Used as a gateway to all modules in
 * <code>genfamclust</code> package, and uses the corresponding class name
 * as a module identifier. There is only one entry point and modules are thus accessed by:
 * <code>java -jar GenFamClust-X.Y.Z.jar -Xmx3096m -Xms2048m [module-name] [module-specific-options] [args] </code>
 * 
 * @author Joel Sjöstrand.
 * @author Raja Hashim Ali.
 */
public class GenFamClustStarter {
	/**
	 * Starts a GenFamClust application located in the <code>modules</code> folder
	 * (or sub-folder).
	 * @param args program name, followed by arguments (and necessarily in that order).
	 * @throws Exception.
	 */
	public static void main(String[] args) throws Exception {
		
		// Find all starter methods in modules package.
		Logger logger = (Logger) Reflections.log;
		Level oldLvl = logger.getLevel();
		logger.setLevel(Level.OFF);
		Reflections reflections = new Reflections(
			    new ConfigurationBuilder()
			        .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("modules")))
			        .setUrls(ClasspathHelper.forPackage("modules"))
			        .setScanners(new SubTypesScanner())
			        
			);
		Set<Class<? extends GenFamModules>> apps = reflections.getSubTypesOf(GenFamModules.class);
		logger.setLevel(oldLvl);
		
		// Create map linking name and module (we assume uniqueness).
		TreeMap<String, GenFamModules> map = new TreeMap<String, GenFamModules>();
		for (Class<? extends GenFamModules> c : apps) {
			try {
				// NOTE: Empty constructor assumed!!!!
				GenFamModules app = c.newInstance();
				map.put(app.getModuleName(), app);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		if(args.length == 0)
			new GFCX();
		else if (args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("--help")) {
			// No module specified.
			System.out.println(
					"================================================================================\n" +
					"GenFamClust is a Java library for primarily phylogenetics designed by Lars Arvestad, \n" +
					"Raja Hashim Ali and Sayyed Auwn Muhammad and developed by Raja Hashim Ali \n" +
					"at Science for Life Laboratory (SciLifeLab) in Stockholm, Sweden.\n\n" +
					"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
					"License: GenFamClust is available under the New BSD License.\n"
					);
			System.out.println("Usage: GenFamClust-x.y.z.jar <module> [options] <arguments>\n");
			System.out.println("List of available modules:");
			for (String k : map.keySet()) {
				System.out.println("    " + k);
			}
			System.out.println("You can usually obtain module-specific help thus:\n" +
					"    java -jar GenFamClust-x.y.z.jar <module> -h");
			System.out.println("================================================================================\n");
		} else if (!map.containsKey(args[0])) {
			System.out.println("Unknown module. Use -h to show help.");
			System.out.println("Usage: java -jar GenFamClust-x.y.z.jar <module> [options] <arguments>");
		} else {
			// Start module. Remove module name first, though.
			String[] moduleArgs = new String[args.length - 1];
			System.arraycopy(args, 1, moduleArgs, 0, moduleArgs.length);
			GenFamModules module = map.get(args[0]);
			module.main(moduleArgs);
		}
	}
}
