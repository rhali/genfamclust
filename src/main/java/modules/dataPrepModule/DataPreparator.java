package modules.dataPrepModule;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.Sequence;
import miscellaneous.SequenceFileWriter;
import miscellaneous.Species;
import miscellaneous.SimpleFileIO;
import miscellaneous.gui.GFCWindow;
import modules.GenFamModules;
import modules.dataPrepModule.Parameters;

/**
 * 
 * @author Raja Hashim Ali
 **/
public class DataPreparator implements GenFamModules {
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public DataPreparator() {
	}
	
	public DataPreparator(String path, String refSequencesFile, String refSyntenyFile, String querySequencesFile, String querySyntenyFile, String output) throws Exception {
		SimpleFileIO reader;
		int totalSpeciesSize;
		int percentage;
		float percent;
		ArrayList<Sequence> sequences;
		ArrayList<Species> speciesList;

		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df.format(cal.getTime()));
		
		reader = new SimpleFileIO();
		
		if(refSequencesFile != "") {
			System.out.print("Reading sequences from " + refSequencesFile + " ... ");
			reader.readSequenceFile(path, refSequencesFile);
			System.out.println(" DONE \n");
			System.out.print("Reading synteny information from " + refSyntenyFile + " ... ");
			reader.readSyntenyFile(path, refSyntenyFile);
			System.out.println(" DONE \n");
		}
		else
			System.out.print("No ref sequence information provided ... Taking query cases as reference cases as well ...");
		
		System.out.print("Reading sequences from " + querySequencesFile + " ... ");
		reader.readSequenceFile(path, querySequencesFile);
		System.out.println(" DONE \n");
		System.out.print("Reading synteny information from " + querySyntenyFile + " ... ");
		reader.readSyntenyFile(path, querySyntenyFile);
		System.out.println(" DONE \n");
		
		sequences = reader.getSequences();
		speciesList	= reader.getSpeciesList();
		totalSpeciesSize = reader.getNumberofSpecies();
		
		System.out.println("Preparing data ... Please wait ... ");
		System.out.println("Total Species = " + totalSpeciesSize);
		System.out.println("Data Processed = " + 0 + "%");
		
		for(int i = 0; i < totalSpeciesSize; i++) {
			for(int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
				for(int k = 0; k < speciesList.get(i).getChromosomes().get(j).getGeneList().size(); k++) {
					String temp = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGeneID().trim();
					for(int l = 0; l < sequences.size(); l++) {
						String temp1 = sequences.get(l).getIdentifier().trim();
						if(temp.equals(temp1)) {
							Sequence sequence = new Sequence(speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getIndex(), sequences.get(l).getSequence());
							SequenceFileWriter.writeAndAppendSequenceInFasta(path, output + ".fasta", sequence);
							sequences.remove(l);
							break;
						}
					}
				}
			}
			percent = ((float)(((float)((float)((float)i+1)/totalSpeciesSize)) * 100));
			percentage = (int)percent;
			System.out.println("Data Processed = " + percentage + "%");
		}
		Calendar cal1 = Calendar.getInstance();
	    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df1.format(cal1.getTime()));
		
		System.out.println("Dataset Processed and Prepared ... COMPLETED");
	}
	
	public DataPreparator(GFCWindow window, String path, String refSequencesFile, String refSyntenyFile, String querySequencesFile, String querySyntenyFile, String output) throws Exception {
		SimpleFileIO reader;
		int totalSpeciesSize;
		int percentage;
		float percent;
		ArrayList<Sequence> sequences;
		ArrayList<Species> speciesList;

		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		window.addMessageForReader("[METHOD] - Time: " + df.format(cal.getTime()));
		window.updateEastPanelText(2);
		
		reader = new SimpleFileIO();
		
		if(refSequencesFile != "") {
			window.addMessageForReader("[METHOD] - Reading sequences from " + refSequencesFile + " ... ");
			window.updateEastPanelText(2);
			reader.readSequenceFile(path, refSequencesFile);
			window.addMessageForReader("[METHOD] - Finished loading reference sequences");
			window.addMessageForReader("[METHOD] - Reading synteny information from " + refSyntenyFile + " ... ");
			window.updateEastPanelText(2);
			reader.readSyntenyFile(path, refSyntenyFile);
			window.addMessageForReader("[METHOD] - Finished loading reference synteny");
			window.updateEastPanelText(2);
		}
		else {
			window.addMessageForReader("[METHOD] - No ref sequence information provided ... Taking query cases as reference cases as well ...");
			window.updateEastPanelText(2);
		}
		window.addMessageForReader("[METHOD] - Reading sequences from " + querySequencesFile + " ... ");
		window.updateEastPanelText(2);
		reader.readSequenceFile(path, querySequencesFile);
		window.addMessageForReader("[METHOD] - Finished loading query sequences");
		window.addMessageForReader("[METHOD] - Reading query information from " + querySyntenyFile + " ... ");
		window.updateEastPanelText(2);
		reader.readSyntenyFile(path, querySyntenyFile);
		window.addMessageForReader("[METHOD] - Finished loading query synteny");
		window.updateEastPanelText(2);
		
		sequences = reader.getSequences();
		speciesList	= reader.getSpeciesList();
		totalSpeciesSize = reader.getNumberofSpecies();
		
		window.addMessageForReader("[STATUS] - Preparing data ...");
		window.addMessageForReader("[STATUS] - Total Species = " + totalSpeciesSize);
		window.addMessageForReader("[STATUS] - Data Processed = " + 0 + "%");
		window.updateEastPanelText(2);
		
		for(int i = 0; i < totalSpeciesSize; i++) {
			for(int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
				for(int k = 0; k < speciesList.get(i).getChromosomes().get(j).getGeneList().size(); k++) {
					String temp = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGeneID().trim();
					for(int l = 0; l < sequences.size(); l++) {
						String temp1 = sequences.get(l).getIdentifier().trim();
						if(temp.equals(temp1)) {
							Sequence sequence = new Sequence(speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getIndex(), sequences.get(l).getSequence());
							SequenceFileWriter.writeAndAppendSequenceInFasta(path, output + ".fasta", sequence);
							sequences.remove(l);
							break;
						}
					}
				}
			}
			percent = ((float)(((float)((float)((float)i+1)/totalSpeciesSize)) * 100));
			percentage = (int)percent;
			window.addMessageForReader("[STATUS] - Data Processed = " + percentage + "%");
			window.updateEastPanelText(2);
		}
		
		window.addMessageForReader("[STATUS] - Dataset Processed and Prepared ... COMPLETED");
		window.updateEastPanelText(2);
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public String getModuleName() {
		return "DataPreparator";
	}

	public void main(String[] args) throws Exception {
		Parameters params;
		String path;
		String refSequenceFile;
		String refSyntenyFile;
		String querySequenceFile;
		String querySyntenyFile;
		String output;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"DataPreparator is ........... \n\n" +
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar DataPreparator [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}
			
			path = params.path;
			querySequenceFile = params.files.get(0);
			querySyntenyFile = params.files.get(1);
			refSequenceFile = params.refseqfile;
			refSyntenyFile = params.refsynteny;
			output = params.output;
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			new DataPreparator(path, refSequenceFile, refSyntenyFile, querySequenceFile, querySyntenyFile, output);
		} catch (Exception e) {
			System.out.println("Data Preparator Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		String path;
		String refSequenceFile;
		String refSyntenyFile;
		String querySequenceFile;
		String querySyntenyFile;
		String output;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);
			
			path = params.path;
			querySequenceFile = params.files.get(0);
			querySyntenyFile = params.files.get(1);
			refSequenceFile = params.refseqfile;
			refSyntenyFile = params.refsynteny;
			output = params.output;
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			new DataPreparator(window, path, refSequenceFile, refSyntenyFile, querySequenceFile, querySyntenyFile, output);
			return true;
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Data Preprocessing failed : " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
