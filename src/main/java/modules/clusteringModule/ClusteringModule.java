package modules.clusteringModule;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.SequenceFileWriter;
import miscellaneous.SimpleFileIO;
import miscellaneous.Species;
import miscellaneous.gui.GFCWindow;

/**
 * The class that contains the methods for clustering data into gene families after the data 
 * been evaluated by the modelEvaluation module. It performs the average linkage clustering,
 * aka UPGMA method, single linkage clustering aka nearest neighbor and complete linkage 
 * clustering aka farthest neighbor.
 * @author Raja Hashim Ali
 *
 **/
public class ClusteringModule {
	/*'******************************************************************************
	 * 							CLASS VARIABLES										*
	 ********************************************************************************/
	private float[][] distanceMatrix;
	private int[] result;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public ClusteringModule() {
		
	}
	
	public ClusteringModule(String path, String querySyntenyFile, String evaluationFile, String reference) throws Exception {
		String str;
		String[] strArr;
		String[] gene1;
		String[] gene2;
		SimpleFileIO reader;
		int count;
		int counter;
		int species1;
		int species2;
		int refSpecies;
		int chr1;
		int chr2;
		int gen1;
		int gen2;
		int[] countedgenes;
		BufferedReader bufferedreader;
		ArrayList<Species> speciesList;
		
		counter = 0;
		reader = new SimpleFileIO();
		if(!reference.equals("")) {
			reader.readSyntenyFile(path, reference);
			refSpecies = reader.getNumberofSpecies();
		} else
			refSpecies = 0;
		reader = null;
		reader = new SimpleFileIO();
		reader.readSyntenyFile(path, querySyntenyFile);
		speciesList = reader.getSpeciesList();
		count = reader.getNumberOfGenes();
		distanceMatrix = new float[count][count];
		
		for(int i = 0; i < count; i++)
			for(int j = 0; j < count; j++)
				distanceMatrix[i][j] = (float)0;
		
		for(int i = refSpecies; i < speciesList.size(); i++) 
			for(int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) 
				counter++;
		
		countedgenes = new int[counter];
		counter = 0;
		count = 0;
		
		for(int i = 0; i < speciesList.size(); i++) {
			for(int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
				counter = counter + speciesList.get(i).getChromosomes().get(j).getGeneList().size();
				countedgenes[count] = counter;
				count++;
			}
		}
		
		result = new int[countedgenes[countedgenes.length-1]];
		for(int i = 0; i < result.length; i++)
			result[i] = i;
		
		bufferedreader = new BufferedReader(new FileReader(path + evaluationFile));
		
		while ((str = bufferedreader.readLine()) != null) {
			strArr = str.split("\t");
			gene1 = strArr[0].split("\\.");
			gene2 = strArr[1].split("\\.");
			
			species1 = Integer.parseInt(gene1[0]);
			species2 = Integer.parseInt(gene2[0]);
			chr1 = Integer.parseInt(gene1[1]);
			chr2 = Integer.parseInt(gene2[1]);
			gen1 = Integer.parseInt(gene1[2]);
			gen2 = Integer.parseInt(gene2[2]);			
			
			count = 0;
			
			for(int i = refSpecies; i < species1; i++)
				for(int j = 0; j < speciesList.get(i).getChromosomes().size(); j++)
					count++;
			
			count = count + chr1;
			
			int position1;
			if (count == 0)
				position1 = gen1;
			else
				position1 = countedgenes[count -1] + gen1;
			
			count = 0;
			
			for(int i = refSpecies; i < species2; i++)
				for(int j = 0; j < speciesList.get(i).getChromosomes().size(); j++)
					count++;
			
			count = count + chr2;
			
			int position2;
			if (count == 0)
				position2 = gen2;
			else
				position2 = countedgenes[count - 1] + gen2;
			
			distanceMatrix[position1][position2] = Float.parseFloat(strArr[2]);
			distanceMatrix[position2][position1] = distanceMatrix[position1][position2];
		}
		bufferedreader.close();
	}
	
	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	private int[] performSingleLinkage(float[][] distanceMatrix, int numberOfClusters, int[] result, int count, float score) {
		if(distanceMatrix[1].length < 3) {
			System.out.println("Matrix length < 3");
			return result;
		} else if ( distanceMatrix[1].length <= numberOfClusters) {
			System.out.println("Number of clusters are less than number of clusters");
			return result;
		} else if (score > 0.5) {
			System.out.println("Score exceeds 0.5");
			return result;
		} else {
			float[][] distmatrix = new float[distanceMatrix.length-1][distanceMatrix.length-1];
			float minimum;
			float score1;
			int index1 = 0;
			int index2 = 1;
			int unique = 1;
			int tempindex1;
			int tempindex2;
			int[] uniqueObservation;
			
			minimum	= distanceMatrix[0][1];
			for(int i = 0; i < distanceMatrix.length; i++) 
				for(int j = 0; j < distanceMatrix.length; j++) 
					if(i<j)
						if(minimum > distanceMatrix[i][j]) {
							minimum = distanceMatrix[i][j];
							index1 = i;
							index2 = j;
						}
			
			if(minimum >= 0.5) {
				System.out.println("Can not combine. Score exceeds max allowed." + minimum);
				System.out.println("DistanceMatrix for run " + count);
				printMatrix(distanceMatrix);
				return result;
			}
			
			uniqueObservation	= new int[distanceMatrix.length];
			uniqueObservation[0] = result[0];
			
			for(int i = 1; i < result.length; i++){
				boolean check = true;
				for(int j = 0; j < unique; j++){
					if(result[i] == uniqueObservation[j]){
						check = false;
						break;
					}
				}
				if(check == true) {
					uniqueObservation[unique]= result[i];
					unique++;
				}
			}
			tempindex1 = index1;
			tempindex2 = index2;
			
			if(uniqueObservation[index1] > uniqueObservation[index2]){
				int temp = index1;
				tempindex1 = index2;
				tempindex2 = temp;
			}
			result = updateArray(result, uniqueObservation, tempindex1, tempindex2);
			
			for(int i = 0; i < index2; i++)
				for(int j = 0; j < index2; j++) {
					distmatrix	[i][j] 		= distanceMatrix[i][j];
					distmatrix	[j][i] 		= distmatrix[i][j];
				}
			
			for(int i = index2; i < distmatrix.length; i++) {
				for(int j = 0; j < index2; j++) {
					distmatrix	[i][j] 		= distanceMatrix[i+1][j];
					distmatrix	[j][i] 		= distmatrix[i][j];
				}
				
				for(int j = index2; j < distmatrix.length; j++) {
					distmatrix	[i][j] 		= distanceMatrix[i+1][j+1];
					distmatrix	[j][i] 		= distmatrix[i][j];
				}
			}
			
			for(int i = 0; i < distmatrix.length; i++){
				if(i == index1) 
					distmatrix	[index1][i] = 0;
				else if(i < index2) {
					if(distanceMatrix[index1][i] > distanceMatrix[index2][i])
						distmatrix[index1][i] = distanceMatrix[index2][i];
					else
						distmatrix[index1][i] = distanceMatrix[index1][i];
				} else {
					if(distanceMatrix[index1][i+1] > distanceMatrix[index2][i+1])
						distmatrix[index1][i] = distanceMatrix[index2][i+1];
					else
						distmatrix[index1][i] = distanceMatrix[index1][i+1];
				}
				distmatrix[i]	[index1] 	= distmatrix[index1][i];
			}
			for(int i = 0; i < result.length; i++)
				System.out.print(result[i] + " ");
			System.out.println();
			
			score1 = distanceMatrix[index1][index2];
			
			distanceMatrix = null;
			count++;
			result = performSingleLinkage(distmatrix, numberOfClusters, result, count, score1);
			return result;
		}
	}
	
	private float computeSum(float[][] distanceMatrix, int row) {
		float sum = 0;
		for (int i = 0; i < distanceMatrix[0].length; i++)
			sum = sum + distanceMatrix[row][i];
		return sum;
	}
	
	private int[] updateArray(int[] array, int[] unique, int index1, int index2){
		int val1 = unique[index1];
		int val2 = unique[index2];
		
		for(int i = 0; i < array.length; i++)
			if(array[i] == val1) 
				array[i] = val2;
		return array;
	}
	
	private void printMatrix(float[][] distanceMatrix){
		for(int i = 0; i < distanceMatrix.length; i++) {
			for(int j = 0; j < distanceMatrix.length; j++)
				System.out.print(distanceMatrix[i][j] + "\t");
			System.out.println("\n");
		}
	}
	
	private int[] performNJ(float[][] distanceMatrix, int numberOfClusters, int[] result, int count, float score) {
		if(distanceMatrix[1].length < 3) {
			System.out.println("Matrix length < 3");
			return result;
		} else if ( distanceMatrix[1].length <= numberOfClusters) {
			System.out.println("Number of clusters are less than number of clusters");
			return result;
		} else if (score > 0.5) {
			System.out.println("Score exceeds 0.5");
			return result;
		} else {
			float[][] distmatrix = new float[distanceMatrix.length-1][distanceMatrix.length-1];
			float[][] QMatrix = new float[distanceMatrix.length]	[distanceMatrix.length];
			float[] sumArray = new float[distanceMatrix.length];
			float minimum;
			int index1 = 0;
			int index2 = 1;
			
			for(int i = 0; i < distanceMatrix.length; i++)
				sumArray[i] = computeSum(distanceMatrix, i)/(distanceMatrix.length - 2);
			
			minimum	= distanceMatrix[0][1] - sumArray[0] - sumArray[1];
			for(int i = 0; i < distanceMatrix.length; i++) {
				for(int j = 0; j < distanceMatrix.length; j++) {
					if(i < j) {
						QMatrix[i][j] = distanceMatrix[i][j] - sumArray[i] - sumArray[j];
						QMatrix[j][i] = QMatrix[i][j];
						
						if(minimum > QMatrix[i][j]) {
							minimum = QMatrix[i][j];
							index1 = i;
							index2 = j;
						}
					} else if(i==j)
						QMatrix[i][j] = 0;
				}
			}
			
			int[] uniqueObservation = new int[distanceMatrix.length];
			int unique = 1;
			uniqueObservation[0] 			= result[0];
			
			for(int i = 1; i < result.length; i++){
				boolean check = true;
				for(int j = 0; j < unique; j++){
					if(result[i] == uniqueObservation[j]){
						check = false;
						break;
					}
				}
				if(check == true) {
					uniqueObservation[unique]= result[i];
					unique++;
				}
			}
			int tempindex1 = index1;
			int tempindex2 = index2;
			
			if(uniqueObservation[index1] > uniqueObservation[index2]){
				int temp = index1;
				tempindex1 = index2;
				tempindex2 = temp;
			}
			result = updateArray(result, uniqueObservation, tempindex1, tempindex2);
			
			for(int i = 0; i < index2; i++)
				for(int j = 0; j < index2; j++) {
					distmatrix[i][j] = distanceMatrix[i][j];
					distmatrix[j][i] = distmatrix[i][j];
				}

			for(int i = index2; i < distmatrix.length; i++) 
				for(int j = 0; j < index2; j++) {
					distmatrix[i][j] = distanceMatrix[i+1][j];
					distmatrix[j][i] = distmatrix[i][j];
				}
			
			for(int i = index2; i < distmatrix.length; i++) 
				for(int j = index2; j < distmatrix.length; j++) {
					distmatrix[i][j] = distanceMatrix[i+1][j+1];
					distmatrix[j][i] = distmatrix[i][j];
				}
			
			for(int i = 0; i< distmatrix.length; i++){
				if(i == index1) 
					distmatrix	[index1][i] = 0;
				else if(i < index1)
					distmatrix[index1][i] = (float) ((float)(distanceMatrix[index1][i] + distanceMatrix[i][index2] - distanceMatrix[index1][index2])/2);
				else
					distmatrix[index1][i] = (float) ((float)(distanceMatrix[index1][i+1] + distanceMatrix[i+1][index2] - distanceMatrix[index1][index2])/2);
				distmatrix[i][index1] = distmatrix[index1][i];
			}
			System.out.println("DistanceMatrix for run " + count);
			printMatrix(distanceMatrix);
			System.out.println("Qmatrix for run " + count);
			printMatrix(QMatrix);
			System.out.println("Combining " + index1 + " with " + index2);
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			for(int i = 0; i < result.length; i++)
				System.out.println(result[i]);
			
			float score1 = distanceMatrix[index1][index2];
			
			distanceMatrix = null;
			QMatrix = null;
			count++;
			result = performNJ(distmatrix, numberOfClusters, result, count, score1);
			return result;
		}
	}
	
	private int[] performAverageLinkage(float[][] distanceMatrix, int numberOfClusters, int[] result, int count, float score, int[] clusterMembers) {
		if(distanceMatrix[1].length < 3) {
			System.out.println("Matrix length < 3");
			return result;
		} else if ( distanceMatrix[1].length <= numberOfClusters) {
			System.out.println("Number of clusters are less than number of clusters");
			return result;
		} else if (score > 0.5) {
			System.out.println("Score exceeds 0.5");
			return result;
		} else {
			float[][] distmatrix = new float[distanceMatrix.length-1][distanceMatrix.length-1];
			float minimum;
			int index1 = 0;
			int index2 = 1;
			int[] newClusterMembers = new int[clusterMembers.length - 1];
			int[] uniqueObservation = new int[distanceMatrix.length];
			int unique = 1;
			int tempindex1;
			int tempindex2;
			
			minimum	= distanceMatrix[0][1];
			for(int i = 0; i < distanceMatrix.length; i++) 
				for(int j = 0; j < distanceMatrix.length; j++) 
					if(i<j)
						if(minimum > distanceMatrix[i][j]) {
							minimum	= distanceMatrix[i][j];
							index1 = i;
							index2 = j;
						}
			
			if(minimum >= 0.5) {
				System.out.println("Can not combine. Score exceeds max allowed." + minimum);
				System.out.println("DistanceMatrix for run " + count);
				printMatrix(distanceMatrix);
				return result;
			}
			
			uniqueObservation[0] 			= result[0];
			
			for(int i = 1; i < result.length; i++){
				boolean check = true;
				for(int j = 0; j < unique; j++){
					if(result[i] == uniqueObservation[j]){
						check = false;
						break;
					}
				}
				if(check == true) {
					uniqueObservation[unique]= result[i];
					unique++;
				}
			}
			tempindex1 = index1;
			tempindex2 = index2;
			
			if(uniqueObservation[index1] > uniqueObservation[index2]){
				int temp = index1;
				tempindex1 = index2;
				tempindex2 = temp;
			}
			result = updateArray(result, uniqueObservation, tempindex1, tempindex2);
			
			
			for(int i = 0; i < index2; i++)
				for(int j = 0; j < index2; j++) {
					distmatrix[i][j] = distanceMatrix[i][j];
					distmatrix[j][i] = distmatrix[i][j];
				}
			
			for(int i = index2; i < distmatrix.length; i++) {
				for(int j = 0; j < index2; j++) {
					distmatrix[i][j] = distanceMatrix[i+1][j];
					distmatrix[j][i] = distmatrix[i][j];
				}
				
				for(int j = index2; j < distmatrix.length; j++) {
					distmatrix[i][j] = distanceMatrix[i+1][j+1];
					distmatrix[j][i] = distmatrix[i][j];
				}
			}
			
			for(int i = 0; i < distmatrix.length; i++){
				if(i == index1) 
					distmatrix	[index1][i] = 0;
				else if(i < index2) 
					distmatrix[index1][i] = ((distanceMatrix[index1][i] * clusterMembers[index1]) + (distanceMatrix[index2][i] * clusterMembers[index2]))/(clusterMembers[index1]+clusterMembers[index2]);
				else 
					distmatrix[index1][i] = ((distanceMatrix[index1][i+1] * clusterMembers[index1]) + (distanceMatrix[index2][i+1] * clusterMembers[index2]))/(clusterMembers[index1]+clusterMembers[index2]);

				distmatrix[i][index1] = distmatrix[index1][i];
			}
			
			for(int i = 0; i < newClusterMembers.length; i++) {
				if(i == index1)
					newClusterMembers[i] = clusterMembers[index1] + clusterMembers[index2];
				else if(i < index2)
					newClusterMembers[i] = clusterMembers[i];
				else
					newClusterMembers[i] = clusterMembers[i+1];
			}
				
			for(int i = 0; i < result.length; i++)
				System.out.print(result[i] + " ");
			System.out.println();
			float score1 = distanceMatrix[index1][index2];
			
			distanceMatrix = null;
			clusterMembers = null;
			count++;
			result = performAverageLinkage(distmatrix, numberOfClusters, result, count, score1, newClusterMembers);
			return result;
		}
	}
	
	private int[] performCompleteLinkage(float[][] distanceMatrix, int numberOfClusters, int[] result, int count, float score) {
		if(distanceMatrix[1].length < 3) {
			System.out.println("Matrix length < 3");
			return result;
		} else if ( distanceMatrix[1].length <= numberOfClusters) {
			System.out.println("Number of clusters are less than number of clusters");
			return result;
		} else if (score > 0.5) {
			System.out.println("Score exceeds 0.5");
			return result;
		} else {
			float[][] distmatrix 			= new float[distanceMatrix.length-1][distanceMatrix.length-1];
			float minimum;
			int index1 = 0;
			int index2 = 1;
			
			minimum	= distanceMatrix[0][1];
			for(int i = 0; i < distanceMatrix.length; i++) {
				for(int j = 0; j < distanceMatrix.length; j++) {
					if(i<j){
						if(minimum > distanceMatrix[i][j]) {
							minimum = distanceMatrix[i][j];
							index1 = i;
							index2 = j;
						}
					} 
				}
			}
			
			if(minimum >= 0.5) {
				System.out.println("Can not combine. Score exceeds max allowed." + minimum);
				System.out.println("DistanceMatrix for run " + count);
				printMatrix(distanceMatrix);
				return result;
			}
			
			int[] uniqueObservation = new int[distanceMatrix.length];
			int unique = 1;
			uniqueObservation[0] = result[0];
			
			for(int i = 1; i < result.length; i++){
				boolean check = true;
				for(int j = 0; j < unique; j++){
					if(result[i] == uniqueObservation[j]){
						check = false;
						break;
					}
				}
				if(check == true) {
					uniqueObservation[unique]= result[i];
					unique++;
				}
			}
			int tempindex1 = index1;
			int tempindex2 = index2;
			
			if(uniqueObservation[index1] > uniqueObservation[index2]){
				int temp = index1;
				tempindex1 = index2;
				tempindex2 = temp;
			}
			result = updateArray(result, uniqueObservation, tempindex1, tempindex2);
			
			for(int i = 0; i < index2; i++)
				for(int j = 0; j < index2; j++) {
					distmatrix[i][j] = distanceMatrix[i][j];
					distmatrix[j][i] = distmatrix[i][j];
				}
			
			for(int i = index2; i < distmatrix.length; i++) {
				for(int j = 0; j < index2; j++) {
					distmatrix[i][j] = distanceMatrix[i+1][j];
					distmatrix[j][i] = distmatrix[i][j];
				}
				
				for(int j = index2; j < distmatrix.length; j++) {
					distmatrix[i][j] = distanceMatrix[i+1][j+1];
					distmatrix[j][i] = distmatrix[i][j];
				}
			}
			
			for(int i = 0; i < distmatrix.length; i++){
				if(i == index1) 
					distmatrix[index1][i] = 0;
				else if(i < index2) {
					if(distanceMatrix[index1][i] > distanceMatrix[index2][i])
						distmatrix[index1][i] = distanceMatrix[index1][i];
					else
						distmatrix[index1][i] = distanceMatrix[index2][i];
				} else {
					if(distanceMatrix[index1][i+1] > distanceMatrix[index2][i+1])
						distmatrix[index1][i] = distanceMatrix[index1][i+1];
					else
						distmatrix[index1][i] = distanceMatrix[index2][i+1];
				}
				distmatrix[i][index1] = distmatrix[index1][i];
			}
			for(int i = 0; i < result.length; i++)
				System.out.print(result[i] + " ");
			System.out.println();
			
			float score1 = distanceMatrix[index1][index2];
			
			distanceMatrix = null;
			count++;
			result = performCompleteLinkage(distmatrix, numberOfClusters, result, count, score1);
			return result;
		}
	}
	
	private void convertToDistance() {
		for(int i = 0; i < distanceMatrix.length; i++)
			for(int j = 0; j < distanceMatrix[i].length; j++) {
				if(i!=j)
					distanceMatrix[i][j] = 1 - distanceMatrix[i][j];
				else
					distanceMatrix[i][j] = 0;
			}
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public String getModuleName() {
		return "ClusteringModule";
	}
	
	public void method(String method, String path, String referenceSyntenyFile, String querySyntenyFile, String output, String evaluationFile) throws Exception {
		BufferedReader 	bufferedreader;
		int[] 			res;
		
		convertToDistance();
		if(method.equals("a")) {
			System.out.println("Average Linkage Method");
			int[] clusterMembers = new int[result.length];
			for(int i = 0; i < clusterMembers.length; i++)
				clusterMembers[i] = 1;
			res = performAverageLinkage(distanceMatrix, 2, result, 1, 0, clusterMembers);
			for(int i = 0; i < res.length; i++)
				System.out.print(res[i] + "\t");
		} else if (method.equals("c")) {
			System.out.println("Complete Linkage Method");
			res = performCompleteLinkage(distanceMatrix, 2, result, 1, 0);
			for(int i = 0; i < res.length; i++)
				System.out.print(res[i] + "\t");
		} else if (method.equals("s")) {
			System.out.println("Single Linkage Method");
			res = performSingleLinkage(distanceMatrix, 2, result, 1, 0);
			for(int i = 0; i < res.length; i++)
				System.out.print(res[i] + "\t");
		} else if (method.equals("n")) {
			System.out.println("NJ Method");
			res = performNJ(distanceMatrix, 2, result, 1, 0);
			for(int i = 0; i < res.length; i++)
				System.out.print(res[i] + "\t");
		} else {
			System.out.println("Invalid option. a, c, s or n only.");
			System.exit(-1);
			res = new int[1];
		}
		distanceMatrix = null;
	}
	
	public void method(GFCWindow window, String method, String path, String referenceSyntenyFile, String querySyntenyFile, String output, String evaluationFile) throws Exception {
		int[] 			res = null;
		
		convertToDistance();
		if(method.equals("a")) {
			window.addMessageForReader("Performing Average Linkage Clustering ...");
			window.updateEastPanelText(1);
			int[] clusterMembers = new int[result.length];
			for(int i = 0; i < clusterMembers.length; i++)
				clusterMembers[i] = 1;
			res = performAverageLinkage(distanceMatrix, 2, result, 1, 0, clusterMembers);
		} else if (method.equals("c")) {
			window.addMessageForReader("Performing Complete Linkage Clustering ...");
			window.updateEastPanelText(1);
			res = performCompleteLinkage(distanceMatrix, 2, result, 1, 0);
		} else if (method.equals("s")) {
			window.addMessageForReader("Performing Single Linkage Clustering ...");
			window.updateEastPanelText(1);
			res = performSingleLinkage(distanceMatrix, 2, result, 1, 0);
		} else if (method.equals("n")) {
			window.addMessageForReader("Performing Neighbor Joining ...");
			window.updateEastPanelText(1);
			res = performNJ(distanceMatrix, 2, result, 1, 0);
		}
		SequenceFileWriter.writeAndAppendString(path, output, String.valueOf(res[0]));
		for(int i = 1; i < res.length; i++)
			SequenceFileWriter.writeAndAppendString(path, output, "\t" + res[i]);
		SequenceFileWriter.writeAndAppendString(path, output, "\n");
		distanceMatrix = null;
	}
	
	public void main(String[] args) throws Exception {
		Parameters params;
		Calendar cal;
		SimpleDateFormat df;
		ClusteringModule cluster;
		String path;
		String querySyntenyFile;
		String evaluationFile;
		String method;
		String referenceSyntenyFile;
		String output;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"ClusteringModule is an implementation of a custom version of Single Linkage clustering,\n" +
						"Average Linkage Clustering and Complete Linkage clustering. \n " + 
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Ultra-fast sequence clustering from similarity networks with SiLiX, \n" + 
						"    Vincent M. et al, BMC Bioinformatics 2011, 12:116\n\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar ClusteringModule [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}

			cal = Calendar.getInstance();
		    df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df.format(cal.getTime()) + '\n');
			
			path = params.path;
			method = params.method;
			querySyntenyFile = params.files.get(0);
			evaluationFile = params.files.get(1);
			referenceSyntenyFile = params.reference;
			output = params.output;
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			cluster = new ClusteringModule(path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
			cluster.method(method, path, referenceSyntenyFile, querySyntenyFile, output, evaluationFile);
			
		} catch (Exception e) {
			System.out.println("Clustering Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public void main(GFCWindow window, String[] args) {
		Parameters params;
		Calendar cal;
		SimpleDateFormat df;
		ClusteringModule cluster;
		String path;
		String querySyntenyFile;
		String evaluationFile;
		String method;
		String referenceSyntenyFile;
		String output;
		
		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);

			cal = Calendar.getInstance();
		    df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    window.addMessageForReader("# Current time: " + df.format(cal.getTime()) + '\n');
			window.updateEastPanelText(1);
			
			path = params.path;
			method = params.method;
			querySyntenyFile = params.files.get(0);
			evaluationFile = params.files.get(1);
			referenceSyntenyFile = params.reference;
			output = params.output;
			
			if(!path.endsWith("/"))
				path = path.concat("/");
			
			cluster = new ClusteringModule(path, querySyntenyFile, evaluationFile, referenceSyntenyFile);
			cluster.method(window, method, path, referenceSyntenyFile, querySyntenyFile, output, evaluationFile);
			
			cal = Calendar.getInstance();
			window.addMessageForReader("Completed inferring gene families from homologs.");
			window.addMessageForReader("# Current time: " + df.format(cal.getTime()) + '\n');
			window.updateEastPanelText(1);
		} catch (Exception e) {
			window.addMessageForReader("Clustering Failed : " + e.getMessage());
			window.updateEastPanelText(3);
		}
	}
}
