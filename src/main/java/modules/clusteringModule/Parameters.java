package modules.clusteringModule;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Handles all regular application parameters for Clustering Module. Used for parameter parsing for clustering module.
 * @author Raja Hashim Ali.
 */
public class Parameters {

	/** Required parameters: Application/Module to run */
	@Parameter(description = "<Query Synteny File> <Evaluated file from modelEvaluation>")
	public List<String> files = new ArrayList<String>();
	
	@Parameter(names = {"-h", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Path of files */
	@Parameter(names = {"-p","--path"}, description = "Path for finding the data and outputting results.")
	public String path = "./";
	
	/** Reference filename */
	@Parameter(names = {"-r","--ref"}, description = "Reference File for estimating the starting position of query sequences.")
	public String reference = "";
	
	@Parameter(names = {"-m", "--method"}, description = "Method for clustering. To perform \"Single Linkage (s)\", \"Complete Linkage (c)\" or \"Average Linkage (a)\", choose the corresponding alphabet in brackets")
	public String method = "n";
	
	@Parameter(names = {"-o", "--output"}, description = "Output file name with .clu")
	public String output = "genFam";
	
	@Parameter(names = {"-t", "--threshold"}, description = "Maximum threhsold for combining members.")
	public String threshold = "0.5";
}
