package modules.syntenyExtractorModule;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

import miscellaneous.Couple;
import miscellaneous.Gene;
import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.SequenceFileWriter;
import miscellaneous.Species;
import miscellaneous.SimpleFileIO;
import miscellaneous.gui.GFCWindow;
import modules.GenFamModules;
import modules.syntenyExtractorModule.Parameters;

/**
 * The class that is entry point to synteny score computation module and also contains
 * the method to compute synteny scores and synteny correlation scores. 
 * @author Raja Hashim Ali
 **/
public class SyntenyExtractor implements GenFamModules {
	/*'******************************************************************************
	 * 							CLASS VARIABLES										*
	 ********************************************************************************/
	private ArrayList<Species> speciesList;
	private String path;
	private String refSyntenyFile;
	private String querySyntenyFile;
	private String ncScoreFile;
	private int neighborhoodSize;
	private int speciesStart;
	private int speciesEnd;
	private String output;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public SyntenyExtractor() {}

	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	private void syntenyComputation() throws Exception {
		int species;
		int geneIndex;
		int chromosome;
		int displaySize;
		int geneCounter;
		int numberofgenes;
		int chromosomeSize;
		int neighborChromosomeSize;
		
		int[] neighborHitCounter;
		int[] ncNeighborHitCounter;
		
		float maximumScore;
		float allSumApproach;
		float neighborMaxScore;
		float ncNeighborMaxScore;
		float sumBestGeneNeighborApproach;
		float sumBestGeneAndNcNeighborApproach;
		
		Float[] maxValue1;
		Float[] maxValue2;
		
		SimpleFileIO reader;
				
		Gene[] neighborhoodGenes;
		Gene[] ncNeighborGenes;
		
		Couple<String, Float> blastRes;
		
		ArrayList<Couple<String, Float>> neighborNcRes;
		
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df.format(cal.getTime()) + '\n');
		
		geneCounter = 0;
		numberofgenes = 0;
		reader = new SimpleFileIO();
		
		if(!refSyntenyFile.equals("")) {
			System.out.print("Reading Reference Synteny File ... ");
			reader.readSyntenyFile(path, refSyntenyFile);
			System.out.println("COMPLETED");
		} else 
			System.out.println("No reference data found. Computing synteny scores for query dataset only.");
		
		if(speciesStart == 0) 
			speciesStart = reader.getSpeciesList().size();

		System.out.print("Reading Query Synteny File ... ");
		reader.readSyntenyFile(path, querySyntenyFile);
		System.out.println("COMPLETED");

		speciesList	= reader.getSpeciesList();
		
		if(speciesEnd == 0) 
			speciesEnd = speciesList.size();

		System.out.print("Reading NC File ... ");
		speciesList = reader.readNCFile(path, ncScoreFile, speciesList);
		System.out.println("COMPLETED");
		reader = null;

		for (int i = speciesStart; i < speciesEnd; i++) {
			for (int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
				chromosomeSize = speciesList.get(i).getChromosomes().get(j).getNumberOfGenes();
				numberofgenes = numberofgenes + chromosomeSize;
			}
		}

		if(numberofgenes>100)
			displaySize = numberofgenes/100;
		else if(numberofgenes > 5)
			displaySize = 5;
		else
			displaySize = numberofgenes;
		
		System.out.println("Synteny Extractor status ... INITIATED");
		System.out.println("Total gene pairs to process ... " + numberofgenes);
		
		for (int i = speciesStart; i < speciesEnd; i++) {
			for (int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
				chromosomeSize = speciesList.get(i).getChromosomes().get(j).getNumberOfGenes();
				
				for (int k = 0; k < chromosomeSize; k++) {
					geneCounter++;

					if(geneCounter % displaySize == 0) {
						float totalblastCompleted = ((float)geneCounter/numberofgenes) * 100;
						System.out.print("Synteny Scores Computation Status ... " + totalblastCompleted + "% ... ");
						System.out.println(geneCounter + "/" + numberofgenes + " genes ... PROCESSED." );
					}

					if(chromosomeSize == 1) {
						for (int l = 0; l < speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().size(); l++) {
							blastRes = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().get(l);
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_ms.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_asa.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbganna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbgna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");									
						}
						continue;
					} else if(k >= neighborhoodSize && k < (chromosomeSize - neighborhoodSize)) {
						neighborhoodGenes = new Gene[2 * neighborhoodSize];
						neighborHitCounter = new int[2 * neighborhoodSize];
						for(int p = 0; p < neighborhoodSize; p++) {
							neighborhoodGenes[p + neighborhoodSize] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k + p + 1);
							neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k - p - 1);
						}
					} else if (k >= neighborhoodSize && k >= (chromosomeSize - neighborhoodSize)) {
						neighborhoodGenes = new Gene[neighborhoodSize + (chromosomeSize - k - 1)];
						neighborHitCounter = new int[neighborhoodSize + (chromosomeSize - k - 1)];
						for(int p = 0; p < neighborhoodSize; p++) 
							neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k - p - 1);
						for (int p = 0; p < (chromosomeSize - k - 1); p++) 
							neighborhoodGenes[p + neighborhoodSize] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k + p + 1);
					} else if (k < neighborhoodSize && k < (chromosomeSize - neighborhoodSize)) {
						neighborhoodGenes = new Gene[neighborhoodSize + k];
						neighborHitCounter = new int[neighborhoodSize + k];
						for(int p = 0; p < k; p++) 
							neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
						for (int p = k + 1; p < (k + neighborhoodSize + 1); p++) 
							neighborhoodGenes[p - 1] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
					} else {
						neighborhoodGenes = new Gene[chromosomeSize - 1];
						neighborHitCounter = new int[chromosomeSize - 1];
						for(int p = 0; p < k; p++) 
							neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
						for(int p = k + 1; p < chromosomeSize; p++) 
							neighborhoodGenes[p - 1] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
					}

					for (int l = 0; l < speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().size(); l++) {
						blastRes = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().get(l);
						String[] strArr = blastRes.first.split("\\.");
						species = Integer.parseInt(strArr[0]);
						chromosome = Integer.parseInt(strArr[1]);
						geneIndex = Integer.parseInt(strArr[2]);
						neighborChromosomeSize = speciesList.get(species).getChromosomes().get(chromosome).getNumberOfGenes();
						
						if(neighborChromosomeSize == 1) {
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_ms.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_asa.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbganna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbgna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");									
							continue;
						} else if(geneIndex >= neighborhoodSize && geneIndex < (neighborChromosomeSize - neighborhoodSize)) {
							ncNeighborGenes = new Gene[2 * neighborhoodSize];
							ncNeighborHitCounter = new int[2 * neighborhoodSize];
							for(int p = 0; p < neighborhoodSize; p++) {
								ncNeighborGenes[p + neighborhoodSize] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex + p + 1);
								ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex - p - 1);
							}
						} else if (geneIndex >= neighborhoodSize && geneIndex >= (neighborChromosomeSize - neighborhoodSize)) {
							ncNeighborGenes = new Gene[neighborhoodSize + (neighborChromosomeSize - geneIndex - 1)];
							ncNeighborHitCounter = new int[neighborhoodSize + (neighborChromosomeSize - geneIndex - 1)];
							for(int p = 0; p < neighborhoodSize; p++) 
								ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex - p - 1);
							for (int p = 0; p < (neighborChromosomeSize - geneIndex - 1); p++) 
								ncNeighborGenes[p + neighborhoodSize] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex + p + 1);
						} else if (geneIndex < neighborhoodSize && geneIndex < (neighborChromosomeSize - neighborhoodSize)) {
							ncNeighborGenes = new Gene[neighborhoodSize + geneIndex];
							ncNeighborHitCounter = new int[neighborhoodSize + geneIndex];
							for(int p = 0; p < geneIndex; p++) 
								ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
							for (int p = geneIndex + 1; p < (geneIndex + neighborhoodSize + 1); p++) 
								ncNeighborGenes[p - 1] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
						} else {
							ncNeighborGenes = new Gene[neighborChromosomeSize - 1];
							ncNeighborHitCounter = new int[neighborChromosomeSize-1];
							for(int p = 0; p < geneIndex; p++) 
								ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
							for(int p = geneIndex + 1; p < neighborChromosomeSize; p++) 
								ncNeighborGenes[p - 1] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
						}

						for(int d = 0; d < ncNeighborHitCounter.length; d++) 
							ncNeighborHitCounter[d] = 0;

						for(int d = 0; d < neighborHitCounter.length; d++) 
							neighborHitCounter[d] = 0;

						maximumScore = (float) 0;
						maxValue1 = new Float[neighborhoodGenes.length];
						maxValue2 = new Float[ncNeighborGenes.length];

						allSumApproach = (float) 0;

						for(int d = 0; d < maxValue1.length; d++) 
							maxValue1[d] = (float)0;
						for(int d = 0; d < maxValue2.length; d++) 
							maxValue2[d] = (float)0;

						for(int a = 0; a < neighborhoodGenes.length; a++) {
							neighborMaxScore = (float) 0;
							for(int b = 0; b < ncNeighborGenes.length; b++) {
								neighborNcRes = neighborhoodGenes[a].getGenencScore();
								for(int c = 0; c < neighborNcRes.size(); c++) {
									String genIndex = neighborNcRes.get(c).first.trim();
									if(genIndex.equals(ncNeighborGenes[b].getIndex().trim())) {
										neighborHitCounter[a]++;
										allSumApproach = allSumApproach + neighborNcRes.get(c).second;
										
										if(neighborNcRes.get(c).second > neighborMaxScore) 
											neighborMaxScore = neighborNcRes.get(c).second;
										if(neighborNcRes.get(c).second > maximumScore) 
											maximumScore = neighborNcRes.get(c).second;
									}
								}
							}
							maxValue1[a] = neighborMaxScore;
						}

						for(int b = 0; b < ncNeighborGenes.length; b++) {
							ncNeighborMaxScore = (float) 0;
							for(int a = 0; a < neighborhoodGenes.length; a++) {
								neighborNcRes = ncNeighborGenes[b].getGenencScore();
								for(int c = 0; c < neighborNcRes.size(); c++) {
									String genIndex = neighborNcRes.get(c).first.trim();
									if(genIndex.equals(neighborhoodGenes[a].getIndex().trim()) && neighborNcRes.get(c).second > ncNeighborMaxScore)
										ncNeighborMaxScore = neighborNcRes.get(c).second;
									if(genIndex.equals(neighborhoodGenes[a].getIndex().trim()))
										ncNeighborHitCounter[b]++;
								}
							}
							maxValue2[b] = ncNeighborMaxScore;
						}

						sumBestGeneNeighborApproach = (float) 0;

						for(int b = 0; b < maxValue1.length; b++) 
							sumBestGeneNeighborApproach = sumBestGeneNeighborApproach + maxValue1[b];
						
						sumBestGeneAndNcNeighborApproach = (float) 0;
						
						for(int b = 0; b < maxValue2.length; b++) 
							sumBestGeneAndNcNeighborApproach = sumBestGeneAndNcNeighborApproach + maxValue2[b];
						
						sumBestGeneAndNcNeighborApproach += sumBestGeneNeighborApproach;
																	
						SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_ms.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + maximumScore);
						SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_asa.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + allSumApproach);
						SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbganna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + sumBestGeneAndNcNeighborApproach);
						SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbgna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + sumBestGeneNeighborApproach);
					}
				}
			}
		}
		
		Calendar cal1 = Calendar.getInstance();
	    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("# Current time: " + df1.format(cal1.getTime()) + '\n');
	}
	
	private boolean syntenyComputation(GFCWindow window) {
		int species;
		int geneIndex;
		int chromosome;
		int displaySize;
		int geneCounter;
		int numberofgenes;
		int chromosomeSize;
		int neighborChromosomeSize;
		
		int[] neighborHitCounter;
		int[] ncNeighborHitCounter;
		
		float maximumScore;
		float allSumApproach;
		float neighborMaxScore;
		float ncNeighborMaxScore;
		float sumBestGeneNeighborApproach;
		float sumBestGeneAndNcNeighborApproach;
		
		Float[] maxValue1;
		Float[] maxValue2;
		
		SimpleFileIO reader;
				
		Gene[] neighborhoodGenes;
		Gene[] ncNeighborGenes;
		
		Couple<String, Float> blastRes;
		
		ArrayList<Couple<String, Float>> neighborNcRes;
		
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	    window.addMessageForReader("[STATUS] - Current time: " + df.format(cal.getTime()));
		window.updateEastPanelText(2);
		
		geneCounter = 0;
		numberofgenes = 0;
		reader = new SimpleFileIO();
		
		if(!refSyntenyFile.equals("")) {
			window.addMessageForReader("[STATUS] - Reading reference synteny file ... ");
			window.updateEastPanelText(2);
			try {
				reader.readSyntenyFile(path, refSyntenyFile);
				window.addMessageForReader("[STATUS] - Loaded reference synteny");
				window.updateEastPanelText(2);
			} catch (Exception e) {
				window.addMessageForReader("[ERROR] - Reading reference synteny file failed. Check file path and file format etc.");
				window.updateEastPanelText(3);
				return false;
			}
		} else {
			window.addMessageForReader("[STATUS] - No reference data found. Computing synteny scores for query dataset only.");
			window.updateEastPanelText(2);
		}
		
		if(speciesStart == 0) 
			speciesStart = reader.getSpeciesList().size();

		window.addMessageForReader("[STATUS] - Reading query synteny file ... ");
		window.updateEastPanelText(2);
		try {
			reader.readSyntenyFile(path, querySyntenyFile);
			window.addMessageForReader("[STATUS] - Loaded query synteny");
			window.updateEastPanelText(2);
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Reading query synteny file failed. Check file path and file format etc.");
			window.updateEastPanelText(3);
			return false;
		}

		speciesList	= reader.getSpeciesList();
		
		if(speciesEnd == 0) 
			speciesEnd = speciesList.size();

		try {
			window.addMessageForReader("[STATUS] - Reading neighborhood correlation score file ... ");
			window.updateEastPanelText(2);
			speciesList = reader.readNCFile(path, ncScoreFile, speciesList);
			window.addMessageForReader("[STATUS] - Loaded NC scores");
			window.updateEastPanelText(2);
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Reading NC score file failed. Check file path and file format etc.");
			window.updateEastPanelText(3);
			return false;
		}
		reader = null;

		for (int i = speciesStart; i < speciesEnd; i++) {
			for (int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
				chromosomeSize = speciesList.get(i).getChromosomes().get(j).getNumberOfGenes();
				numberofgenes = numberofgenes + chromosomeSize;
			}
		}

		if(numberofgenes>100)
			displaySize = numberofgenes/100;
		else if(numberofgenes > 5)
			displaySize = 5;
		else
			displaySize = numberofgenes;
		
		window.addMessageForReader("[STATUS] - Synteny Extractor status ... INITIATED");
		window.addMessageForReader("[STATUS] - Total gene pairs to process ... " + numberofgenes);
		window.updateEastPanelText(2);
		
		try {
			for (int i = speciesStart; i < speciesEnd; i++) {
				for (int j = 0; j < speciesList.get(i).getChromosomes().size(); j++) {
					chromosomeSize = speciesList.get(i).getChromosomes().get(j).getNumberOfGenes();

					for (int k = 0; k < chromosomeSize; k++) {
						geneCounter++;

						if(geneCounter % displaySize == 0) {
							float totalblastCompleted = ((float)geneCounter/numberofgenes) * 100;
							window.addMessageForReader("[STATUS] - " + totalblastCompleted + "% completed ... " + geneCounter + "/" + numberofgenes + " genes processed." );
							window.updateEastPanelText(2);
						}

						if(chromosomeSize == 1) {
							for (int l = 0; l < speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().size(); l++) {
								blastRes = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().get(l);
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_ms.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_asa.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbganna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbgna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");									
							}
							continue;
						} else if(k >= neighborhoodSize && k < (chromosomeSize - neighborhoodSize)) {
							neighborhoodGenes = new Gene[2 * neighborhoodSize];
							neighborHitCounter = new int[2 * neighborhoodSize];
							for(int p = 0; p < neighborhoodSize; p++) {
								neighborhoodGenes[p + neighborhoodSize] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k + p + 1);
								neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k - p - 1);
							}
						} else if (k >= neighborhoodSize && k >= (chromosomeSize - neighborhoodSize)) {
							neighborhoodGenes = new Gene[neighborhoodSize + (chromosomeSize - k - 1)];
							neighborHitCounter = new int[neighborhoodSize + (chromosomeSize - k - 1)];
							for(int p = 0; p < neighborhoodSize; p++) 
								neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k - p - 1);
							for (int p = 0; p < (chromosomeSize - k - 1); p++) 
								neighborhoodGenes[p + neighborhoodSize] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k + p + 1);
						} else if (k < neighborhoodSize && k < (chromosomeSize - neighborhoodSize)) {
							neighborhoodGenes = new Gene[neighborhoodSize + k];
							neighborHitCounter = new int[neighborhoodSize + k];
							for(int p = 0; p < k; p++) 
								neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
							for (int p = k + 1; p < (k + neighborhoodSize + 1); p++) 
								neighborhoodGenes[p - 1] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
						} else {
							neighborhoodGenes = new Gene[chromosomeSize - 1];
							neighborHitCounter = new int[chromosomeSize - 1];
							for(int p = 0; p < k; p++) 
								neighborhoodGenes[p] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
							for(int p = k + 1; p < chromosomeSize; p++) 
								neighborhoodGenes[p - 1] = speciesList.get(i).getChromosomes().get(j).getGeneList().get(p);
						}

						for (int l = 0; l < speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().size(); l++) {
							blastRes = speciesList.get(i).getChromosomes().get(j).getGeneList().get(k).getGenencScore().get(l);
							String[] strArr = blastRes.first.split("\\.");
							species = Integer.parseInt(strArr[0]);
							chromosome = Integer.parseInt(strArr[1]);
							geneIndex = Integer.parseInt(strArr[2]);
							neighborChromosomeSize = speciesList.get(species).getChromosomes().get(chromosome).getNumberOfGenes();

							if(neighborChromosomeSize == 1) {
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_ms.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_asa.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbganna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");
								SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbgna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + "0.0");									
								continue;
							} else if(geneIndex >= neighborhoodSize && geneIndex < (neighborChromosomeSize - neighborhoodSize)) {
								ncNeighborGenes = new Gene[2 * neighborhoodSize];
								ncNeighborHitCounter = new int[2 * neighborhoodSize];
								for(int p = 0; p < neighborhoodSize; p++) {
									ncNeighborGenes[p + neighborhoodSize] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex + p + 1);
									ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex - p - 1);
								}
							} else if (geneIndex >= neighborhoodSize && geneIndex >= (neighborChromosomeSize - neighborhoodSize)) {
								ncNeighborGenes = new Gene[neighborhoodSize + (neighborChromosomeSize - geneIndex - 1)];
								ncNeighborHitCounter = new int[neighborhoodSize + (neighborChromosomeSize - geneIndex - 1)];
								for(int p = 0; p < neighborhoodSize; p++) 
									ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex - p - 1);
								for (int p = 0; p < (neighborChromosomeSize - geneIndex - 1); p++) 
									ncNeighborGenes[p + neighborhoodSize] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(geneIndex + p + 1);
							} else if (geneIndex < neighborhoodSize && geneIndex < (neighborChromosomeSize - neighborhoodSize)) {
								ncNeighborGenes = new Gene[neighborhoodSize + geneIndex];
								ncNeighborHitCounter = new int[neighborhoodSize + geneIndex];
								for(int p = 0; p < geneIndex; p++) 
									ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
								for (int p = geneIndex + 1; p < (geneIndex + neighborhoodSize + 1); p++) 
									ncNeighborGenes[p - 1] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
							} else {
								ncNeighborGenes = new Gene[neighborChromosomeSize - 1];
								ncNeighborHitCounter = new int[neighborChromosomeSize-1];
								for(int p = 0; p < geneIndex; p++) 
									ncNeighborGenes[p] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
								for(int p = geneIndex + 1; p < neighborChromosomeSize; p++) 
									ncNeighborGenes[p - 1] = speciesList.get(species).getChromosomes().get(chromosome).getGeneList().get(p);
							}

							for(int d = 0; d < ncNeighborHitCounter.length; d++) 
								ncNeighborHitCounter[d] = 0;

							for(int d = 0; d < neighborHitCounter.length; d++) 
								neighborHitCounter[d] = 0;

							maximumScore = (float) 0;
							maxValue1 = new Float[neighborhoodGenes.length];
							maxValue2 = new Float[ncNeighborGenes.length];

							allSumApproach = (float) 0;

							for(int d = 0; d < maxValue1.length; d++) 
								maxValue1[d] = (float)0;
							for(int d = 0; d < maxValue2.length; d++) 
								maxValue2[d] = (float)0;

							for(int a = 0; a < neighborhoodGenes.length; a++) {
								neighborMaxScore = (float) 0;
								for(int b = 0; b < ncNeighborGenes.length; b++) {
									neighborNcRes = neighborhoodGenes[a].getGenencScore();
									for(int c = 0; c < neighborNcRes.size(); c++) {
										String genIndex = neighborNcRes.get(c).first.trim();
										if(genIndex.equals(ncNeighborGenes[b].getIndex().trim())) {
											neighborHitCounter[a]++;
											allSumApproach = allSumApproach + neighborNcRes.get(c).second;

											if(neighborNcRes.get(c).second > neighborMaxScore) 
												neighborMaxScore = neighborNcRes.get(c).second;
											if(neighborNcRes.get(c).second > maximumScore) 
												maximumScore = neighborNcRes.get(c).second;
										}
									}
								}
								maxValue1[a] = neighborMaxScore;
							}

							for(int b = 0; b < ncNeighborGenes.length; b++) {
								ncNeighborMaxScore = (float) 0;
								for(int a = 0; a < neighborhoodGenes.length; a++) {
									neighborNcRes = ncNeighborGenes[b].getGenencScore();
									for(int c = 0; c < neighborNcRes.size(); c++) {
										String genIndex = neighborNcRes.get(c).first.trim();
										if(genIndex.equals(neighborhoodGenes[a].getIndex().trim()) && neighborNcRes.get(c).second > ncNeighborMaxScore)
											ncNeighborMaxScore = neighborNcRes.get(c).second;
										if(genIndex.equals(neighborhoodGenes[a].getIndex().trim()))
											ncNeighborHitCounter[b]++;
									}
								}
								maxValue2[b] = ncNeighborMaxScore;
							}

							sumBestGeneNeighborApproach = (float) 0;

							for(int b = 0; b < maxValue1.length; b++) 
								sumBestGeneNeighborApproach = sumBestGeneNeighborApproach + maxValue1[b];

							sumBestGeneAndNcNeighborApproach = (float) 0;

							for(int b = 0; b < maxValue2.length; b++) 
								sumBestGeneAndNcNeighborApproach = sumBestGeneAndNcNeighborApproach + maxValue2[b];

							sumBestGeneAndNcNeighborApproach += sumBestGeneNeighborApproach;

							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_ms.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + maximumScore);
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_asa.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + allSumApproach);
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbganna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + sumBestGeneAndNcNeighborApproach);
							SequenceFileWriter.writeAndAppendLine(path, output + "_k" + neighborhoodSize + "_sbgna.SySc", i + "." + j + "." + k + "\t" + blastRes.first + "\t" + blastRes.second + "\t" + sumBestGeneNeighborApproach);
						}
					}
				}
			}
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Calculating synteny score failed. Check that you have write permissions for files etc.");
			window.updateEastPanelText(3);
			return false;
		}
		
		Calendar cal1 = Calendar.getInstance();
	    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	    window.addMessageForReader("[METHOD] - Current time: " + df1.format(cal1.getTime()));
		window.updateEastPanelText(2);
		return true;
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public ArrayList<Species> getSpeciesList() { return speciesList; }

	public void main(String[] args) throws Exception {
		Parameters params;
		try {
			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"SyntenyExtractor is an implementation under progress \n\n" +
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar SyntenyExtractor [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}

			path = params.path;
			refSyntenyFile = params.refsynteny;
			querySyntenyFile = params.files.get(0);
			ncScoreFile = params.files.get(1);
			neighborhoodSize = Integer.parseInt(params.neighborhoodsize);
			output = params.output;
			speciesStart = Integer.parseInt(params.speciesStart);
			speciesEnd = Integer.parseInt(params.speciesEnd);
			
			if(!path.endsWith("/"))
				path = path.concat("/");

			syntenyComputation();
			System.out.println("Synteny Scores computed successfully");	
		} catch (Exception e) {
			System.out.println("Synteny Extractor Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		try {
			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);

			path = params.path;
			refSyntenyFile = params.refsynteny;
			querySyntenyFile = params.files.get(0);
			ncScoreFile = params.files.get(1);
			neighborhoodSize = Integer.parseInt(params.neighborhoodsize);
			output = params.output;
			speciesStart = Integer.parseInt(params.speciesStart);
			speciesEnd = Integer.parseInt(params.speciesEnd);
			
			if(!path.endsWith("/"))
				path = path.concat("/");

			syntenyComputation(window);
			window.addMessageForReader("[METHOD] - Synteny scores computed successfully");
			window.updateEastPanelText(2);
			return true;
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Synteny scores computation failed : " + e.getMessage());
			window.updateEastPanelText(2);
			return false;
		}
	}

	public String getModuleName() {
		return "SyntenyExtractor";
	}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
