package modules.syntenyExtractorModule;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Handles all regular application parameters for VMCMC. Used by MCMCMainApplication class for parameter parsing in the main function.
 * @author Raja Hashim Ali.
 */
public class Parameters {

	/** Required parameters: Application/Module to run */
	@Parameter(description = "<Query Synteny File> <Neighborhood Correlation Score File>")
	public List<String> files = new ArrayList<String>();
	
	/** Help. */
	@Parameter(names = {"-h", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Filename */
	@Parameter(names = {"-p","--path"}, description = "Path for finding the data and outputting results.")
	public String path = "./";
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-f","--referencesynteny"}, description = "File containing reference synteny information.")
	public String refsynteny = "";
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-o","--output"}, description = "Name of all output files with corresponding file extensions.")
	public String output = "GenFam";
	
	/** Simple Statistics only */
	@Parameter(names = {"-k","--syntenicneighborhoodsize"}, description = "Size of Syntenic Neighborhood. Must be a non-negative integer. Number of genes to consider in the nighborhood for computing synteny scores and correlation scores. ")
	public String neighborhoodsize = "5";
	
	/** Simple Statistics only */
	@Parameter(names = {"-s","--speciesStart"}, description = "Species from which to start computing synteny scores.")
	public String speciesStart = "0";
	
	/** Simple Statistics only */
	@Parameter(names = {"-e","--speciesEnd"}, description = "Species uptil which to end computing synteny scores.")
	public String speciesEnd = "0";
}
