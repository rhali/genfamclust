package modules.ncCompModule;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Handles all regular application parameters for VMCMC. Used by MCMCMainApplication class for parameter parsing in the main function.
 * @author Raja Hashim Ali.
 */
public class Parameters {

	/** Required parameters: Application/Module to run */
	@Parameter(description = "<blast file>")
	public List<String> files = new ArrayList<String>();
	
	/** Help. */
	@Parameter(names = {"-h", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-p","--path"}, description = "Path for finding the data and outputting results.")
	public String path = "./";
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-o","--output"}, description = "Name of all output files with corresponding file extensions.")
	public String output = "GenFam";
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-rp","--resourcepath"}, description = "Resource path, where the Neighborhood Correlation program from Durand et. al can be found. ")
	public String resourcepath = "./";
	
	/** Simple Statistics only */
	@Parameter(names = {"-t","--threshold"}, description = "Threshold for calculating NC scores. Scores below this threshold will not be reported to the file. ")
	public String threshold =  "0.3";
}
