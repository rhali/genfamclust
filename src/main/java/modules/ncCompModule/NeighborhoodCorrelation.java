package modules.ncCompModule;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.beust.jcommander.JCommander;

import miscellaneous.JCommanderUsageWrapper;
import miscellaneous.TabToSpaceConverter;
import miscellaneous.gui.GFCWindow;
import modules.GenFamModules;
import modules.ncCompModule.Parameters;

/**
 * 
 * @author Raja Hashim Ali
 **/
public class NeighborhoodCorrelation implements GenFamModules{

	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private String softwareName;
	private String dataPath;
	private String resourcePath;
	private String resultPath;
	private String output;
	private String blastFile;
	private float ncThreshold;

	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public NeighborhoodCorrelation(String path, String resourcePath, String blastFile, float ncThreshold, String output){
		softwareName = "NC_standalone";
		dataPath = path;
		this.resourcePath = resourcePath;
		resultPath = path;
		this.output = output;
		this.ncThreshold = ncThreshold;
		this.blastFile = blastFile;
	}

	public NeighborhoodCorrelation(){
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */

	public void displayHelp() {
		String[] command = {resourcePath + softwareName, "-h"};
		runCommand(command);
	}

	public void setRelativePath(String resourcePath, String dataPath, String resultPath) {
		this.resourcePath 	= resourcePath;
		this.dataPath 		= dataPath;
		this.resultPath 	= resultPath;
	}

	public String[] makeCommand() {
		String[] command = {resourcePath + softwareName, "-f", dataPath + "spaced" + blastFile, "-o", resultPath + output + ".nnc", "--nc_thresh", String.valueOf(ncThreshold)};
		return command;
	}

	public boolean runCommand(String[] command) {
		String 				line;
		Runtime 			runtime;
		Process 			process;
		InputStream 		stdin;
		InputStream 		stderr;
		BufferedReader 		bufferedreader;
		InputStreamReader 	inputstreamreader;

		try {
			
			TabToSpaceConverter ts = new TabToSpaceConverter(dataPath, blastFile, "spaced" + blastFile);
			ts.runCommand(ts.makeCommand());
			
			runtime = Runtime.getRuntime();
			process = runtime.exec(command, null, null);
			stdin = process.getInputStream();
			inputstreamreader = new InputStreamReader(stdin);
			bufferedreader = new BufferedReader(inputstreamreader);

			System.out.println(	"Starting python program Neighborhood Correlation\n");
			System.out.println("\n\nPlease wait ... The NC program is computing NC scores in the background ...");
			System.out.println("\nIt will take a few minutes per mb of blast results ... Please wait ...");
			while ( (line = bufferedreader.readLine()) != null)
				System.out.println(line);

			stderr = process.getErrorStream();
			inputstreamreader = new InputStreamReader(stderr);
			bufferedreader = new BufferedReader(inputstreamreader);
			while ( (line = bufferedreader.readLine()) != null)
				System.out.println(line);
			
			process.waitFor();
			
			System.out.println("Neighborhood Correlation status ... COMPLETED SUCCESSFULLY");
			return true;
		} catch(Exception e) {
			System.out.println(e.toString());
			System.exit(-1);
			return false;
		}
	}
	
	public boolean runCommand(GFCWindow window, String[] command) {
		String 				line;
		Runtime 			runtime;
		Process 			process;
		InputStream 		stdin;
		InputStream 		stderr;
		BufferedReader 		bufferedreader;
		InputStreamReader 	inputstreamreader;

		try {
			
			TabToSpaceConverter ts = new TabToSpaceConverter(dataPath, blastFile, "spaced" + blastFile);
			ts.runCommand(ts.makeCommand());
			
			runtime = Runtime.getRuntime();
			process = runtime.exec(command, null, null);
			stdin = process.getInputStream();
			inputstreamreader = new InputStreamReader(stdin);
			bufferedreader = new BufferedReader(inputstreamreader);

			window.addMessageForReader("[METHOD] - Starting python program Neighborhood Correlation.");
			window.addMessageForReader("[STATUS] - Please wait ... The NC program is computing NC scores in the background ...");
			window.addMessageForReader("[STATUS] - NC might take a few MINUTES per mb of blast results without activity! Please wait ...");
			window.updateEastPanelText(2);
			
			while ( (line = bufferedreader.readLine()) != null) {
				window.addMessageForReader("[STATUS] - " + line);
				window.updateEastPanelText(2);
			}

			stderr = process.getErrorStream();
			inputstreamreader = new InputStreamReader(stderr);
			bufferedreader = new BufferedReader(inputstreamreader);
			while ( (line = bufferedreader.readLine()) != null) {
				if(line.startsWith("WARNING:")) {
					window.addMessageForReader("[ERROR] - Check python implementation/version. NC program is unable to run correctly.");
					window.addMessageForReader("[ERROR] - Neighborhood correlation scores computation failed. ");
					window.addMessageForReader("[ERROR] - Possible cause: Incorrect python version being used (NC works with 2.7 and not 3.0.");
					window.addMessageForReader("[ERROR] - Possible cause: Numpy not present in Python version. ");
					window.updateEastPanelText(3);
					return false;
				}
				window.addMessageForReader("[STATUS] - " + line);
				window.updateEastPanelText(2);
			}
			
			process.waitFor();
			return true;
		} catch(Exception e) {
			window.addMessageForReader("[ERROR] - Neighborhood correlation scores computation failed : " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}

	public String getModuleName() {
		return "NeighborhoodCorrelation";
	}

	public void main(String[] args) throws Exception {
		Parameters params;
		String path;
		String resourcePath;
		String blastFile;
		float ncThreshold; 
		String output;

		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			JCommander jc = new JCommander(params, args);

			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						"Neighborhood Correlation is a program developed by Dannie Durand et al at Carnegie \n" +
						"Mellon University USA and is used to predict homology based on similarity. \n\n" +
						"Version 2.1 (July 2011)\n\n" +
						"This implementation depends upon the Numpy numerical package, and uses a compiled C\n" +
						"helper to for memory and calculation efficiency.\n\n" +
						"If the helper module is not available and cannot be compiled, this implementation\n" +
						"will still run, but use a slower python implementation of the helper.\n\n" +
						"For details of Neighborhood Correlation please refer to\n" +
						"http://www.neighborhoodcorrelation.org/ or the publication mentioned below. \n\n" +
						"(C) 2011 Jacob Joseph <jmjoseph@andrew.cmu.edu> and Carnegie Mellon University\n\n" +
						"This program is free software; you can redistribute it and/or modify it under the\n" +
						"terms of the GNU General Public License as published by the Free Software Foundation;" +
						"either version 3 of the License, or (at your option) any later version." +
						"If you use this program in your research or for comparison reasons, please cite the \n" +
						"following articles.\n" +
						"References:\n" +
						"    Sequence Similarity Network Reveals Common Ancestry of Multidomain Proteins,\n" +
						"    Song N et al., PLoS Computational Biology 4(5): e1000063 doi:10.1371/journal.pcbi.1000063\n\n" +
						"    Synteny-aware scoring improves homology inference and gives a more accurate \n" +
						"    partitioning of gene families,\n" +
						"    Ali RH et al., ..........., 2013, doi: .................\n\n" +
						"Releases, source code and tutorial: http://code.google.com/p/genfamclust/\n\n" +
						"Releases, source code and tutorial: http://www.NeighborhoodCorrelation.org/\n\n" +
						"License: GenFamClust is available under the New BSD License.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
				"    java -jar GenFamClust-X.Y.Z.jar NeighborhoodCorrelation [options] <args>\n");
				JCommanderUsageWrapper.getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}

			Calendar cal = Calendar.getInstance();
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df.format(cal.getTime()));
			
			path = params.path;
			resourcePath = params.resourcepath;
			blastFile = params.files.get(0);
			try {
				Float.parseFloat(params.threshold);
			} catch (Exception e) {
				System.out.println("Neighborhood Correlation Computation Failed : Threshold " + params.threshold + " not a valid float");
				System.exit(-1);
			}
			if(Float.parseFloat(params.threshold) < (float)0 || Float.parseFloat(params.threshold) > (float)1) {
				System.out.println("Neighborhood Correlation Computation Failed : Threshold " + params.threshold + " not in range 0 and 1 inclusive");
				System.exit(-1);
			}
			ncThreshold = Float.parseFloat(params.threshold);
			output = params.output;

			if(!path.endsWith("/"))
				path = path.concat("/");

			if(!resourcePath.endsWith("/"))
				resourcePath = resourcePath.concat("/");

			NeighborhoodCorrelation nc = new NeighborhoodCorrelation(path, resourcePath, blastFile, ncThreshold, output);
			String[] cmd = nc.makeCommand();
			nc.runCommand(cmd);
			
			Calendar cal1 = Calendar.getInstance();
		    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			System.out.println("# Current time: " + df1.format(cal1.getTime()));
			
			System.out.println("Neighborhood Correlation computed successfully");
		} catch (Exception e) {
			System.out.println("Neighborhood Correlation Computation Failed : " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public boolean main(GFCWindow window, String[] args) {
		Parameters params;
		String path;
		String resourcePath;
		String blastFile;
		float ncThreshold; 
		String output;

		try {

			// ================ PARSE USER OPTIONS AND ARGUMENTS ================

			params = new Parameters();
			new JCommander(params, args);

			Calendar cal = Calendar.getInstance();
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    window.addMessageForReader("[METHOD] - Current time: " + df.format(cal.getTime()));
			window.updateEastPanelText(2);
			
			path = params.path;
			resourcePath = params.resourcepath;
			blastFile = params.files.get(0);
			ncThreshold = Float.parseFloat(params.threshold);
			output = params.output;

			if(!path.endsWith("/"))
				path = path.concat("/");

			if(!resourcePath.endsWith("/"))
				resourcePath = resourcePath.concat("/");

			NeighborhoodCorrelation nc = new NeighborhoodCorrelation(path, resourcePath, blastFile, ncThreshold, output);
			String[] cmd = nc.makeCommand();
			boolean run = nc.runCommand(window, cmd);
			
			if(run == true) {
				Calendar cal1 = Calendar.getInstance();
				SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				window.addMessageForReader("[METHOD] - Time: " + df1.format(cal1.getTime()) + " Neighborhood correlation scores computed successfully!");
				window.updateEastPanelText(2);
			}
			return true;
		} catch (Exception e) {
			window.addMessageForReader("[ERROR] - Neighborhood correlation scores computation failed : " + e.getMessage());
			window.updateEastPanelText(3);
			return false;
		}
	}

	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */

}
