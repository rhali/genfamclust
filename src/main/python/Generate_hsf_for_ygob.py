speciesList = ['Cglabrata', 'Ecymbalariae', 'Egossypii', 'Kafricana', 'Klactis', 'Knaganishii', 'Lkluyveri', 'Lthermotolerans', 'Lwaltii', 'Ncastellii', 'Ndairenensis', 'Scerevisiae', 'Skudriavzevii', 'Smikatae', 'Suvarum', 'Tblattae', 'Tdelbrueckii', 'Tphaffii', 'Vpolyspora', 'Zrouxii'];
output_handle = open("RefSynteny.hsf", "w")
path = '/Users/rhali/Documents/Eclipse/workspace/GenFamClust/Datasets/YGOB/'
for species in speciesList:
	names = []
	startCoords = []
	chromosomes = []
	f = open(path + species + '_genome.tab', 'r');
	for line in f:
		tempList = line.strip().split("\t");
		name = tempList[0]
		startCoord = tempList[2]
		chromosome = tempList[5]
		names.append(name)
		startCoords.append(startCoord)
		chromosomes.append(chromosome)

	for count in range(len(names)):
		geneName = names[count]
		
		if(('trna' not in geneName) == 1 and ('_CEN' not in geneName) == 1 and ('snR' not in geneName) == 1 and (geneName.endswith('r') == False) and ('_RDN' not in geneName) and ('_NME' not in geneName) and ('_RTS' not in geneName) and ('_ETS' not in geneName) and ('rrna' not in geneName) and ('_PWR' not in geneName) and ('_ICR' not in geneName) and ('_RUF' not in geneName) and ('_SCR' not in geneName) and ('_SRG' not in geneName) and ('_RPR' not in geneName) and ('_LSR' not in geneName) and ('_TLC' not in geneName)):
			output_handle.write(species)
			output_handle.write('\t')
			output_handle.write(chromosomes[count])
			output_handle.write('\t')
			output_handle.write(geneName)
			output_handle.write('\t')
			output_handle.write(startCoords[count])
			output_handle.write('\n')

output_handle.close()
